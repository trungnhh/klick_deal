backend default {
  .host = "localhost";
  .port = "8080";
  .connect_timeout = 18000s;
  .first_byte_timeout = 18000s;
  .between_bytes_timeout = 18000s;
}

sub vcl_recv {
    if (req.restarts == 0) {
        if (req.http.x-forwarded-for) {
        set req.http.X-Client-Ip = req.http.X-Forwarded-For;
            set req.http.X-Forwarded-For =
            req.http.X-Forwarded-For + ", " + client.ip;
        } else {
            set req.http.X-Forwarded-For = client.ip;
        set req.http.X-Client-Ip = req.http.X-Forwarded-For;
        }
    }

    set req.backend = default;

    #for adminhtml
    if (req.url ~ "admin") {
        return (pass);
    }       

    if (req.url ~ "^cgi-bin") {
        error 404;
    }
    
    # don't cache following user agents
    if (req.http.user-agent ~ "iP(hone|od)" || req.http.user-agent ~ "Android" || req.http.user-agent ~ "BlackBerry") {
        return (pass);
    }
    
    if (req.request != "GET" &&
      req.request != "HEAD" &&
      req.request != "PUT" &&
      req.request != "POST" &&
      req.request != "TRACE" &&
      req.request != "OPTIONS" &&
      req.request != "DELETE" &&
      req.request != "PURGE") {
        /* Non-RFC2616 or CONNECT which is weird. */
        return (pipe);
    }
    
    # purge request
    if (req.request == "PURGE") {
        if (req.http.X-Purge-Host == "*") {
            if(req.http.X-Purge-Regex != "") {
                ban("obj.http.X-Purge-Host ~ " + req.http.X-Purge-Regex);
            } else {
                ban("obj.http.X-Purge-Host ~ " + req.http.host);
            }
        }
        else if (req.http.X-Purge-Regex == "/") {
            ban("obj.http.X-Purge-Host ~ " + req.http.X-Purge-Host + " && obj.http.X-Purge-URL == " + req.http.X-Purge-Regex);
        }
        else if(req.http.X-Purge-Regex != "") {
            ban("obj.http.X-Purge-Host ~ " + req.http.X-Purge-Host + " && obj.http.X-Purge-URL ~ " + req.http.X-Purge-Regex);
        }
        else {
            ban("obj.http.X-Purge-Host ~ " + req.http.X-Purge-Host + " && obj.http.X-Purge-URL ~ " + req.http.X-Purge-Regex + " && obj.http.Content-Type ~ " + req.http.X-Purge-Content-Type);
        }
        error 200 "Purged.";
    }

    # purge from frontend
    if (req.url ~ "varnishrefresh=1") {
        set req.url = regsuball(req.url,"([&]|[?]?)varnishrefresh=1","");
        if(req.url ~ "[?]" || req.url == "/") {
                ban("obj.http.X-Purge-Host ~ " + req.http.host + " && obj.http.X-Purge-URL == " + req.url);
        } else {
                ban("obj.http.X-Purge-Host ~ " + req.http.host + " && obj.http.X-Purge-URL ~ " + req.url);
        }
        error 450 "Purged.";
    }

    # we only deal with GET and HEAD by default    
    if (req.request != "GET" && req.request != "HEAD") {
        return (pass);
    }
    
    # don't cache following url pattern
    if ( req.url ~ "a3-storage-box-cloth-4" || req.url ~ "/(giftcard)" || req.url ~ "lcompare" || req.url ~ "product_compare" || req.url ~ "^/(apcc.php)" || req.url ~ "^/(apc.php)" || req.url ~ "^/(ajaxaddtocart)" || req.url ~ "^/(onestepcheckout)" || req.url ~ "^/(ajaxcart)" || req.url ~ "^/(biajax)" || req.url ~ "^/(checkout)" || req.url ~ "^/(customer)" || req.url ~ "^/(wishlist)" || req.url ~ "^/(paypal)" || req.url ~ "^/(api)" || req.url ~ "^/(sales)" || req.url ~ "^/(facebookall)") {
        return (pass);
    }
    
     # do not cache any page has url param no_cache
    if (req.url ~ "NO_CACHE") {
        return (pass);
    
    }
    if(req.url ~ "ajax"){
        return (pass);
    
    }
    if(req.url ~ "varnish/update"){
       return (pass);
    }
    if(req.url ~ "giftoption"){
        return (pass);
    }
   
    # not cacheable by default
    if (req.http.Authorization || req.http.Front-End-Https || req.http.X_Forwarded_Proto ~ "(?i)https" || req.http.X-Forwarded-Proto ~ "(?i)https" ) {
        return (pass);
    }
    
    # normalize url in case of leading HTTP scheme and domain
    set req.url = regsub(req.url, "^http[s]?://[^/]+", "");
    
    # static files are always cacheable. remove SSL flag and cookie
    if (req.url ~ "^/(media|js|skin)/.*\.(png|jpg|jpeg|gif|swf|ico)$") {
        unset req.http.Https;
        unset req.http.Cookie;
    }

    # as soon as we have a NO_CACHE cookie pass request (useless)?
    if (req.http.cookie ~ "NO_CACHE=") {
        return (pass);
    }

    # normalize Aceept-Encoding header
    # http://varnish.projects.linpro.no/wiki/FAQ/Compression
    if (req.http.Accept-Encoding) {
        if (req.url ~ "\.(jpg|png|gif|gz|tgz|bz2|tbz|mp3|ogg|swf|flv)$") {
            # No point in compressing these
            remove req.http.Accept-Encoding;
        } elsif (req.http.Accept-Encoding ~ "gzip") {
            set req.http.Accept-Encoding = "gzip";
        } elsif (req.http.Accept-Encoding ~ "deflate" && req.http.user-agent !~ "MSIE") {
            set req.http.Accept-Encoding = "deflate";
        } else {
            # unkown algorithm
            remove req.http.Accept-Encoding;
        }
    }
    
    # remove Google gclid parameters
    set req.url = regsuball(req.url,"\?gclid=[^&]+$",""); # strips when QS = "?gclid=AAA"
    set req.url = regsuball(req.url,"\?gclid=[^&]+&","?"); # strips when QS = "?gclid=AAA&foo=bar"
    set req.url = regsuball(req.url,"&gclid=[^&]+",""); # strips when QS = "?foo=bar&gclid=AAA" or QS = "?foo=bar&gclid=AAA&bar=baz"

    set req.url = regsuball(req.url,"&((utm)|(mc))_[^=]+=[^&]+",""); # strips when QS = "?foo=bar&utm_xxxx=AAA" or QS = "?foo=bar&utm_xxxx=AAA&bar=baz"
    set req.url = regsuball(req.url,"\?((utm)|(mc))_[^=]+=[^&]+&","?"); # strips when QS = "?utm_xxxx=AAA&foo=bar"
    set req.url = regsuball(req.url,"\?((utm)|(mc))_[^=]+=[^&]+$",""); # strips when QS = "?utm_xxxxx=AAA"
  
    return (lookup);
}

sub vcl_hash {
    hash_data(req.url);
    if (req.http.host) {
        hash_data(req.http.host);
    } else {
        hash_data(server.ip);
    }

    if (!(req.url ~ "^/(media|js|skin)/.*\.(png|jpg|jpeg|gif|css|js|swf|ico)$")) {
        call design_exception;
    }
    hash_data(req.http.X-Country-Code);

    return (hash);
}

sub vcl_fetch {
    if (beresp.status == 500) {
       set beresp.saintmode = 10s;
       return (restart);
    }
    set beresp.grace = 1h;
    # add ban-lurker tags to object
    set beresp.http.X-Purge-URL = req.url;
    set beresp.http.X-Purge-Host = req.http.host;
    set beresp.http.X-Country-Code = req.http.X-Country-Code;
    # fix cache per browser issue
    set beresp.http.Vary = "Accept-Encoding";
    
    if (beresp.status == 200 || beresp.status == 301 || beresp.status == 404 || beresp.status == 302) {
        if (beresp.http.Content-Type ~ "text/html" || beresp.http.Content-Type ~ "text/xml") {
        if (beresp.ttl < 1s) {
                set beresp.ttl = 0s;
                return (hit_for_pass);
            }

            # marker for vcl_deliver to reset Age:
            set beresp.http.magicmarker = "1";

            # Don't cache cookies
            unset beresp.http.set-cookie;
        }else{
        set beresp.ttl = 24h;   
        }
        
        return (deliver);
    }

    return (hit_for_pass);
}

sub vcl_deliver {
    # debug info
    if (resp.http.X-Cache-Debug) {
        if (obj.hits > 0) {
           set resp.http.X-Cache = "HIT";
           set resp.http.X-Cache-Hits = obj.hits;
        } else {
           set resp.http.X-Cache = "MISS";
        }
        set resp.http.X-Cache-Expires = resp.http.Expires;
    } else {
        # remove Varnish/proxy header
        remove resp.http.X-Varnish;
        remove resp.http.Via;
        remove resp.http.Age;
        remove resp.http.X-Purge-URL;
        remove resp.http.X-Purge-Host;
    }
    
    # Add in hit and miss markers
    if (obj.hits > 0) {
        set resp.http.X-Cache = "HIT";
        set resp.http.X-Cache-Hits = obj.hits;
    } else {
       set resp.http.X-Cache = "MISS";
    }
}
sub design_exception {
}
