<?php
class IWD_Opc_IndexController extends Mage_Checkout_Controller_Action{
	
	const XML_PATH_TITLE = 'opc/global/title';
	
	/**
	 * Predispatch: should set layout area
	 *
	 * @return Mage_Checkout_OnepageController
	 */
	public function preDispatch()
	{
		parent::preDispatch();
		$this->_preDispatchValidateCustomer();
	
		$checkoutSessionQuote = Mage::getSingleton('checkout/session')->getQuote();
		if ($checkoutSessionQuote->getIsMultiShipping()) {
			$checkoutSessionQuote->setIsMultiShipping(false);
			$checkoutSessionQuote->removeAllAddresses();
		}
	
		if (!$this->_canShowForUnregisteredUsers()) {
			$this->norouteAction();
			$this->setFlag('',self::FLAG_NO_DISPATCH,true);
			return;
		}
	
		return $this;
	}
	
	
	
	/**
	 * Get one page checkout model
	 *
	 * @return Mage_Checkout_Model_Type_Onepage
	 */
	public function getOnepage(){
		return Mage::getSingleton('checkout/type_onepage');
	}
	
	/**
     * Checkout page
     */
    public function indexAction(){
        if (!Mage::helper('checkout')->canOnepageCheckout()) {
            Mage::getSingleton('checkout/session')->addError($this->__('The onepage checkout is disabled.'));
            $this->_redirect('/');
            return;
        }
        $quote = $this->getOnepage()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->_redirect('/');
            return;
        }
        if (!$quote->validateMinimumAmount()) {
            $error = Mage::getStoreConfig('sales/minimum_order/error_message') ?
                Mage::getStoreConfig('sales/minimum_order/error_message') :
                Mage::helper('checkout')->__('Subtotal must exceed minimum order amount');

            Mage::getSingleton('checkout/session')->addError($error);
            $this->_redirect('/');
            return;
        }
        Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
        Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure' => true)));
        $this->getOnepage()->initCheckout();
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__(Mage::getStoreConfig(self::XML_PATH_TITLE)));
        $this->renderLayout();
    }
	
    /**
     * Check can page show for unregistered users
     *
     * @return boolean
     */
    protected function _canShowForUnregisteredUsers()
    {
    	return Mage::getSingleton('customer/session')->isLoggedIn()
    	|| $this->getRequest()->getActionName() == 'index'
    			|| Mage::helper('checkout')->isAllowedGuestCheckout($this->getOnepage()->getQuote())
    			|| !Mage::helper('checkout')->isCustomerMustBeLogged();
    }

    public function downloadAction(){
        $isbn   =   $this->getRequest()->getParam('isbn');
        $link   =   '';
        if($isbn){
            $productId  =   $this->getRequest()->getParam('product');
            $product =  Mage::getModel('catalog/product')->load($productId);
            if($product->getId()){
                if($isbn==$product->getData('isbn_product')){
                    $downloadLinks = Mage::getModel('downloadable/link')
                        ->getCollection()
                        ->addProductToFilter($product->getId());
                    $downloadLink   =   $downloadLinks->getFirstItem();
                    if(count($downloadLink->getData())){
                        $link = $downloadLink->getLinkFile();
                    }
                }
                if($link==''){
                    $message    =   $this->__('Invalid ISBN code. Please recheck.');
                    Mage::getSingleton('customer/session')->addError(Mage::helper('core')->escapeHtml($message));
                    //$url = $_SERVER['HTTP_REFERER'];
                    //$this->_redirectUrl( $url );
                    $this->_redirectUrl(Mage::getBaseUrl());
                    return false;
                }
            }
        }else{
            $link   =   $this->getRequest()->getParam('link');
        }

        $resource = Mage::helper('downloadable/file')->getFilePath(
            Mage_Downloadable_Model_Link::getBasePath(), $link
        );
        $resourceType = Mage_Downloadable_Helper_Download::LINK_TYPE_FILE;
        try {
            $this->_processDownload($resource, $resourceType);
            exit(0);
        }
        catch (Exception $e) {
            $this->_getCustomerSession()->addError(
                Mage::helper('downloadable')->__('An error occurred while getting the requested content. Please contact the store owner.')
            );
        }
    }

    public function demoAction(){
        $link   =   $this->getRequest()->getParam('link');


        $resource = Mage::getBaseDir('media').DS.$link;

        $resourceType = Mage_Downloadable_Helper_Download::LINK_TYPE_FILE;
        try {
            $this->_processDownload($resource, $resourceType);
            exit(0);
        }
        catch (Exception $e) {
            $this->_getCustomerSession()->addError(
                Mage::helper('downloadable')->__('An error occurred while getting the requested content. Please contact the store owner.')
            );
        }
    }

    protected function _processDownload($resource, $resourceType)
    {
        $helper = Mage::helper('downloadable/download');
        /* @var $helper Mage_Downloadable_Helper_Download */

        $helper->setResource($resource, $resourceType);

        $fileName       = $helper->getFilename();
        $contentType    = $helper->getContentType();

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->setHeader('Pragma', 'public', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
            ->setHeader('Content-type', $contentType, true);

        if ($fileSize = $helper->getFilesize()) {
            $this->getResponse()
                ->setHeader('Content-Length', $fileSize);
        }

        if ($contentDisposition = $helper->getContentDisposition()) {
            $this->getResponse()
                ->setHeader('Content-Disposition', $contentDisposition . '; filename='.$fileName);
        }
        $this->getResponse()
            ->clearBody();
        $this->getResponse()
            ->sendHeaders();
        header('Content-Disposition: attachment; filename=' . $fileName);
        session_write_close();
        $helper->output();
    }
    /**
     * Return customer session object
     *
     * @return Mage_Customer_Model_Session
     */
    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }
}