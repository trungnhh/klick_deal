<?php
/**
 * MageWorx
 * MageWorx SeoMarkup Extension
 *
 * @category   MageWorx
 * @package    MageWorx_SeoMarkup
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */


/**
 * @see MageWorx_SeoMarkup_Model_Catalog_Product_Richsnippet_Product
 */
class MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Meta_Depth extends MageWorx_SeoMarkup_Model_Richsnippet_Catalog_Product_Abstract
{
    protected function _addAttributeForNodes(simple_html_dom_node $node)
    {
        $attributeCode = Mage::helper('seomarkup/config')->getDepthAttributeCode();
        if($attributeCode){
            $depth = $this->_getDepth($attributeCode);
            if($depth){
                $node->innertext = $node->innertext . '<meta itemprop="depth" content="'. $depth. '">' . "\n";
                return true;
            }
        }
        return false;
    }

    protected function _getDepth($attributeCode)
    {
        $depthValue = Mage::helper('seomarkup')->getAttributeValueByCode($this->_product, $attributeCode);
        $unit = Mage::helper('seomarkup/config')->getRichsnippetDimensionsUnit();
        if($depthValue){
            if(is_numeric($depthValue) && $unit){
                return $depthValue . ' ' . $unit;
            }elseif(preg_match('/^([0-9]+[\s]+[a-zA-Z]+)$/', $depthValue)){
                return $depthValue;
            }
            return false;
        }
    }

    protected function _getItemConditions()
    {
        return array("*[itemtype=http://schema.org/Product]");
    }

    protected function _checkBlockType()
    {
        return true;
    }

    protected function _isValidNode(simple_html_dom_node $node)
    {
        return true;
    }
}