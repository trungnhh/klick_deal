<?php
/**
 * MageWorx
 * MageWorx SeoMarkup Extension
 * 
 * @category   MageWorx
 * @package    MageWorx_SeoMarkup
 * @copyright  Copyright (c) 2015 MageWorx (http://www.mageworx.com/)
 */



class MageWorx_SeoMarkup_Model_System_Config_Source_Condition
{
    public function toOptionArray()
    {
        return array(
            array('value' => 'NewCondition', 'label' => Mage::helper('seomarkup')->__('New')),
            array('value' => 'RefurbishedCondition', 'label' => Mage::helper('seomarkup')->__('Refurbished')),
            array('value' => 'UsedCondition', 'label' => Mage::helper('seomarkup')->__('Used')),
            array('value' => 'DamagedCondition', 'label' => Mage::helper('seomarkup')->__('Damaged')),
        );
    }
}
