<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to info@nostresscommerce.cz so we can send you a copy immediately.
*
* @copyright Copyright (c) 2009 NoStress Commerce (http://www.nostresscommerce.cz)
*
*/

/**
 *
 * @category Nostress
 * @package Nostress_Nscexport
 */

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Grid_Renderer_Upload extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
	public function render(Varien_Object $row)
	{
		$url = $this->getUrl('*/nscexport_profiles_grid/uploadFeed', array('id' => $row->getId()));
		$label = Mage::helper('nscexport')->__('Submit via FTP');
		$disabledHtml = "";
		
		if(!$row->isUploadable())
		{
		    $url = "";
		    $disabledHtml = 'disabled="disabled" class="disabled"';
		}
		
		$title =  Mage::helper('nscexport')->__('Submit feed via FTP.');
		$html = '<form><button title="'.$title.'" onclick="event.stopPropagation();setLocation(\''.$url.'\');" type="button" '.$disabledHtml.' >' .$label. '</button></form>';
		
		$enabled =  Mage::helper('nscexport/version')->isLicenseValid();
		$disabledHtml = "";
		
		if(!$enabled)
		{			
			$disabledHtml = 'disabled="disabled" class="disabled"';
		}
		
		//$html .= "<br/>&nbsp;";
		$label = Mage::helper('nscexport')->__('Submission Info');
		$feedObject = $row->getFeedObject();
		$channelManualUrl = $feedObject->getChannelManualUrl();
		$channelPageUrl = $feedObject->getChannelPageUrl();
		
		if(!Mage::helper('nscexport/version')->isLicenseValid(true))
		{
			$channelManualUrl = "empty";
			$channelPageUrl = "";
		}
		
		$channelLabel = $feedObject->getChannelLabel();
		$pageLabel = $this->__("How to submit %s feed",$channelLabel);
		$feedFileUrl = $row->getUrl();
		if(!(strstr($feedFileUrl,'http') > -1))
			$feedFileUrl = "";
		
		$executeProfileUrl = $this->getUrl('*/nscexport_profiles_grid/generate', array('export_id' => $row->getId()));
		//button
		$html .= '<form><button title="'.$label.'" onclick="event.stopPropagation();loadSubmissionInfo(\''.$pageLabel.'\',\''.$channelPageUrl.'\',\''.$channelManualUrl.'\',\''.$feedFileUrl.'\',\''.$executeProfileUrl.'\')" type="button" '.$disabledHtml.'>' .$label. '</button></form>';
		//link
		//$html .= '<br><a href="#" title="'.$label.'" onclick="event.stopPropagation();loadSubmissionInfo(\''.$pageLabel.'\',\''.$channelPageUrl.'\',\''.$channelManualUrl.'\')">'.$label.'</a>';
		
		return $html;
	}
}
