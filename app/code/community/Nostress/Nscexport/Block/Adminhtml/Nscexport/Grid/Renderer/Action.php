<?php
/** 
 * Magento Module developed by NoStress Commerce 
 * 
 * NOTICE OF LICENSE 
* 
* This source file is subject to the Open Software License (OSL 3.0) 
* that is bundled with this package in the file LICENSE.txt. 
* It is also available through the world-wide-web at this URL: 
* http://opensource.org/licenses/osl-3.0.php 
* If you did of the license and are unable to 
* obtain it through the world-wide-web, please send an email 
* to info@nostresscommerce.cz so we can send you a copy immediately. 
* 
* @copyright Copyright (c) 2009 NoStress Commerce (http://www.nostresscommerce.cz) 
* 
*/ 

/** 
 * 
 * @category Nostress 
 * @package Nostress_Nscexport
 */

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Grid_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
	public function render(Varien_Object $row)
	{
		$html = "";
		$buttonRun = $this->getRunButtonHtml($row);
		$html .= $buttonRun;
		$html .= $this->getEditLinkHtml($row);				
		return $html;
	}
	
	protected function getRunButtonHtml($row)
	{
		$url = $this->getUrl('*/nscexport_profiles_grid/generate', array('export_id' => $row->getId()));
		$label = Mage::helper('nscexport')->__('Execute Profile');
		$enabled =  Mage::helper('nscexport/version')->isLicenseValid();
		$disabledHtml = "";
		
		if(!$enabled)
		{
			$label = Mage::helper('nscexport')->__('Execute Disabled');
			$url = "";
			$disabledHtml = 'disabled="disabled" class="disabled"';
		}
		$title =  Mage::helper('nscexport')->__('Execute profile to update feed.');
		$html = '<form><button title="'.$title.'"onclick="event.stopPropagation();setLocation(\''.$url.'\');" type="button" '.$disabledHtml.' >' .$label. '</button></form>';
		return $html;
	}
	
	protected function getEditLinkHtml($row)
	{
		$url = $this->getUrl("*/*/edit",array("id"=>$row->getId()));
		$label = Mage::helper('nscexport')->__('Edit Profile');
		$html = '<a href="'.$url.'"  title="'.$label.'">'.$label.'</a>';
		return $html;
	}
}
