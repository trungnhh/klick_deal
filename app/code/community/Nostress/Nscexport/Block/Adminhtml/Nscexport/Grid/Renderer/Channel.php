<?php
/** 
 * Magento Module developed by NoStress Commerce 
 * 
 * NOTICE OF LICENSE 
 * 
 * This source file is subject to the Open Software License (OSL 3.0) 
 * that is bundled with this package in the file LICENSE.txt. 
 * It is also available through the world-wide-web at this URL: 
 * http://opensource.org/licenses/osl-3.0.php 
 * If you did of the license and are unable to 
 * obtain it through the world-wide-web, please send an email 
 * to info@nostresscommerce.cz so we can send you a copy immediately. 
 * 
 * @copyright Copyright (c) 2009 NoStress Commerce (http://www.nostresscommerce.cz) 
 * 
 */ 

/** 
 * 
 * @category Nostress 
 * @package Nostress_Nscexport
 */

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Grid_Renderer_Channel extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Options
{	//Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text
    /**
     * Prepare link to display in grid
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
    	//$html = parent::render($row);
    	$feed = $row->getFeedObject();
    	//<br>
    	$html = '<img class="img-thumbnail" style="max-width:100px" title="'.$feed->getLink().'" src="'.$feed->getChannelLogoUrl().'" alt="">';
    	return $html;    		
    }

	/**
	 * Renders grid column
	 *
	 * @param Varien_Object $row
	 * @return mixed
	 */
// 	public function _getValue(Varien_Object $row)
// 	{
// 		$html = parent::_getValue($row);
// 		$feed = $row->getFeedObject();
// 		$html .= '<br><img class="img-thumbnail" style="max-width:100px" title="'.$feed->getChannelLabel().'" src="'.$feed->getChannelLogoUrl().'" alt="">';
// 		return $html;
// 	}

}
