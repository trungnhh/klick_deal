<?php
/** 
 * Magento Module developed by NoStress Commerce 
 * 
 * NOTICE OF LICENSE 
 * 
 * This source file is subject to the Open Software License (OSL 3.0) 
 * that is bundled with this package in the file LICENSE.txt. 
 * It is also available through the world-wide-web at this URL: 
 * http://opensource.org/licenses/osl-3.0.php 
 * If you did of the license and are unable to 
 * obtain it through the world-wide-web, please send an email 
 * to info@nostresscommerce.cz so we can send you a copy immediately. 
 * 
 * @copyright Copyright (c) 2009 NoStress Commerce (http://www.nostresscommerce.cz) 
 * 
 */ 

/** 
 * 
 * @category Nostress 
 * @package Nostress_Nscexport
 */

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Grid_Renderer_Categorymapping extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Prepare link to display in grid
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
    	//$feed = $row->getFeedObject();	
    	$taxonomyCode = $row->getTaxonomyCode();//$feed->getTaxonomyCode();
    	if($taxonomyCode)
    	{	
    		$url = $this->getUrl('*/catalog_category/');
    		
    		$taxonomyFullName = $row->getTaxonomyName();
    		$type = $row->getTaxonomyType();
    		if(!empty($type))
    			$taxonomyFullName .= " - ".$row->getTaxonomyType();
    		$title =  Mage::helper('nscexport')->__('Set up %s Categories',$taxonomyFullName);
    		$label =  $row->getTaxonomyName()."<br>".Mage::helper('nscexport')->__('Categories');
    		$html = '<form><button  title="'.$title.'" onclick="event.stopPropagation();setLocation(\''.$url.'\');" type="button" >' .$label. '</button></form>';    		
    		return $html;
    	}
    	else
    		return  Mage::helper('nscexport')->__('Not Available');
    		
    }

}
