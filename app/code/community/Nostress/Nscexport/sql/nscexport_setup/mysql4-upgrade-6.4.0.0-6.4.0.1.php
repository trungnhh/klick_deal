<?php
/** 
* Magento Module developed by NoStress Commerce 
* 
* NOTICE OF LICENSE 
* 
* This source file is subject to the Open Software License (OSL 3.0) 
* that is bundled with this package in the file LICENSE.txt. 
* It is also available through the world-wide-web at this URL: 
* http://opensource.org/licenses/osl-3.0.php 
* If you did of the license and are unable to 
* obtain it through the world-wide-web, please send an email 
* to info@nostresscommerce.cz so we can send you a copy immediately. 
* 
* @copyright Copyright (c) 2009 NoStress Commerce (http://www.nostresscommerce.cz) 
* 
*/ 

/** 
* Sql update skript
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$this->startSetup()->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('nostress_export_cache_enginecategory')} (
  `entity_id` int(11) unsigned NOT NULL,   
  `taxonomy_code` varchar(255) NOT NULL,
  `locale` varchar(255) NOT NULL DEFAULT 'en_UK',
  `hash` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `id` BIGINT( 20 ) NULL DEFAULT '-1',
  `path` text CHARACTER SET utf8 DEFAULT '' NOT NULL,
  `ids_path` text CHARACTER SET utf8 DEFAULT '' NOT NULL, 
  `level` int(11) NOT NULL DEFAULT '-1', 
  `parent_name` varchar(255) DEFAULT '',
  `parent_id`  BIGINT( 20 ) NULL DEFAULT '-1',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
")->endSetup();

