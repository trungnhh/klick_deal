<?php
class Eb_Download_Model_Attribute_Source_Downloadtype extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = array(
                array(
                    'label' => Mage::helper('download')->__('Select allow download type'),
                    'value' =>  ''
                ),
                array(
                    'label' => Mage::helper('download')->__('Free Download'),
                    'value' =>  'free'
                ),
                array(
                    'label' => Mage::helper('download')->__('Login'),
                    'value' =>  'login'
                ),
                array(
                    'label' => Mage::helper('download')->__('Vip'),
                    'value' =>  'vip'
                ),
                array(
                    'label' => Mage::helper('download')->__('Code'),
                    'value' =>  'code'
                ),
            );
        }
        return $this->_options;
    }
}

?>