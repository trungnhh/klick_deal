<?php

class Eb_Download_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getDownloadallow($product)
    {
        $downloadType   =   $product->getData('allow_download_type');
        if ($downloadType!='') {
            switch ($downloadType) {
                case 'free':
                    return true;
                    break;
                case 'login':
                    if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                        return true;
                    } else {
                        return false;
                    }
                    break;
                case 'vip':

                    if (Mage::getSingleton('customer/session')->isLoggedIn()) {

                        $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
                        $groupCode  = Mage::getModel('customer/group')->load($groupId)->getCustomerGroupCode();

                        if (strtolower($groupCode)=='vip') {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                    break;
                case 'code':
                    return 'code';
                    break;
            }
        } else {
            return false;
        }

    }
}