<?php
$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('catalog_setup');
$installer->startSetup();

$setup->addAttribute('catalog_product', 'allow_download_type', array(
    'group' => 'General',
    'label'     => 'Allow Download Type',
    'type'              => 'varchar',
    'input'             => 'select',
    'frontend'          => '',
    'source'            => 'download/attribute_source_downloadtype',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => true,
    'required'          => false,
    'user_defined'      => true,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'visible_in_advanced_search' => false
));

$model = Mage::getModel('eav/entity_attribute')
    ->load($setup->getAttributeId('catalog_product', 'allow_download_type'));
$model
    ->setDefaultValue($model->getSource()->getOptionId(''))
    ->save();

$setup->addAttribute('catalog_product', "total_download", array(
    'group' => 'General',
    'type'       => 'int',
    'input'      => 'text',
    'label'      => 'Total download',
    'sort_order' => 1000,
    'required'          => false,
    'user_defined'      => true,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'visible_in_advanced_search' => false,
    'global'     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
));


$setup->addAttribute( 'catalog_category', 'is_download', array(
    'group' => 'Display Settings',
    'type' => 'int',
    'backend' => '',
    'frontend' => '',
    'label' => 'Download Category',
    'input' => 'select',
    'source' => 'eav/entity_attribute_source_boolean',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => true,
    'required' => false,
    'user_defined' => true,
    'default' => '',
    'unique' => false,
) );

$installer->endSetup();
