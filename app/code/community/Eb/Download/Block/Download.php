<?php
class Eb_Download_Block_Download extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getDownload()     
     { 
        if (!$this->hasData('download')) {
            $this->setData('download', Mage::registry('download'));
        }
        return $this->getData('download');
        
    }
}