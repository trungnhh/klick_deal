<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Discount
 *
 * @author Ea Design
 */
class EaDesign_PdfGenerator_Model_Entity_Totals_Discount extends Mage_Core_Model_Abstract
{

    /**
     * Get the variables and values acording to tax rules
     * @return array
     */
    public function getTotalsForDisplay()
    {
        $amount = $this->getAmount();
        $html = '';
        if ($amount != 0) {
            $amount = $this->getOrder()->formatPriceTxt($amount);
            $description = $this->getDescription();

            $html .= '<tr><td colspan="10">'. Mage::helper('tax')->__('Discount');
            if ($description != '') {
                $html .= ' ('.$description .')';
            }
            $html .= '</td><td align="right">'.$amount.'</td></tr>';
        }

        $totals = array(array(
            'discountammount' => array(
                'value' => $html,
                'label' => Mage::helper('tax')->__('Subtotal (Excl. Tax)') . ':',
            )));
        return $totals;
    }

    public function getAmount()
    {
        return $this->getSource()->getDataUsingMethod('discount_amount');
    }

    public function getDescription()
    {
        return $this->getSource()->getDataUsingMethod('discount_description');
    }

}
