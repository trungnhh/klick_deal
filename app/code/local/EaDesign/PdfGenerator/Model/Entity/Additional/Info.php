<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Info
 *
 * @author Ea Design
 */
class EaDesign_PdfGenerator_Model_Entity_Additional_Info extends EaDesign_PdfGenerator_Model_Entity_Pdfgenerator
{

    public function getStoreId()
    {
        return $this->getOrder()->getStoreId();
    }

    public function getTheInfoVariables()
    {
        $order = $this->getOrder();
        $store = Mage::app()->getStore($this->getStoreId());
        $image = Mage::getStoreConfig('sales/identity/logo', $this->getStoreId());
        $sourceDate = Mage::helper('core')->formatDate($this->getSource()->getCreatedAt(), 'long', false);
        $variables = array(
            'ea_logo_store' => array(
                'value' => '<img src="' . Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . '/sales/store/logo/' . $image . '" />',
            ),
            'ea_order_number' => array(
                'value' => $this->getOrder()->getRealOrderId(),
                'label' => Mage::helper('sales')->__('Order # %s')
            ),
            'ea_purcase_from_website' => array(
                'value' => $store->getWebsite()->getName(),
                'label' => Mage::helper('sales')->__('Purchased From')
            ),
            'ea_order_group' => array(
                'value' => $store->getGroup()->getName(),
                'label' => Mage::helper('pdfgenerator')->__('Purchased From Store')
            ),
            'ea_order_store' => array(
                'value' => $store->getName(),
                'label' => Mage::helper('sales')->__('Purchased From Website')
            ),
            'ea_order_status' => array(
                'value' => $this->getOrder()->getStatus(),
                'label' => Mage::helper('sales')->__('Order Status')
            ),
            'ea_source_date' => array(
                'value' => $sourceDate,
                'label' => Mage::helper('sales')->__('Order Date')
            ),
            'ea_order_totalpaid' => array(
                'value' => $order->formatPriceTxt($this->getOrder()->getTotalPaid()),
                'label' => Mage::helper('sales')->__('Total Paid')
            ),
            'ea_order_totalrefunded' => array(
                'value' => $order->formatPriceTxt($this->getOrder()->getTotalRefunded()),
                'label' => Mage::helper('sales')->__('Total Refunded')
            ),
            'ea_order_totaldue' => array(
                'value' => $order->formatPriceTxt($this->getOrder()->getTotalDue()),
                'label' => Mage::helper('sales')->__('Total Due')
            ),
        );

        return $variables;
    }

    public function getTheInvoiceVariables()
    {
        $invoice = $this->getSource();
        $createAtDate = Mage::helper('core')->formatDate($invoice->getCreatedAtDate(), 'long', false);
        //$tmp = explode('/',$createAtDate);
        //$createAtDateFormat = $tmp[1] .'/'. $tmp[0] .'/'. $tmp[2];
            $variables = array(
            'ea_invoice_id' => array(
                'value' => $invoice->getIncrementId(),
                'label' => Mage::helper('pdfgenerator')->__('Invoice Id'),
            ),
            'ea_invoice_status' => array(
                'value' => $invoice->getStateName(),
                'label' => Mage::helper('pdfgenerator')->__('Invoice Status'),
            ),
            'ea_invoice_date' => array(
                'value' => $createAtDate,
                'label' => Mage::helper('pdfgenerator')->__('Invoice Date'),
            ),
        );

        return $variables;
    }

    public function getTheCustomerVariables()
    {
        $order = $this->getSource()->getOrder();
        $store = Mage::app()->getStore($this->getStoreId());
        $customerId = $order->getCustomerId();
        $getCustomer = Mage::getModel('customer/customer')->load($customerId);
        $getCustomerGroup = Mage::getModel('customer/group')->load((int) $this->getOrder()->getCustomerGroupId())->getCode();

        $variables = array(
            'customer_name' => array(
                'value' => $order->getData('customer_lastname') . ' ' . $order->getData('customer_firstname'),
                'label' => Mage::helper('sales')->__('Customer Name'),
            ),
            'customer_email' => array(
                'value' => $order->getCustomerEmail(),
                'label' => Mage::helper('sales')->__('Email'),
            ),
            'customer_group' => array(
                'value' => $getCustomerGroup,
                'label' => Mage::helper('sales')->__('Customer Group'),
            ),
            'customer_firstname' => array(
                'value' => $getCustomer->getData('firstname'),
                'label' => Mage::helper('customer')->__('First Name'),
            ),
            'customer_lastname' => array(
                'value' => $getCustomer->getData('lastname'),
                'label' => Mage::helper('customer')->__('Last Name'),
            ),
            'customer_middlename' => array(
                'value' => $getCustomer->getData('middlename'),
                'label' => Mage::helper('customer')->__('Middle Name/Initial'),
            ),
            'customer_prefix' => array(
                'value' => $getCustomer->getData('prefix'),
                'label' => Mage::helper('customer')->__('Prefix'),
            ),
            'customer_suffix' => array(
                'value' => $getCustomer->getData('suffix'),
                'label' => Mage::helper('customer')->__('Suffix'),
            ),
            'customer_taxvat' => array(
                'value' => $getCustomer->getData('taxvat'),
                'label' => Mage::helper('customer')->__('Tax/VAT number'),
            ),
            'customer_dob' => array(
                'value' => Mage::helper('core')->formatDate($getCustomer->getData('dob'), 'long', false),
                'label' => Mage::helper('customer')->__('Date Of Birth'),
            ),
            'customer_id' => array(
                'value' => ($getCustomer->getId())?$getCustomer->getId(): 'NOT LOGGED IN',
                'label' => Mage::helper('customer')->__('ID'),
            ),
            'ccustomer_id' => array(
                'value' => ($getCustomer->getId()) ? '<p>Ihre Kunden-Nr.:&nbsp;'.$getCustomer->getId().'</p>' : '',
                'label' => Mage::helper('customer')->__('ID'),
            ),
        );

        return $variables;
    }

    public function getTheAddresInfo()
    {
        $order = $this->getOrder();
        $billingInfo = $order->getBillingAddress()->getName() . "<br />";
        if($order->getBillingAddress()->getCompany()!='') {
            $billingInfo .= $order->getBillingAddress()->getCompany() . "<br />";
        }
        $billingInfo .= $order->getBillingAddress()->getStreetFull() . "<br />";

        //Customize for MMS-99
        if ($order->getBillingAddress()->getCountryId() == 'US' || $order->getBillingAddress()->getCountryId() == 'GB') {
            $billingInfo .= $order->getBillingAddress()->getCity() .', '. $order->getBillingAddress()->getPostcode() . "<br />";
        } else {
            $billingInfo .= $order->getBillingAddress()->getPostcode() .' '. $order->getBillingAddress()->getCity() . "<br />";
        }

        $billingInfo .= Mage::app()->getLocale()->getCountryTranslation($order->getBillingAddress()->getCountry()) . "<br />";
        if ($order->getShippingAddress())
        {
            $shippingInfo = $order->getShippingAddress()->getFormated(true);
        } else
        {
            $shippingInfo = '';
        }
        $variables = array(
            'billing_address' => array(
                'value' => $billingInfo,
                'label' => Mage::helper('sales')->__('Billing Address'),
            ),
            'shipping_address' => array(
                'value' => $shippingInfo,
                'label' => Mage::helper('sales')->__('Shipping Address'),
            )
        );
        return $variables;
    }

    public function getThePaymentInfo()
    {
        $order = $this->getOrder();
        $paymentInfo = $order->getPayment()->getMethodInstance()->getTitle();

        $paymentMethod = $order->getPayment()->getMethod();
        $billing_method_text = Mage::helper('sales')->__('14Tg. 3%, 30 Tg. Netto');
        if (strpos($paymentMethod, 'saferpaycw') !== false) {
            $billing_method_text = Mage::helper('sales')->__('bereits bezahlt per Kreditkarte');
        } elseif (strpos($paymentMethod, 'paypal') !== false) {
            $billing_method_text = Mage::helper('sales')->__('bereits bezahlt per Paypal');
        } elseif (strpos($paymentMethod, 'pnsofortueberweisung') !== false) {
            $billing_method_text = Mage::helper('sales')->__('bereits bezahlt per Sofort-Überweisung');
        }

        $variables = array(
            'billing_method' => array(
                'value' => $paymentInfo,
                'label' => Mage::helper('sales')->__('Billing Method'),
            ),
            'billing_method_text' => array(
                'value' => $billing_method_text,
                'label' => Mage::helper('sales')->__('Billing Method Text'),
            ),
            'billing_method_currency' => array(
                'value' => $order->getOrderCurrencyCode(),
                'label' => Mage::helper('sales')->__('Order was placed using'),
            ),
        );
        return $variables;
    }

    public function getTheShippingInfo()
    {
        $order = $this->getOrder();


        if ($order->getShippingDescription())
        {
            $shippingInfo = $order->getShippingDescription();
        }
        else
        {
            $shippingInfo = '';
        }

        $variables = array(
            'shipping_method' => array(
                'value' => $shippingInfo,
                'label' => Mage::helper('sales')->__('Shipping Information'),
            ),
        );
        return $variables;
    }

    public function getTheInfoMergedVariables()
    {
        $vars = array_merge(
                $this->getTheInfoVariables()
                , $this->getTheCustomerVariables()
                , $this->getTheAddresInfo()
                , $this->getThePaymentInfo()
                , $this->getTheShippingInfo()
                , $this->getTheInvoiceVariables()
        );
        $processedVars = Mage::helper('pdfgenerator')->arrayToStandard($vars);

        return $processedVars;
    }

}
