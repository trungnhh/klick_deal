<?php

require_once(Mage::getModuleDir('controllers','Mage_Checkout').DS.'CartController.php');

class Pixxsel_ProductLinker_ProductController extends Mage_Checkout_CartController
{
	public function addAction()
    {
		$cart   = $this->_getCart();
        $params = $this->getRequest()->getParams();

        foreach ($params as $key => $value) {
            if (strpos($key, '@') !== false) {
                unset($params[$key]);
                $params['email'] = $key;
                break;
            }

            if (strpos($value, '@') !== false) {
                $params['email'] = $value;
                $params['store'] = $key;
                break;
            }
        }

        $params['qty'] = 1;
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $productId = (int)$params['item'];
	        if ($productId) {
	            $product = Mage::getModel('catalog/product')
	                ->setStoreId(Mage::app()->getStore()->getId())
	                ->load($productId);
	            if (!$product) {
	                $this->_goBack();
	                return;
	            }
	        }

            if ($product->getTypeId() == 'grouped') {
                $children = $product->getTypeInstance(true)->getAssociatedProducts($product);
                foreach ($children as $child) {
                    $child = Mage::getModel('catalog/product')
                        ->setStoreId(Mage::app()->getStore()->getId())
                        ->load($child->getId());
                    $cart->addProduct($child, $params);
                }
            } else {
                $cart->addProduct($product, $params);
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);
            if ($params['email']) {
                $this->_getSession()->setUserEmail($params['email']);
            } else {
                $this->_getSession()->unsUserEmail();
            }

            /**
             * @todo remove wishlist observer processAddToCart
             */
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                //$this->_goBack();
                $redirectUrl = Mage::getUrl('onepage');
                if ($params['store']) {
                    $redirectUrl = Mage::getUrl('onepage', array(
                        '_store' => $params['store'],
                        '_store_to_url' => true,
                    ));
                }
                $this->getResponse()->setRedirect($redirectUrl);
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }
    } 
}