<?php

class Pixxsel_ProductLinker_Model_Adminhtml_Observer
{
    public function addLinkToProductGrid(Varien_Event_Observer $observer)
    {  
        $block = $observer->getBlock();
        if (!isset($block)) return;

        if($block->getType() == 'adminhtml/catalog_product_grid'){
            /* @var $block Mage_Adminhtml_Block_Catalog_Product_Grid */
            $block->addColumn('product_linker', array(
                'header' => Mage::helper('productlinker')->__('External Url'),
                'index'  => 'entity_id',
                'renderer'  => 'Pixxsel_ProductLinker_Block_Adminhtml_Renderer_Url',
                'filter'    => false,
      			'sortable'  => false,
      			'width' 	=> '450px'
            ));
        }
    }  
}