<?php
class Mut_Catalog_Helper_Product extends Mage_Catalog_Helper_Product
{
    /**
     * Check if a product can be shown
     *
     * @param  Mage_Catalog_Model_Product|int $product
     * @return boolean
     */
    public function canShow($product, $where = 'catalog')
    {
        if (is_int($product)) {
            $product = Mage::getModel('catalog/product')->load($product);
        }

        /* @var $product Mage_Catalog_Model_Product */

        if (!$product->getId()) {
            return false;
        }

        return true;
    }

    public function isCampaign()
    {
        $customerSession = Mage::getSingleton('customer/session');
        $customerGroup = Mage::getModel('customer/group');
        $customerGroup->load($customerSession->getCustomerGroupId());
        if ($customerGroup->getCustomerGroupCode() == 'campaign') {
            $session = Mage::getSingleton('checkout/session');
            foreach ($session->getQuote()->getAllItems() as $item) {
                $priceDatas = Mage::getResourceModel('catalog/product_attribute_backend_groupprice')
                    ->loadPriceData($item->getProductId(), Mage::app()->getWebsite()->getId());
                if (!empty($priceDatas)) {
                    foreach ($priceDatas as $priceData) {
                        if (is_array($priceData) && $priceData['cust_group'] == $customerSession->getCustomerGroupId()) {
                            continue 2;
                        }
                    }
                    return false;
                } else {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}