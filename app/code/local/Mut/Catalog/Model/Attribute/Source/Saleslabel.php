<?php
class Mut_Catalog_Model_Attribute_Source_Saleslabel extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $this->_options = array(
                
                array(
                    'label' => Mage::helper('catalog')->__('None'),
                    'value' =>  'none'
                ),
                array(
                    'label' => Mage::helper('catalog')->__('Sale'),
                    'value' =>  'sale'
                ),
                array(
                    'label' => Mage::helper('catalog')->__('Deal'),
                    'value' =>  'deal'
                ),
                array(
                    'label' => Mage::helper('catalog')->__('Percent(%)'),
                    'value' =>  'percent'
                ),
                array(
                    'label' => Mage::helper('catalog')->__('Preistipp'),
                    'value' =>  'preistipp'
                ),
                array(
                    'label' => Mage::helper('catalog')->__('Custom label'),
                    'value' =>  'custom'
                ),
            );
        }
        return $this->_options;
    }
}

?>