<?php
/**
 * Mut Setup Attribute
 *
 * @category  Mut
 * @package   Mut_Catalog
 * @author    EB dev Team <hoang.vjet@gmail.com>
 * @copyright 2015 Ebluestore
 */


$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('catalog_setup');
$installer->startSetup();

$installer->removeAttribute('catalog_product', 'sales_label');
$installer->removeAttribute('catalog_product', 'sales_custom_label');

$attributeId = $installer->getAttributeId('catalog_product', 'sales_label');
if (!$attributeId) {
    $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'sales_label', array(
        'group'             => 'General',
        'type'              => 'varchar',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Sales Label',
        'input'             => 'select',
        'class'             => '',
        'source'            => 'Mut_Catalog_Model_Attribute_Source_Saleslabel',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => true,
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'used_in_product_listing' => true,
        'unique'            => false,
        'is_configurable'   => false,
    ));
}

$attributeId = $installer->getAttributeId('catalog_product', 'sales_custom_label');
if (!$attributeId) {
    // recurring payment profile
    $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'sales_custom_label', array(
        'group'             => 'General',
        'type'              => 'text',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Sales Custom Label',
        'input'             => 'text',
        'class'             => '',
        'source'            => '',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => true,
        'default'           => '',
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'used_in_product_listing'    => true,
        'unique'            => false,
        'is_configurable'   => false
    ));
}
$installer->endSetup();