<?php
/**
 * Mut Setup Attribute
 *
 * @category  Mut
 * @package   Mut_Catalog
 * @author    EB dev Team <hoang.vjet@gmail.com>
 * @copyright 2015 Ebluestore
 */


$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('catalog_setup');
$installer->startSetup();

$attributeId = $installer->getAttributeId('catalog_product', 'ebay_price');
if (!$attributeId) {
    $setup->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'ebay_price', array(
        'group'             => 'Prices',
        'type'              => 'decimal',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Ebay Price',
        'input'             => 'price',
        'class'             => '',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => true,
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'used_in_product_listing' => false,
        'unique'            => false,
        'is_configurable'   => false,
    ));
}

$installer->endSetup();