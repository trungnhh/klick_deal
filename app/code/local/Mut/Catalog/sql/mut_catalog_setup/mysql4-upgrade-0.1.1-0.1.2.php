<?php
/**
 * Mut Setup Attribute
 *
 * @category  Mut
 * @package   Mut_Catalog
 * @author    EB dev Team <hoang.vjet@gmail.com>
 * @copyright 2015 Ebluestore
 */


$installer = $this;
$installer->startSetup();

$installer->removeAttribute('catalog_product', 'sales_label');
$installer->removeAttribute('catalog_product', 'sales_custom_label');

$attributeId = $installer->getAttributeId('catalog_product', 'sales_label');
if (!$attributeId) {
    $installer->addAttribute('catalog_product', 'sales_label', array(
        'group' => 'General',
        'label'     => 'Sales Label',
        'type'              => 'varchar',
        'input'             => 'select',
        'frontend'          => '',
        'backend'           => '',
        'source'            => 'Mut_Catalog_Model_Attribute_Source_Saleslabel',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => true,
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => true,
        'unique'            => false,
        'is_configurable'   => false,
        'used_in_product_listing' => true,
        'visible_in_advanced_search' => false,
    ));
}

$attributeId = $installer->getAttributeId('catalog_product', 'sales_custom_label');
if (!$attributeId) {
    $installer->addAttribute('catalog_product', 'sales_custom_label', array(
        'group' => 'General',
        'input' => 'text',
        'type' => 'text',
        'label' => 'Sales custom label',
        'backend' => '',
        'frontend' => '',
        'visible' => true,
        'required' => false,
        'user_defined' => true,
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => true,
        'visible_in_advanced_search' => false,
        'is_html_allowed_on_front' => false,
        'used_in_product_listing'    => true,
        'is_configurable'   => false,
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    ));
}
$installer->endSetup();