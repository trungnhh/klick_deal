<?php
$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('catalog_setup');
$installer->startSetup();

$setup->addAttribute('catalog_product', 'amazon_buy_link', array(
    'group'                      => 'General',
    'type'                       => 'varchar',
    'input'                      => 'text',
    'label'                      => 'Amazon Buy Link',
    'sort_order'                 => 1001,
    'required'                   => false,
    'user_defined'               => true,
    'searchable'                 => false,
    'filterable'                 => false,
    'comparable'                 => false,
    'visible_on_front'           => false,
    'unique'                     => false,
    'visible_in_advanced_search' => false,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$setup->addAttribute('catalog_product', 'amazon_buy_only', array(
    'group'                      => 'General',
    'type'                       => 'int',
    'backend'                    => '',
    'frontend'                   => '',
    'label'                      => 'Display Only Amazon Button',
    'input'                      => 'select',
    'source'                     => 'eav/entity_attribute_source_boolean',
    'sort_order'                 => 1002,
    'required'                   => false,
    'user_defined'               => true,
    'searchable'                 => false,
    'filterable'                 => false,
    'comparable'                 => false,
    'visible_on_front'           => false,
    'unique'                     => false,
    'visible_in_advanced_search' => false,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'default'                    => '',
));

$setup->addAttribute('catalog_product', 'affiliate_buy_link', array(
    'group'                      => 'General',
    'type'                       => 'varchar',
    'input'                      => 'text',
    'label'                      => 'Affiliate Buy Link',
    'sort_order'                 => 1003,
    'required'                   => false,
    'user_defined'               => true,
    'searchable'                 => false,
    'filterable'                 => false,
    'comparable'                 => false,
    'visible_on_front'           => false,
    'unique'                     => false,
    'visible_in_advanced_search' => false,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$installer->endSetup();