<?php
class Mut_Reports_Block_Adminhtml_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $actionUrl = $this->getUrl('*/*/index');
        $form = new Varien_Data_Form(
            array('id' => 'filter_form', 'action' => $actionUrl, 'method' => 'get')
        );
        $htmlIdPrefix = 'admin_report_';
        $form->setHtmlIdPrefix($htmlIdPrefix);
        $fieldset = $form->addFieldset('base_fieldset', array('legend'=>Mage::helper('adminorderreport')->__('Filter')));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);

        $fieldset->addField('store_ids', 'hidden', array(
            'name'  => 'store_ids'
        ));

        $fieldset->addField('period', 'select', array(
            'name'      => 'period',
            'label'     => Mage::helper('adminorderreport')->__('Period'),
            'required' => true,
            'class'     => 'required-entry',
            'values'    => Mage::helper('adminorderreport')->getOrderPeriod(),
        ));

        $fieldset->addField('from', 'date', array(
            'name'      => 'from',
            'format'    => $dateFormatIso,
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            'label'     => Mage::helper('adminorderreport')->__('From'),
            'title'     => Mage::helper('adminorderreport')->__('From'),
            'required'  => true
        ));

        $fieldset->addField('to', 'date', array(
            'name'      => 'to',
            'format'    => $dateFormatIso,
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            'label'     => Mage::helper('adminorderreport')->__('To'),
            'title'     => Mage::helper('adminorderreport')->__('To'),
            'required'  => true
        ));

        $fieldset->addField('countries', 'multiselect', array(
            'name' => 'countries[]',
            'label' => 'Länder',
            'title' => 'Länder',
            'class' => 'required-entry',
            'required' => true,
            'index' => 'countries',
            'values' => Mage::helper('adminorderreport')->getCountryList(),
        ));

        $fieldset->addField('order_status', 'multiselect', array(
            'name'      => 'order_status[]',
            'label'     => Mage::helper('adminorderreport')->__('Status der Bestellung'),
            'title'     => Mage::helper('adminorderreport')->__('Status der Bestellung'),
            'class'     => 'required-entry',
            'required'  => true,
            'index'     => 'order_status',
            'values'    => Mage::helper('adminorderreport')->orderStatusToOptionArray(),
        ));

        $fieldset->addField('order_fom', 'select', array(
            'name'      => 'order_fom',
            'label'     => Mage::helper('adminorderreport')->__('Order from'),
            'required' => true,
            'class'     => 'required-entry',
            'values'    => Mage::helper('adminorderreport')->getOrderFromList(),
        ));

        $fieldset->addField('product_type', 'select', array(
            'name'      => 'product_type',
            'label'     => Mage::helper('adminorderreport')->__('Product Type'),
            'required' => true,
            'class'     => 'required-entry',
            'values'    => Mage::helper('adminorderreport')->getProductTypes(),
        ));

        $fieldset->addField('payment_method', 'multiselect', array(
            'name'      => 'payment_method',
            'label'     => Mage::helper('adminorderreport')->__('Payment Method'),
            'required'  => true,
            'class'     => 'required-entry',
            'index'     => 'payment_method',
            'values'    => Mage::helper('adminorderreport')->getPaymentMethods(),
        ));

        $fieldset->addField('sort_by', 'select', array(
            'name'      => 'sort_by',
            'label'     => Mage::helper('adminorderreport')->__('Sort By'),
            'required' => true,
            'class'     => 'required-entry',
            'values'    => Mage::helper('adminorderreport')->getSortTypes(),
        ));

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

    protected function _initFormValues()
    {
        $data = $this->getFilterData()->getData();
        foreach ($data as $key => $value) {
            if (is_array($value) && isset($value[0])) {
                $data[$key] = explode(',', $value[0]);
            }
        }
        $this->getForm()->addValues($data);
        return parent::_initFormValues();
    }
}