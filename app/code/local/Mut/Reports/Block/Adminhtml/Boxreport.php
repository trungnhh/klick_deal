<?php

class Mut_Reports_Block_Adminhtml_Boxreport extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->setId('developmentContainer');
        $this->_blockGroup = 'Reports';
        $this->_controller = 'adminhtml';
        $this->_mode = 'development';
        $this->_headerText = Mage::helper('adminhtml')->__('Box-Export');
        $this->removeButton('back');
        $this->removeButton('reset');
        $this->removeButton('delete');
        $this->removeButton('add');
        $this->removeButton('save');
        $this->removeButton('edit');
        /*$this->_addButton('show_report', array(
            'label'     => Mage::helper('adminhtml')->__('Show Reports'),
            'onclick'   => 'filterFormSubmit()',
        ), -1);
        /*
        $this->_addButton('print_pdf', array(
            'label'     => Mage::helper('adminhtml')->__('Print PDF'),
            'onclick'   => 'setLocation(\''. $this->getPrintpdfUrl() .'\')',
        ), -1);
        */
        $this->_addButton('export_csv', array(
            'label'     => Mage::helper('adminhtml')->__('Export CSV'),
            'onclick'   => 'filterFormExport()',
        ), -1);
    }

    public function getFilterUrl()
    {
        $this->getRequest()->setParam('filter', null);
        return $this->getUrl('*/*/box', array('_current' => true));
    }

    public function getPrintpdfUrl()
    {
        return $this->getUrl('*/*/printboxpdf', array('_current' => true));
    }

    public function getExportCsvUrl()
    {
        return $this->getUrl('*/*/exportboxcsv', array('_current' => true));
    }
}