<?php

class Mut_Reports_Block_Adminhtml_Boxresult extends Mage_Core_Block_Template
{
    public function getFilterResult()
    {

        $filterData = $this->getFilterData()->getData();
        if (empty($filterData)) return array();

        $from = $filterData['from'] . " 00:00:00";
        $to = $filterData['to'] . " 23:59:59";
        $fromGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $from);
        $toGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $to);
        $countries = $filterData['countries'][0];
        $orderFrom = $filterData['order_fom'];

        $countryWithinSlash = "'" . str_replace(",", "','", $countries) . "'";
        $status = $filterData['order_status'][0];
        $statusWithinSlash = "'" . str_replace(",", "','", $status) . "'";
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "
SELECT `order`.`created_at`, `order`.`entity_id`, `order`.`remote_ip`, `order`.`order_type`,`address`.`company`, `address`.`telephone`, `address`.`postcode`, `address`.`postnumber`, `address`.`street`, `address`.`city`, `address`.`prefix`, `address`.`firstname`, `address`.`lastname`, `address`.`email`, `order`.`increment_id`, `payment`.`method`, `order`.`grand_total`, `order`.`tax_amount`, `address`.`country_id`, `order`.`subtotal`, `order`.`subtotal_incl_tax`
FROM  sales_flat_order `order`
LEFT JOIN sales_flat_order_address `address` ON `order`.`billing_address_id` = `address`.`entity_id`
LEFT JOIN sales_flat_order_payment `payment` ON `order`.`entity_id` = `payment`.`parent_id`
WHERE `order`.`created_at` >= '{$fromGMT}' AND `order`.`created_at` <= '{$toGMT}'
";

        $type = $filterData['type'];
        if($type != 'all'){
            $sql .= " AND `order`.`order_type` like '{$type}%'";
        }

        if($orderFrom == 'shop'){
            $sql .= " AND `payment`.`method` != 'm2epropayment'";
        }elseif($orderFrom == 'ebay'){
            $sql .= " AND `payment`.`method` = 'm2epropayment'";
        }

        if (strpos($countries, "all") === false) {
            $sql .= " AND `address`.`country_id` IN ({$countryWithinSlash})";
        }

        if (strpos($status, "all") === false) {
            $sql .= " AND `order`.`status` IN ({$statusWithinSlash})";
        }

        $sql .= " ORDER BY created_at ASC";
        $orders = $readConnection->fetchAll($sql);

        $result = array();
        $result['from'] = date('d.m.Y', strtotime($from));
        $result['to'] = date('d.m.Y', strtotime($to));

        $countryArray = array();
        if (strpos($countries, "all") === false) {
            $result['land'] = $this->_countriesToLand($countries);
            $countryArray = explode(",", $countries);
        } else {
            $result['land'] = "Alle";
            $countryList = Mage::helper('adminorderreport')->getCountryList();
            array_shift($countryList);
            foreach ($countryList as $country) {
                $countryArray[] = $country['value'];
            }
        }

        $currentStoreId = Mage::app()->getStore()->getId();
        foreach ($orders as $order) {
            if($order['method'] == 'm2epropayment'){
                $order['payment_method'] = Mage::helper('adminorderreport')->__('eBay');
            }else{
                $order['payment_method'] = strtok(trim(Mage::helper('payment')->getMethodInstance($order['method'])->getTitle()), " ");
            }

            $order['tax_percent'] = round(($order['tax_amount'] * 100) / ($order['subtotal_incl_tax'] - $order['tax_amount'])) . "%";
            foreach ($countryArray as $countryCode) {
                if(!isset($paymentValue[$countryCode])){
                    $paymentValue[$countryCode] = array();
                }
                $type = explode('-', $order['order_type']);
                $orderType = $type[1];
                $orderTypeString = $type[0];
                if($orderTypeString == 'box+download'){
                    $orderTypeString = 'mixed';
                }

                if ($order['method'] == 'm2epropayment') {
                    $orderType = 'ebay';
                }
                $order['department'] = '';

                $countryName = (string) Mage::app()->getLocale()->getCountryTranslation($countryCode);
                if ($order['country_id'] == $countryCode) {
                    $orderMonth = Mage::app()->getLocale()->storeDate(
                        $currentStoreId,
                        Varien_Date::toTimestamp($order['created_at']),
                        true
                    )->toString("MM.YYYY");
                    $order['order_type_string'] = $orderTypeString;
                    $result['orders'][$countryName]['orders'][$orderMonth]['orders'][] = $order;
                    $result['orders'][$countryName]['orders'][$orderMonth]['total'] += $order['grand_total'];
                    $result['orders'][$countryName]['orders'][$orderMonth]['total_tax'] += $order['tax_amount'];
                    $result['orders'][$countryName]['orders'][$orderMonth]['order_type'][$orderType]['total'] += $order['grand_total'];
                    $result['orders'][$countryName]['orders'][$orderMonth]['order_type'][$orderType]['total_tax'] += $order['tax_amount'];
                    $result['orders'][$countryName]['total'] += $order['grand_total'];
                    $result['orders'][$countryName]['total_tax'] += $order['tax_amount'];
                    $result['orders'][$countryName]['order_type'][$orderType]['total'] += $order['grand_total'];
                    $result['orders'][$countryName]['order_type'][$orderType]['total_tax'] += $order['tax_amount'];
                    $result['total'] += $order['grand_total'];
                    $result['total_tax'] += $order['tax_amount'];
                    $result['order_type'][$orderType]['total'] += $order['grand_total'];
                    $result['order_type'][$orderType]['total_tax'] += $order['tax_amount'];
                }
            }
        }

        return $result;
    }

    protected function _countriesToLand($countries)
    {
        $codeList = explode(",", $countries);
        $land = array();
        foreach ($codeList as $countryCode) {
            $land[] = Mage::app()->getLocale()->getCountryTranslation($countryCode);
        }
        return implode(",", $land);
    }

    public function getCsv()
    {
        $filterData = $this->getFilterData()->getData();
        if (empty($filterData)) return array();

        $from = $filterData['from'] . " 00:00:00";
        $to = $filterData['to'] . " 23:59:59";
        $fromGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $from);
        $toGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $to);
        $countries = $filterData['countries'][0];
        $orderFrom = $filterData['order_fom'];

        $countryWithinSlash = "'" . str_replace(",", "','", $countries) . "'";
        $status = $filterData['order_status'][0];
        $statusWithinSlash = "'" . str_replace(",", "','", $status) . "'";
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "
SELECT `order`.`created_at`, `order`.`order_type`, `address`.`lastname`, `address`.`email`, `order`.`increment_id`, `payment`.`method`, `order`.`grand_total`, `order`.`tax_amount`, `address`.`country_id`, `order`.`subtotal`, `order`.`subtotal_incl_tax`
FROM  sales_flat_order `order`
LEFT JOIN sales_flat_order_address `address` ON `order`.`billing_address_id` = `address`.`entity_id`
LEFT JOIN sales_flat_order_payment `payment` ON `order`.`entity_id` = `payment`.`parent_id`
WHERE `order`.`created_at` >= '{$fromGMT}' AND `order`.`created_at` <= '{$toGMT}'
";

        $type = $filterData['type'];
        if($type != 'all'){
            $sql .= " AND `order`.`order_type` like '{$type}%'";
        }

        if($orderFrom == 'shop'){
            $sql .= " AND `payment`.`method` != 'm2epropayment'";
        }elseif($orderFrom == 'ebay'){
            $sql .= " AND `payment`.`method` = 'm2epropayment'";
        }

        if (strpos($countries, "all") === false) {
            $sql .= " AND `address`.`country_id` IN ({$countryWithinSlash})";
        }

        if (strpos($status, "all") === false) {
            $sql .= " AND `order`.`status` IN ({$statusWithinSlash})";
        }

        $sql .= " ORDER BY created_at ASC";
        $orders = $readConnection->fetchAll($sql);

        /*
        $csvHeader = array(
            'Firma',
            'Abteilung',
            'Anrede',
            'Name Id',
            'Vorname',
            'Strasse+Nr',
            'Postfach',
            'Land',
            'PLZ',
            'Ort',
            'Telefonnummer',
            'E-Mail',
            'Gesamtpreis',
            'Datum+Zeit',
            'IP+Porto[+"*"+]4',
            'Artikelnummer&Anzahl&Preis'
        );
        */

        $csvHeader = array(
            'orderid',
            'zusatz1',
            'zusatz2',
            'anrede',
            'name',
            'vorname',
            'strasse',
            'freie_kenn',
            'lkz',
            'plz',
            'ort',
            'telefon',
            'e_mail',
            'gesamt',
            'datum',
            'ip',
            //'shopnummer',
            'liefer_zusatz1',
            'liefer_zusatz2',
            'liefer_anrede',
            'liefer_name',
            'liefer_vorname',
            'liefer_strasse',
            'liefer_lkz',
            'liefer_plz',
            'liefer_ort'
        );

        //@TODO if need use get csv
        $csv = $this->_getCsvRow($csvHeader);
        $_coreHelper = $this->helper('core');
        foreach ($orders as $order) {
            $tmp = array();
            $tmp['date'] = Mage::app()->getLocale()->storeDate(
                Mage::app()->getStore()->getId(),
                Varien_Date::toTimestamp($order['created_at']),
                true
            )->toString("dd.MM.YYYY");
            $tmp['lastname'] = $order['lastname'];
            $tmp['email'] = $order['email'];
            $tmp['order_id'] = $order['increment_id'];

            $type = explode('-', $order['order_type']);
            $orderTypeString = $type[0];
            if ($orderTypeString == 'box+download') {
                $orderTypeString = 'mixed';
            }
            $tmp['art'] = $orderTypeString;

            if($order['method'] == 'm2epropayment'){
                $tmp['payment_method'] = Mage::helper('adminorderreport')->__('eBay');
            }else{
                $tmp['payment_method'] = strtok(trim(Mage::helper('payment')->getMethodInstance($order['method'])->getTitle()), " ");
            }

            $tmp['order_total'] = number_format($_coreHelper->currency($order['grand_total'], false, false), 2, ',', '.');
            $tmp['tax_percent'] = round(($order['tax_amount'] * 100) / ($order['subtotal_incl_tax'] - $order['tax_amount'])) . "%";
            $tmp['tax_amount'] = number_format($_coreHelper->currency($order['tax_amount'], false, false), 2, ',', '.');
            $csv .= $this->_getCsvRow($tmp);
        }
        return $csv;
    }

    protected function _getCsvRow($dataArray)
    {
        $csvRowData = array();
        foreach ($dataArray as $item) {
            $csvRowData[] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'), $item) . '"';
        }
        return implode(",", $csvRowData) . "\n";
    }

    public function exportCsv()
    {
        $filterData = $this->getFilterData()->getData();
        if (empty($filterData)) return array();

        $from = $filterData['from'] . " 00:00:00";
        $to = $filterData['to'] . " 23:59:59";
        $fromGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $from);
        $toGMT = Mage::getSingleton('core/date')->gmtDate("Y-m-d H:i:s", $to);
        $countries = $filterData['countries'][0];
        $orderFrom = $filterData['order_fom'];

        $countryWithinSlash = "'" . str_replace(",", "','", $countries) . "'";
        $status = $filterData['order_status'][0];
        $statusWithinSlash = "'" . str_replace(",", "','", $status) . "'";
        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $sql = "
SELECT `order`.`created_at`, `order`.`entity_id`, `order`.`remote_ip`, `order`.`order_type`, `order`.`shipping_address_id`, `address`.`company`, `address`.`telephone`, `address`.`postcode`, `address`.`postnumber`, `address`.`street`, `address`.`city`, `address`.`prefix`, `address`.`firstname`, `address`.`lastname`, `address`.`email`, `order`.`increment_id`, `payment`.`method`, `order`.`grand_total`, `order`.`tax_amount`, `address`.`country_id`, `order`.`subtotal`, `order`.`subtotal_incl_tax`
FROM  sales_flat_order `order`
LEFT JOIN sales_flat_order_address `address` ON `order`.`billing_address_id` = `address`.`entity_id`
LEFT JOIN sales_flat_order_payment `payment` ON `order`.`entity_id` = `payment`.`parent_id`
WHERE `order`.`created_at` >= '{$fromGMT}' AND `order`.`created_at` <= '{$toGMT}'
";

        $type = $filterData['type'];
        if($type != 'all'){
            $sql .= " AND `order`.`order_type` like '{$type}%'";
        }

        if($orderFrom == 'shop'){
            $sql .= " AND `payment`.`method` != 'm2epropayment'";
        }elseif($orderFrom == 'ebay'){
            $sql .= " AND `payment`.`method` = 'm2epropayment'";
        }

        if (strpos($countries, "all") === false) {
            $sql .= " AND `address`.`country_id` IN ({$countryWithinSlash})";
        }

        if (strpos($status, "all") === false) {
            $sql .= " AND `order`.`status` IN ({$statusWithinSlash})";
        }

        $sql .= " ORDER BY created_at ASC";
        $orders = $readConnection->fetchAll($sql);
        /*
        $csvHeader = array(
            'Firma',
            'Abteilung',
            'Anrede',
            'Name Id',
            'Vorname',
            'Strasse+Nr',
            'Postfach',
            'Land',
            'PLZ',
            'Ort',
            'Telefonnummer',
            'E-Mail',
            'Gesamtpreis',
            'Datum+Zeit',
            'IP+Porto[+"*"+]4',
            'Artikelnummer&Anzahl&Preis'
        );
        $csv .= implode(';', array(
                $order['company'],
                $order['department'],
                $order['prefix'],
                $order['firstname'],
                $order['lastname'],
                $order['street'],
                $order['postnumber'],
                $this->getCountry($order['country_id']),
                $order['postcode'],
                $order['city'],
                $order['telephone'],
                $order['email'],
                $total,
                $date,
                $order['remote_ip'].'[+"*"+]4',
                $this->getOrderSkus($order['entity_id'])
            ))."\n";
        */

        $csvHeader = array(
            'orderid',
            'zusatz1',
            'zusatz2',
            'anrede',
            'name',
            'vorname',
            'strasse',
            'freie_kenn',
            'lkz',
            'plz',
            'ort',
            'telefon',
            'e_mail',
            'gesamt',
            'datum',
            'ip',
            //'shopnummer',
            'liefer_zusatz1',
            'liefer_zusatz2',
            'liefer_anrede',
            'liefer_name',
            'liefer_vorname',
            'liefer_strasse',
            'liefer_lkz',
            'liefer_plz',
            'liefer_ort'
        );
        $_coreHelper = $this->helper('core');

        $csv = implode(';', $csvHeader)."\n";

        foreach ($orders as $order) {
            $date = Mage::app()->getLocale()->storeDate(
                Mage::app()->getStore()->getId(),
                Varien_Date::toTimestamp($order['created_at']),
                true
            )->toString("dd.MM.YYYY");

            $total = number_format($_coreHelper->currency($order['grand_total'], false, false), 2, ',', '.');
            // use billing
            $lieferAnrede = '';//$order['prefix'];
            $lieferFirstName = '';//$order['firstname'];
            $lieferLastName = '';//$order['lastname'];
            $lieferStrasse = '';//$order['street'];
            $lieferLkz = '';//$this->getCountry($order['country_id']);
            $lieferPlz = '';//$order['postcode'];
            $lieferOrt = '';//$order['city'];
            $shipCompany = '';
            if ($order['shipping_address_id']) {
                // use sql get shipping address
                $sql = "SELECT * FROM sales_flat_order_address WHERE entity_id=".$order['shipping_address_id'];
                $result = $readConnection->fetchRow($sql);
                // use shipping
                if (count($result) && $order['street'] != $result['street']) {
                    $lieferAnrede = $result['prefix'];
                    $lieferFirstName = $result['firstname'];
                    $lieferLastName = $result['lastname'];
                    $lieferStrasse = $result['street'];
                    $lieferLkz = $this->getCountry($result['country_id']);
                    $lieferPlz = $result['postcode'];
                    $lieferOrt = $result['city'];
                    $shipCompany = $result['company'];
                }
            }
            $csv .= implode(';', $this->parseArrayCsv(array(
                    $order['increment_id'],
                    $order['company'],// zusatz1 no info temporary use company
                    '',// zusatz2 no info
                    $order['prefix'],
                    $order['firstname'],
                    $order['lastname'],
                    $order['street'],
                    $order['postnumber'], //  freie_kenn no info temporary using postnumber
                    $this->getCountry($order['country_id']),
                    $order['postcode'],
                    $order['city'],
                    $order['telephone'],
                    $order['email'],
                    $total,
                    $date,
                    $order['remote_ip'],
                    //$order['company'], //Shopnumber no info temporary use company
                    $shipCompany, // liefer_zusatz1 no info
                    '', //  liefer_zusatz2  no info
                    $lieferAnrede, // liefer_anrede
                    $lieferFirstName,
                    $lieferLastName,
                    $lieferStrasse,
                    $lieferLkz,
                    $lieferPlz,
                    $lieferOrt,
                    $this->getOrderSkus($order['entity_id'])
                )))."\n";
        }

        return mb_convert_encoding($csv, 'ISO-8859-1', 'UTF-8');
    }

    public function getCountry($countryId)
    {
        $country = array(
            'DE' => 'D',
            'AT' => 'A',
            'CH' => 'CH'
        );
        if (isset($country[$countryId])) {
            return $country[$countryId];
        }
        return $countryId;
    }

    public function getOrderSkus($orderId)
    {
        $order = Mage::getModel('sales/order')->load($orderId);
        $skus = array();
        if ($order->getId()) {
            foreach ($order->getAllVisibleItems() as $item) {
                $skus[]= $item->getSku().'&'.(int)$item->getQtyOrdered().'&'.number_format($item->getPriceInclTax(), 2);
            }
            return implode(';', $skus);
        }
        return '';
    }

    /**
     * Parse array Csv
     *
     * @param $arr
     * @return array
     */
    public function parseArrayCsv($arr)
    {
        $newArr = array();
        foreach ($arr as $value) {
            $newArr[] = $this->removeEscapeString($value);
        }
        return $newArr;
    }

    /**
     * Parse string to csv string
     *
     * @param $field
     * @return
     */
    public function  removeEscapeString($field)
    {
        $field = str_replace("\r", "", $field);
        $field = str_replace("\n", "", $field);
        $field = str_replace('""', "", $field);
        return $field;
    }
}