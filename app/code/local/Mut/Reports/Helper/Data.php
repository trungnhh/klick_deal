<?php

class Mut_Reports_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getCountryList()
    {
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $list = $readConnection->fetchAll("SELECT DISTINCT country_id FROM `sales_flat_order_address` WHERE country_id IS NOT NULL");
        $countries = array();
        $countries[] = array(
            'label' => 'Alle',
            'value' => 'all',
        );
        foreach ($list as $item) {
            $countryCode = $item['country_id'];
            $countryName = Mage::app()->getLocale()->getCountryTranslation($countryCode);
            $countries[] = array(
                'label' => $countryName,
                'value' => $countryCode,
            );
        }
        return $countries;
    }

    public function orderTypeToArray()
    {
        return array(
            array('label' => 'alle', 'value' => 'all'),
            array('label' => 'nur downloads', 'value' => 'download'),
            array('label' => 'nur box', 'value' => 'box'),
        );
    }

    public function getProductTypes()
    {
        $productTypes = array(
            array(
                'label' => $this->__('Any'),
                'value' => 'all',
            ),
            array(
                'label' => $this->__('Physical products'),
                'value' => 'physical_products',
            ),
            array(
                'label' => $this->__('Physical books'),
                'value' => 'physical_books',
            ),
            array(
                'label' => $this->__('Download products'),
                'value' => 'download_products',
            )
        );

        return $productTypes;
    }

    public function getPaymentMethods()
    {
        $productTypes = array(
            array(
                'label' => $this->__('Rechnung'),
                'value' => 'checkmo',
            ),
            array(
                'label' => $this->__('Paypal Express'),
                'value' => 'paypal_express',
            ),
            array(
                'label' => $this->__('Sofort'),
                'value' => 'paymentnetwork_pnsofortueberweisung',
            ),
            array(
                'label' => $this->__('Invoice'),
                'value' => 'vaimo_klarna_invoice',
            ),
            array(
                'label' => $this->__('Saferpay'),
                'value' => 'saferpay',
            )
        );

        return $productTypes;
    }

    public function getSortTypes()
    {
        $sortTypes = array(
            array(
                'label' => $this->__('Product Name (A-Z)'),
                'value' => 'name',
            ),
            array(
                'label' => $this->__('Bestellsumme (9-0)'),
                'value' => 'price',
            ),
            array(
                'label' => $this->__('Stück (9-0)'),
                'value' => 'qty_ordered',
            )
        );

        return $sortTypes;
    }

    public function getOrderFromList()
    {
        $fromList = array(
            array(
                'label' => $this->__('any order'),
                'value' => 'all',
            ),
            array(
                'label' => $this->__('shop only'),
                'value' => 'shop',
            ),
            array(
                'label' => $this->__('eBay only'),
                'value' => 'ebay',
            )
        );
        return $fromList;
    }

    public function getOrderPeriod()
    {
        $fromList = array(
            array(
                'label' => $this->__('Custom'),
                'value' => 'custom',
            ),
            array(
                'label' => $this->__('This week'),
                'value' => 'this_week',
            ),
            array(
                'label' => $this->__('Last Week'),
                'value' => 'last_week',
            ),
            array(
                'label' => $this->__('This Month'),
                'value' => 'this_month',
            ),
            array(
                'label' => $this->__('Last Month'),
                'value' => 'last_month',
            ),
            array(
                'label' => $this->__('This Year'),
                'value' => 'this_year',
            ),
            array(
                'label' => $this->__('Last Year'),
                'value' => 'last_year',
            )
        );
        return $fromList;
    }

    /**
     * Get Custom Date
     *
     * @param $type
     * @param $range
     * @return bool|string
     */
    public function getDate($type, $range)
    {
        // last year
        if ($type == 'last_year') {
            if ($range == 'from') {
                return date("Y-01-01", strtotime("-1 year"));
            } else {
                $firstDay = strtotime(date('Y-01-01').'- 1day');
                return date("Y-m-d", $firstDay);
            }
        }

        // this year
        if ($type == 'this_year') {
            if ($range == 'from') {
                $firstDay = strtotime(date('Y-01-01'));
                return date("Y-m-d", $firstDay);
            } else {
                $currentDate = strtotime(now());
                return date("Y-m-d", $currentDate);
            }
        }

        // last month
        if ($type == 'last_month') {
            if ($range == 'from') {
                $firstDay = strtotime("first day of last month");
                return date("Y-m-d", $firstDay);
            } else {
                $lastDay = strtotime("last day of last month");
                return date("Y-m-d", $lastDay);
            }
        }

        // this month
        if ($type == 'this_month') {
            if ($range == 'from') {
                $firstDay = strtotime("first day of this month");
                return date("Y-m-d", $firstDay);
            } else {
                $currentDate = strtotime(now());
                return date("Y-m-d", $currentDate);
            }
        }

        // last week
        if ($type == 'last_week') {
            if ($range == 'from') {
                $firstDay = strtotime("monday last week");
                return date("Y-m-d", $firstDay);
            } else {
                $lastDay = strtotime("sunday last week");
                return date("Y-m-d", $lastDay);
            }
        }

        // this week
        if ($type == 'this_week') {
            if ($range == 'from') {
                $firstDay = strtotime("monday this week");
                return date("Y-m-d", $firstDay);
            } else {
                $lastDay = strtotime(now());
                return date("Y-m-d", $lastDay);
            }
        }
    }

    public function orderStatusToOptionArray()
    {
        $statuses = Mage::getModel('sales/order_status')->getCollection();
        if($statuses->count() > 0){
            $result = array();
            $result[] = array(
                'label' => $this->__('Alle'),
                'value' => 'all',
            );

            foreach($statuses as $item){
                $result[] = array(
                    'label' => $item->getLabel(),
                    'value' => $item->getStatus(),
                );
            }
            return $result;
        }
        return array();
    }
}