<?php

class Mut_Checkout_Model_Observer
{
    public function customAddressFormat($observer)
    {
        $address = $observer->getEvent()->getAddress();
        if($address->getData('is_packstation')!=null && $address->getData('is_packstation')!=0){
            $address->setStreet(array(0 => "Postnummer: ". $address->getData('postnumber'). ' Packstation: '.$address->getData('packstation')));
        }

        if ($address->getData('country_id') == 'US' || $address->getData('country_id') == 'GB') {
            $renderer = $observer->getEvent()->getType()->getRenderer()->getType();
            $defaultFormat = $renderer->getDefaultFormat();
            $defaultFormat = str_replace("{{if postcode}}{{var postcode}}{{/if}} {{if city}}{{var city}} {{/if}}{{if region}}{{var region}}{{/if}}", "{{if city}}{{var city}},{{/if}} {{if postcode}}{{var postcode}}{{/if}}", $defaultFormat);
            $renderer->setDefaultFormat($defaultFormat);
        }
        return $address;
    }

    public function updateCustomerGroup($observer)
    {
        $request = $observer->getEvent()->getControllerAction()->getRequest();
        if ($request->getParam('campaign') == 2019) {
            $targetGroup = Mage::getModel('customer/group');
            $targetGroup->load('campaign', 'customer_group_code');
            $customerSession = Mage::getSingleton('customer/session');
            $customerSession->setCustomerGroupId((int)$targetGroup->getId());
        }
    }
}