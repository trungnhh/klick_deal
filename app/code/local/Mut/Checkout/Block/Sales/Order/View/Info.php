<?php
/**
 * Mut Checkout
 *
 * @category  Mut
 * @package   Mut_Checkout
 * @author    EB dev Team <hoang.vjet@gmail.com>
 * @copyright 2015 Ebluestore
 */
class Mut_Checkout_Block_Sales_Order_View_Info extends Mage_Adminhtml_Block_Sales_Order_View_Info{
    /**
     * Retrieve required options from parent
     */
    protected function _beforeToHtml()
    {
        if (!$this->getParentBlock()) {
            Mage::throwException(Mage::helper('adminhtml')->__('Invalid parent block for this block.'));
        }
        $this->setTemplate('mut/checkout/sales/order/view/info.phtml');
        $this->setOrder($this->getParentBlock()->getOrder());

        foreach ($this->getParentBlock()->getOrderInfoData() as $k => $v) {
            $this->setDataUsingMethod($k, $v);
        }

        parent::_beforeToHtml();
    }
}
?>