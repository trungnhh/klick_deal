<?php
/**
 * Mut Setup Attribute
 *
 * @category  Mut
 * @package   Mut_Catalog
 * @author    EB dev Team <hoang.vjet@gmail.com>
 * @copyright 2015 Ebluestore
 */


$installer = $this;
$installer->startSetup();

$sales_quote_address = $installer->getTable('sales/quote_address');
$installer->getConnection()
    ->addColumn($sales_quote_address, 'is_packstation', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'Address is packstation'
    ));

/**
 * Adding Extra Column to sales_flat_order_address
 * to store the is_packstation field
 */
$sales_order_address = $installer->getTable('sales/order_address');
$installer->getConnection()
    ->addColumn($sales_order_address, 'is_packstation', array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'comment' => 'Address is packstation'
    ));

$installer->endSetup();