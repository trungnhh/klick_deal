<?php
/**
 * Mut Setup Attribute
 *
 * @category  Mut
 * @package   Mut_Catalog
 * @author    EB dev Team <hoang.vjet@gmail.com>
 * @copyright 2015 Ebluestore
 */


$installer = $this;
$installer->startSetup();
$setup = new Mage_Customer_Model_Resource_Setup();
$entityTypeId = $setup->getEntityTypeId('customer_address');
$attributeSetId = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

//this is for creating a new attribute for customer address entity
$setup->addAttribute("customer_address", "postnumber", array(
    "type" => "varchar",
    "backend" => "",
    "label" => "Postnumber",
    "input" => "text",
    "source" => "",
    "visible" => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique" => false,
));

$attribute = Mage::getSingleton("eav/config")->getAttribute("customer_address", "postnumber");

$setup->addAttributeToGroup(
    $entityTypeId, $attributeSetId, $attributeGroupId, 'postnumber', '1000'  //sort_order
);

$used_in_forms = array();

//$used_in_forms[]="adminhtml_customer";
//$used_in_forms[]="checkout_register";
//$used_in_forms[]="customer_account_create";
$used_in_forms[] = "customer_address_edit"; //this form code is used in checkout billing/shipping address
$used_in_forms[] = "adminhtml_checkout";
$attribute->setData("used_in_forms", $used_in_forms)
    ->setData("is_used_for_customer_segment", true)
    ->setData("is_system", 0)
    ->setData("is_user_defined", 1)
    ->setData("is_visible", 1)
    ->setData("sort_order", 100)
;
$attribute->save();
/**
 * Adding Extra Column to sales_flat_quote_address
 * to store the postnumber instruction field
 */
$sales_quote_address = $installer->getTable('sales/quote_address');
$installer->getConnection()
    ->addColumn($sales_quote_address, 'postnumber', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'New postnumber Field Added'
    ));

/**
 * Adding Extra Column to sales_flat_order_address
 * to store the postnumber field
 */
$sales_order_address = $installer->getTable('sales/order_address');
$installer->getConnection()
    ->addColumn($sales_order_address, 'postnumber', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'New postnumber Field Added'
    ));




//this is for creating a new attribute for customer address entity
$setup->addAttribute("customer_address", "packstation", array(
    "type" => "varchar",
    "backend" => "",
    "label" => "Packstation",
    "input" => "text",
    "source" => "",
    "visible" => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique" => false,
));

$attribute = Mage::getSingleton("eav/config")->getAttribute("customer_address", "packstation");

$setup->addAttributeToGroup(
    $entityTypeId, $attributeSetId, $attributeGroupId, 'packstation', '1010'  //sort_order
);

$used_in_forms = array();

//$used_in_forms[]="adminhtml_customer";
//$used_in_forms[]="checkout_register";
//$used_in_forms[]="customer_account_create";
$used_in_forms[] = "customer_address_edit"; //this form code is used in checkout billing/shipping address
$used_in_forms[] = "adminhtml_checkout";
$attribute->setData("used_in_forms", $used_in_forms)
    ->setData("is_used_for_customer_segment", true)
    ->setData("is_system", 0)
    ->setData("is_user_defined", 1)
    ->setData("is_visible", 1)
    ->setData("sort_order", 100)
;
$attribute->save();
/**
 * Adding Extra Column to sales_flat_quote_address
 * to store the Packstation field
 */
$sales_quote_address = $installer->getTable('sales/quote_address');
$installer->getConnection()
    ->addColumn($sales_quote_address, 'packstation', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Packstation'
    ));

/**
 * Adding Extra Column to sales_flat_order_address
 * to store the Packstation field
 */
$sales_order_address = $installer->getTable('sales/order_address');
$installer->getConnection()
    ->addColumn($sales_order_address, 'packstation', array(
        'type' => Varien_Db_Ddl_Table::TYPE_TEXT,
        'comment' => 'Packstation'
    ));

$installer->endSetup();