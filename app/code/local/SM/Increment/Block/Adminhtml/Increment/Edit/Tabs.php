<?php
class SM_Increment_Block_Adminhtml_Increment_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {
    public function __construct()
    {
        parent::__construct();
        $this->setId('increment_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('sm_increment')->__('Increment Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_increment', array(
            'label'        => Mage::helper('sm_increment')->__('General'),
            'title'        => Mage::helper('sm_increment')->__('General'),
            'content'     => $this->getLayout()->createBlock('sm_increment/adminhtml_increment_edit_tab_form')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

    public function getIncrement()
    {
        return Mage::registry('current_increment');
    }
}
