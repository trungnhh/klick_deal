<?php
class SM_Increment_Block_Adminhtml_Increment_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct(){
        parent::__construct();
        $this->_blockGroup = 'sm_increment';
        $this->_controller = 'adminhtml_increment';
        $this->_updateButton('save', 'label', Mage::helper('sm_increment')->__('Save Increment'));
        $this->_removeButton('delete');
        $this->_removeButton('reset');

        $this->_addButton('saveandcontinue', array(
            'label'        => Mage::helper('sm_increment')->__('Save And Continue Edit'),
            'onclick'    => 'saveAndContinueEdit()',
            'class'        => 'save',
        ), -100);
        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText(){
        if( Mage::registry('current_increment') && Mage::registry('current_increment')->getId() ) {
            return Mage::helper('sm_increment')->__("Edit Increment");
        }
        else {
            return Mage::helper('sm_increment')->__('Add Increment');
        }
    }
}