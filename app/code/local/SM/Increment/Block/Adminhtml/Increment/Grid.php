<?php
class SM_Increment_Block_Adminhtml_Increment_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct(){
        parent::__construct();
        $this->setId('incrementGrid');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection(){
        $collection = Mage::getModel('sm_increment/entity_resource_store_collection');
        $collection->getSelect()
            ->join(
            Mage::getConfig()->getTablePrefix().'eav_entity_type',
            'main_table.entity_type_id ='.Mage::getConfig()->getTablePrefix().'eav_entity_type.entity_type_id',
            array('entity_type_code'))
            ->join(
                Mage::getConfig()->getTablePrefix().'core_store',
                'main_table.store_id ='.Mage::getConfig()->getTablePrefix().'core_store.store_id',
                array('name','store_id'));
//        var_dump($collection->getSelect()->__toString());die;
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns(){
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => Mage::helper('sales')->__('Store'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
            ));
        }
        $this->addColumn('entity_type_code', array(
            'header'    => Mage::helper('sm_increment')->__('Entity code'),
            'index'        => 'entity_type_code',
        ));

        $this->addColumn('increment_prefix', array(
            'header'    => Mage::helper('sm_increment')->__('Increment Prefix'),
            'index'        => 'increment_prefix',
        ));
        $this->addColumn('increment_last_id', array(
            'header'    => Mage::helper('sm_increment')->__('Increment Last Id'),
            'index'        => 'increment_last_id',
        ));
        return parent::_prepareColumns();
    }

    public function getRowUrl($row){
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
    public function getGridUrl(){
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
    protected function _afterLoadCollection(){
        $this->getCollection()->walk('afterLoad');
        parent::_afterLoadCollection();
    }
}