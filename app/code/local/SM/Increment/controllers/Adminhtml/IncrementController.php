<?php
class SM_Increment_Adminhtml_IncrementController extends Mage_Adminhtml_Controller_Action
{
    protected function _initIncrement()
    {
        $incrementId  = (int) $this->getRequest()->getParam('id');
        $increment    = Mage::getModel('eav/entity_store');
        if ($incrementId) {
            $increment->load($incrementId);
        }
        Mage::register('current_increment', $increment);
        return $increment;
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function editAction() {
        $incrementId    = $this->getRequest()->getParam('id');
        $increment      = $this->_initIncrement();
        if ($incrementId && !$increment->getId()) {
            $this->_getSession()->addError(Mage::helper('sm_increment')->__('This increment no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }
        $data = Mage::getSingleton('adminhtml/session')->getIncrementData(true);
        if (!empty($data)) {
            $increment->setData($data);
        }
        Mage::register('increment_data', $increment);
        $this->loadLayout();
        if ($increment->getId()){
            $this->_title($increment->getName());
        }
        else{
            $this->_title(Mage::helper('sm_increment')->__('Add Increment'));
        }
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        $this->renderLayout();
    }

    public function newAction() {
        $this->_forward('edit');
    }

    public function saveAction() {
        if ($data = $this->getRequest()->getPost('increment')) {
            try {
                $increment = $this->_initIncrement();
                $increment->addData($data);
                $this->_validateIncrement($increment);
                $increment->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('sm_increment')->__('Increment was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $increment->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            }
            catch (Mage_Core_Exception $e){
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setIncrementData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sm_increment')->__('There was a problem saving the ncrement.'));
                Mage::getSingleton('adminhtml/session')->setIncrementData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('sm_increment')->__('Unable to find increment to save.'));
        $this->_redirect('*/*/');
    }

    protected function _validateIncrement($increment)
    {
        $entityTypeId = $increment->getEntityTypeId();
        $storeId = $increment->getStoreId();
        $entityTypeModel = Mage::getModel('eav/entity_type')->load($entityTypeId);
        $entityTypeCode = $entityTypeModel->getEntityTypeCode();
        $storeName = Mage::getModel('core/store')->load($storeId)->getName();

        //Check if a increment for the same entity from the same store has already existed
        $testIncrement = Mage::getModel('eav/entity_store')->loadByEntityStore($entityTypeId,$storeId);
        if( ($testIncrement->getId() && $testIncrement->getId() != $increment->getId() ) ){
            throw new Mage_Core_Exception(Mage::helper('sm_increment')->__("The increment for %s of store view %s has already existed ",$entityTypeCode,$storeName));
        }

        //Check if increment last Id has 8 number after prefix
        $newLast = $increment->getIncrementLastId();
        $newPrefix = $increment->getIncrementPrefix();

        if (strpos($newLast, $newPrefix) === 0) {
            $newLast = (string) substr($newLast, strlen($newPrefix));
        } else {
            throw new Mage_Core_Exception(Mage::helper('sm_increment')->__('Your last increment ID does not contain appropriate prefix.'));
        }

        if( strlen($newLast) != 8 ){
            throw new Mage_Core_Exception(Mage::helper('sm_increment')->__('Please make sure the Increment Last ID has exactly 8 number after Increment Prefix'));
        }

        //Check if new last id is higher than existed entity's id
        $newLast = (int) $newLast;
        $biggestIncrementId = Mage::getModel($entityTypeModel->getEntityModel())->getCollection()
            ->addFieldToFilter('store_id',$increment->getStoreId())
            ->addFieldToFilter('increment_id',array('like' => $newPrefix.'%'))
            ->addAttributeToSelect('increment_id')
            ->setOrder('increment_id','desc')
            ->getFirstItem()
            ->getIncrementId();


        if (strpos($biggestIncrementId, $newPrefix) === 0) {
            $biggestIncrementId = (int) substr($biggestIncrementId, strlen($newPrefix));
        } else {
            $biggestIncrementId = (int) $biggestIncrementId;
        }

        if($newLast < $biggestIncrementId){
            $biggestIncrementId = $newPrefix.$biggestIncrementId;
            throw new Mage_Core_Exception(Mage::helper('sm_increment')->__("Please select an Increment Last ID greater than or equal %s",$biggestIncrementId));
        }
    }
}