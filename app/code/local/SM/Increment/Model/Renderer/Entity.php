<?php
class SM_Increment_Model_Renderer_Entity
{
    public function toOptionArray()
    {
        $entityTypes = Mage::getModel('eav/entity_type')->getCollection()->addFieldToFilter('increment_per_store',1);

        $array = array();
        $array[] = array(
            'value' => '',
            'label' => 'Please select entity type code'
        );
        foreach($entityTypes as $type)
        {
            $array[] = array(
                'value' => $type->getEntityTypeId(), 'label' => Mage::helper('sm_increment')->__($type->getEntityTypeCode())
            );
        }

        return $array;
    }
}