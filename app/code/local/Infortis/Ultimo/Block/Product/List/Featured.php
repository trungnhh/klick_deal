<?php
class Infortis_Ultimo_Block_Product_List_Featured extends Mage_Catalog_Block_Product_List
{
	protected $_collectionCount = NULL;
	protected $_productCollectionId = NULL;
	protected $_cacheKeyArray = NULL;
	
	/**
	 * Initialize block's cache
	 */
	protected function _construct()
	{
		parent::_construct();

		$this->addData(array(
			'cache_lifetime'    => 99999999,
			'cache_tags'        => array(Mage_Catalog_Model_Product::CACHE_TAG),
		));
	}
	
	/**
	 * Get Key pieces for caching block content
	 *
	 * @return array
	 */
	public function getCacheKeyInfo()
	{
		if (NULL === $this->_cacheKeyArray)
		{
			$this->_cacheKeyArray = array(
				'INFORTIS_ITEMSLIDER',
				Mage::app()->getStore()->getCurrentCurrency()->getCode(),
				//Mage::app()->getStore()->getCurrentCurrencyCode(),
				
				Mage::app()->getStore()->getId(),
				Mage::getDesign()->getPackageName(),
				Mage::getDesign()->getTheme('template'),
				Mage::getSingleton('customer/session')->getCustomerGroupId(),
				'template' => $this->getTemplate(),
				
				$this->getBlockName(),
				$this->getCategoryId(),
				$this->getShowItems(),
				$this->getIsResponsive(),
				$this->getBreakpoints(),
				$this->getHideButton(),
				$this->getTimeout(),
				$this->getInitDelay(),
				
				(int)Mage::app()->getStore()->isCurrentlySecure(),
				$this->getUniqueCollectionId(),
			);
		}
		return $this->_cacheKeyArray;
	}
	
	/**
	 * Get collection id
	 *
	 * @return string
	 */
	public function getUniqueCollectionId()
	{
		if (NULL === $this->_productCollectionId)
		{
			$this->_prepareCollectionAndCache();
		}
		return $this->_productCollectionId;
	}
	
	/**
	 * Get number of products in the collection
	 *
	 * @return int
	 */
	public function getCollectionCount()
	{
		if (NULL === $this->_collectionCount)
		{
			$this->_prepareCollectionAndCache();
		}
		return $this->_collectionCount;
	}
	
	/**
	 * Prepare collection id, count collection
	 */
	protected function _prepareCollectionAndCache()
	{
		$ids = array();
		$i = 0;
		foreach ($this->_getProductCollection() as $product)
		{
			$ids[] = $product->getId();
			$i++;
		}
		
		$this->_productCollectionId = implode("+", $ids);
		$this->_collectionCount = $i;
	}
	
	/**
	 * Retrieve loaded category collection.
	 * Variables collected from CMS markup: category_id, product_count, is_random
	 */
	protected function _getProductCollection()
	{
		if (is_null($this->_productCollection))
		{
            $storeId    = Mage::app()->getStore()->getId();
            $productCount = $this->getProductCount() ? $this->getProductCount() : 8;
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('*')
                //->addAttributeToSelect(array('name', 'price', 'small_image')) //edit to suit tastes
                ->setStoreId($storeId)
                ->addStoreFilter($storeId)
                ->addAttributeToFilter('featured_product', array('eq' => '1'))
                ->addFieldToFilter('visibility', Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH)
                ->setPageSize($productCount)
                ->setCurPage(1)
                ->load();
            
			$this->_productCollection = $collection;
		}
		return $this->_productCollection;
	}
	
	/**
	 * Create unique block id for frontend
	 *
	 * @return string
	 */
	public function getFrontendHash()
	{
		return md5(implode("+", $this->getCacheKeyInfo()));
	}

	public function getMinimalPrice($product){
		$_associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);
		$price =	array();
		foreach($_associatedProducts as $_associatedProduct) {
		    $_product	=	Mage::getModel('catalog/product')->load($_associatedProduct->getId());
		    if ($_product->getMinimalPrice()) {
		    	$price[]	=	$_product->getMinimalPrice();
		    } else {
		    	$price[]	=	$_product->getFinalPrice();
		    }
		}
		if (sizeof($price)>0) {
			return min($price);
		} else {
			return 0;
		}
		
	}
}
