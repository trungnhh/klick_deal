<?php

class Eb_Varnish_Model_Adminhtml_System_Config_Backend_Log_Cron extends Mage_Core_Model_Config_Data
{
    const CRON_STRING_PATH  = 'crontab/jobs/varnish_crawler/schedule/cron_expr';
    const CRON_MODEL_PATH   = 'crontab/jobs/varnish_crawler/run/model';

    /**
     * Changes to the object before save.
     *
     * @return void
     */
    protected function _beforeSave()
    {
        $cronExprString = $this->getData('groups/varnish_crawler/fields/cron_expr/value');
        try {
            Mage::getModel('core/config_data')
                ->load(self::CRON_STRING_PATH, 'path')
                ->setValue($cronExprString)
                ->setPath(self::CRON_STRING_PATH)
                ->save();
            Mage::getModel('core/config_data')
                ->load(self::CRON_MODEL_PATH, 'path')
                ->setValue((string) Mage::getConfig()->getNode(self::CRON_MODEL_PATH))
                ->setPath(self::CRON_MODEL_PATH)
                ->save();
        } catch (Exception $e) {
            Mage::throwException(Mage::helper('adminhtml')->__('Unable to save the cron expression.'));
        }
    }
}
