<?php

class Eb_Varnish_Model_Control_Catalog_Category extends Eb_Varnish_Model_Control_Abstract
{
    protected $_helperName = 'eb_varnish/control_catalog_category';

    /**
     * Purge Category.
     *
     * @param Mage_Catalog_Model_Category $category The category to be purged.
     *
     * @return Eb_Varnish_Model_Control_Catalog_Category
     */
    public function purge(Mage_Catalog_Model_Category $category)
    {
        if ($this->_canPurge()) {
            $this->_purgeById($category->getId());
            if ($categoryName = $category->getName()) {
                $this->_getSession()->addSuccess(
                    Mage::helper('eb_varnish')->__('Varnish cache for "%s" has been purged.', $categoryName)
                );
            }
        }
        return $this;
    }

    /**
     * Purge Category by id.
     *
     * @param int $id The category id.
     *
     * @return Eb_Varnish_Model_Control_Catalog_Category
     */
    public function purgeById($id)
    {
        if ($this->_canPurge()) {
            $this->_purgeById($id);
        }
        return $this;
    }

    /**
     * Purge Category by id.
     *
     * @param int $id The category id.
     *
     * @return Eb_Varnish_Model_Control_Catalog_Category
     */
    protected function _purgeById($id)
    {
        $collection = $this->_getUrlRewriteCollection()
            ->filterAllByCategoryId($id);
        foreach ($collection as $urlRewriteRule) {
            $urlRegexp = '/' . $urlRewriteRule->getRequestPath();
            $this->_getCacheControl()
                ->clean($this->_getStoreDomainList(), $urlRegexp);
        }
        return $this;
    }
}
