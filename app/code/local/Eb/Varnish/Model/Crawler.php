<?php

class Eb_Varnish_Model_Crawler extends Mage_Core_Model_Abstract
{
    const XML_PATH_CRAWLER_ENABLED = 'system/varnish_crawler/enabled';
    const XML_PATH_CRAWLER_DESIGN_EXCEPTIONS = 'system/varnish_crawler/design_exceptions';
    const XML_PATH_CRAWLER_MULTICURRENCY = 'system/varnish_crawler/multicurrency';
    const XML_PATH_CRAWLER_THREADS_NUM = 'system/varnish_crawler/threads_num';

    protected $_adapter;

    /**
     * Initialize crawler.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('eb_varnish/crawler');
    }

    /**
     * Crawl stores.
     *
     * @return Eb_Varnish_Model_Crawler
     */
    public function run()
    {
        foreach ($this->getStores() as $storeId => $store) {
            if (!Mage::getStoreConfig(self::XML_PATH_CRAWLER_ENABLED, $storeId)) {
                continue;
            }
            foreach ($this->getCurrencies($store) as $currencyCode) {
                foreach ($this->getUserAgents($store) as $userAgent) {
                    $this->_run($store, $currencyCode, $userAgent);
                }
            }
        }
        return $this;
    }

    /**
     * Crawl store.
     *
     * @param Mage_Core_Model_Store $store        The store object.
     * @param string                $currencyCode Code for currency.
     * @param string                $userAgent    The user agent.
     *
     * @return Eb_Varnish_Model_Crawler
     */
    protected function _run(Mage_Core_Model_Store $store, $currencyCode, $userAgent)
    {
        $storeId = $store->getId();
        $baseUrl = $store->getBaseUrl();

        $defaultStoreId = $store->getWebsite()->getDefaultStore()->getId();
        $defaultBaseUrl = $store->getWebsite()->getDefaultStore()->getBaseUrl();

        $options = array();
        if (($baseUrl == $defaultBaseUrl) && ($storeId != $defaultStoreId)) {
            $options[CURLOPT_COOKIE] = sprintf('store=%s;', $store->getCode());
        }
        if ($currencyCode != $store->getDefaultCurrencyCode()) {
            $currency = Mage::getModel('directory/currency')->load($currencyCode);
            $options[CURLOPT_COOKIE] = sprintf('currency=%s;', $currency->getCode());
        }
        $options[CURLOPT_USERAGENT] = $userAgent;

        $threadsNum = intval(Mage::getStoreConfig(self::XML_PATH_CRAWLER_THREADS_NUM, $storeId));
        if (!$threadsNum) {
            $threadsNum = 1;
        }
        foreach (array_chunk($this->_getUrls($store), $threadsNum) as $urls) {
            $this->_getAdapter()
                ->multiRequest($urls, $options);
            Mage::helper('eb_varnish')->debug(
                array(
                    'urls' => $urls,
                    'options' => $options
                )
            );
        }

        return $this;
    }

    /**
     * Returns available stores.
     *
     * @return array
     */
    public function getStores()
    {
        return Mage::app()->getStores();
    }

    /**
     * Returns available currencies.
     *
     * @param Mage_Core_Model_Store $store The store object.
     *
     * @return string
     */
    public function getCurrencies(Mage_Core_Model_Store $store)
    {
        if (Mage::getStoreConfig(self::XML_PATH_CRAWLER_MULTICURRENCY, $store->getId())) {
            $currencies = $store->getAvailableCurrencyCodes(true);
        } else {
            $currencies = array($store->getDefaultCurrencyCode());
        }
        return $currencies;
    }

    /**
     * Returns User Agent according to design exceptions.
     *
     * @param Mage_Core_Model_Store $store The store.
     *
     * @return array
     */
    public function getUserAgents(Mage_Core_Model_Store $store)
    {
        return array('VarnishCrawler');
    }

    /**
     * Returns urls to crawl.
     *
     * @param Mage_Core_Model_Store $store The Store.
     *
     * @return string
     */
    protected function _getUrls(Mage_Core_Model_Store $store)
    {
        $urls = array();
        $stmt = $this->_getResource()
            ->getUrlStmt($store->getId());
        while ($row = $stmt->fetch()) {
            $urls[] = $store->getBaseUrl() . $row['request_path'];
        }
        return $urls;
    }

    /**
     * Returns curl adapter.
     *
     * @return Varien_Http_Adapter_Curl
     */
    protected function _getAdapter()
    {
        if (!$this->_adapter) {
            $this->_adapter = new Varien_Http_Adapter_Curl();
        }
        return $this->_adapter;
    }

    /**
     * Parses urls from text/html.
     *
     * @param string $html Html content.
     *
     * @return array
     */
    public function parseUrls($html)
    {
        $urls = array();
        preg_match_all("/\s+href\s*=\s*[\"\']?([^\s\"\']+)[\"\'\s]+/ims", $html, $urls);
        return $urls[1];
    }
}
