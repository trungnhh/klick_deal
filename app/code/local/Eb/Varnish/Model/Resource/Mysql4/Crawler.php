<?php

class Eb_Varnish_Model_Resource_Mysql4_Crawler extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Initializations depending on version.
     *
     * @return void
     */
    protected function _construct()
    {
        $version = explode('.', Mage::getVersion());
        if ($version[1] >= 13) {
            $this->_init('enterprise_urlrewrite/url_rewrite', 'url_rewrite_id');
        } else {
            $this->_init('core/url_rewrite', 'url_rewrite_id');
        }
    }

    /**
     * Returns core_url_rewrite query statement.
     *
     * @param int $storeId The store id.
     *
     * @return Zend_Db_Statement
     */
    public function getUrlStmt($storeId)
    {
        $select = $this->_getReadAdapter()->select()
            ->from(
                $this->getTable('core/url_rewrite'),
                array('store_id', 'request_path')
            )
            ->where('store_id=?', $storeId)
            ->where('is_system=1');

        return $this->_getReadAdapter()->query($select);
    }
}
