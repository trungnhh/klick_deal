<?php

if (class_exists('Enterprise_UrlRewrite_Model_Resource_Url_Rewrite_Collection', false)) {
    class Eb_Varnish_Model_Resource_Mysql4_Core_Url_Rewrite_Collection extends
        Enterprise_UrlRewrite_Model_Resource_Url_Rewrite_Collection
        //    extends Mage_Core_Model_Mysql4_Url_Rewrite_Collection
    {
        /**
         * Filter collection by category id.
         *
         * @param int $categoryId The category id.
         *
         * @return Eb_Varnish_Model_Resource_Mysql4_Core_Url_Rewrite_Collection
         */
        public function filterAllByCategoryId($categoryId)
        {
            $this
                ->getSelect()
                ->where('target_path = ?', sprintf('catalog/category/view/id/%s', $categoryId));
            return $this;
        }
    }

} else {
    class Eb_Varnish_Model_Resource_Mysql4_Core_Url_Rewrite_Collection extends
        Mage_Core_Model_Mysql4_Url_Rewrite_Collection
    {
        /**
         * Filter collection by category id.
         *
         * @param int $categoryId The category id.
         *
         * @return Eb_Varnish_Model_Resource_Mysql4_Core_Url_Rewrite_Collection
         */
        public function filterAllByCategoryId($categoryId)
        {
            $this->getSelect()
                ->where('id_path = ?', sprintf('category/%s', $categoryId));
            return $this;
        }
    }
}
