<?php

class Eb_Varnish_Model_Catalog_Product_Relation extends Mage_Core_Model_Abstract
{
    /**
     * Constructor.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('catalog/product_relation');
    }
}
