<?php

class Eb_Varnish_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_VARNISH_CACHE_ENABLED  = 'system/varnish/enabled';
    const XML_PATH_VARNISH_CACHE_DEBUG    = 'system/varnish/debug';

    protected $_arrModuleSkip = array(
        'customer',
        'newsletter',
        'checkout',
        'onestepcheckout',
        'paypal',
        'wishlist',
        'api',
        'sales',
        'giftcard',
        'compare',
        'onepage',
        'contacts'
    );
    /**
     * Check whether Varnish cache is enabled.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_VARNISH_CACHE_ENABLED);
    }

    /**
     * Check whether debuging is enabled.
     *
     * @return bool
     */
    public function isDebug()
    {
        if (Mage::getStoreConfigFlag(self::XML_PATH_VARNISH_CACHE_DEBUG)) {
            return true;
        }
        return false;
    }

    /**
     * Log debugging data.
     *
     * @param string|array $debugData The debug data.
     *
     * @return void
     */
    public function debug($debugData)
    {
        if ($this->isDebug()) {
            Mage::log($debugData, null, 'varnish_cache.log');
        }
    }

    /**
     * Get Varnish control model.
     *
     * @return Phoenix_VarnishCache_Model_Control
     */
    public function getCacheControl()
    {
        return Mage::getSingleton('eb_varnish/control');
    }


    /**
     * Check configuration to identify modules to skip.
     *
     * @return bool
     */
    public function shouldSkip()
    {
        //@todo get controllers from admin and config module should skip from admin
        return true;
        if (in_array(
            Mage::app()->getFrontController()->getRequest()->getModuleName(),
            $this->_arrModuleSkip
            )) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check whether ajax has to be active.
     *
     * @return bool
     */
    public function isAjax()
    {
        return Mage::app()->getFrontController()->getRequest()->getModuleName() == 'varnish';
    }
}
