<?php
class Eb_Varnish_UpdateController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        if (!$this->getRequest()->isPost()) {
            return;
        }

        $id = $this->getRequest()->getParam('id', false);

        $handleUpdate = 'checkout_cart_index';
        if ($id) {
            $handleUpdate = 'catalog_product_view';
        }
        if (!is_object(Mage::registry('current_product'))) {
            $storeId = Mage::app()->getStore()->getId();
            $_product = Mage::getModel('catalog/product')->load($id);
            $summaryData = Mage::getModel('review/review_summary')
                ->setStoreId($storeId)
                ->load($_product->getId());
            $summary = new Varien_Object();
            $summary->setData($summaryData->getData());
            $_product->setRatingSummary($summary);
            Mage::register('current_product', $_product);
            Mage::register('product', $_product);
        }

        $update = $this->getLayout()->getUpdate();
        $update->addHandle('default');
        $this->addActionLayoutHandles();
        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        $update->addHandle($handleUpdate);
        $this->loadLayoutUpdates();
        $this->generateLayoutXml();
        $this->generateLayoutBlocks();
        $this->_isLayoutLoaded = true;


        $blocks = array();

        $userMenu = $this->getLayout()->getBlock('header');
        if(is_object($userMenu)){
            $blocks['usermenu'] = $userMenu->setTemplate('page/html/header_varnish.phtml')->toHtml();
        }

        $headerTopLinks = $this->getLayout()->getBlock('header.top.links');
        if(is_object($headerTopLinks)){
            $blocks['header_top_links'] = $headerTopLinks->toHtml();
        }

        $customerLinks = $this->getLayout()->getBlock('mobile.customer.link');
        if(is_object($customerLinks)){
            $blocks['mobile.customer.link'] = $customerLinks->toHtml();
        }
        $block = $this->getLayout()->createBlock('jimmy_countdown/countDown');
        if(is_object($block) && $id) {
            $blocks['count_down'] = $block->toHtml();
        }

        $block = Mage::getBlockSingleton('files/product_view');
        if(is_object($block) && $id){
            $block->setProductId($id);
            $blocks['related_download'] = $block->setTemplate('files/files.phtml')->toHtml();
        }

        $review = $this->getLayout()->getBlock('catalog.product.review');
        try {
        if (is_object($review)) {
            $blocks['reviews_summary'] = $review->setProduct($_product)->toHtml();
        }
        } catch (Exception $e) {
            echo $e->getMessage();exit;
        }
        $review = $this->getLayout()->getBlock('tabreviews');
        if (is_object($review)) {
            $blocks['reviews_form'] = $review->toHtml();
        }

        $globalMessages = $this->getLayout()->getBlock('global_messages');
        $message = $this->getLayout()->getBlock('messages');
        if(is_object($globalMessages)){
            $blocks['globalmsg'] = $globalMessages->toHtml();
        }
        if(is_object($message)){
            $blocks['msg'] = $message->toHtml();
        }
        $blocks['formkey'] = Mage::getSingleton('core/session')->getFormKey();
        @header('Content-type: application/json');
        echo json_encode($blocks);
        exit;
    }

    public function pdpUpdateAction()
    {   /*
        if (!$this->getRequest()->isPost()) {
            return;
        }
        $id = $this->getRequest()->getParams('id');
        $block = Mage::getBlockSingleton('files/product_view');
        $response = array();
        if(is_object($block)){
            $block->setProductId($id);
            $response['related_download'] = $block->setTemplate('files/files.phtml')->toHtml();
        }
        $response['formkey'] = Mage::getSingleton('core/session')->getFormKey();
        @header('Content-type: application/json');
        echo json_encode($response);
        exit;
        */
    }
}