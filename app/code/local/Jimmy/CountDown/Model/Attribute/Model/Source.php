<?php
class Jimmy_CountDown_Model_Attribute_Model_Source
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label'=>Mage::helper('catalog')->__('Evergreen')),
            array('value' => 2, 'label'=>Mage::helper('catalog')->__('Date time count down')),
            array('value' => 3, 'label'=>Mage::helper('catalog')->__('Quantity count down')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            1 => Mage::helper('catalog')->__('Evergreen'),
            2 => Mage::helper('catalog')->__('Date time count down'),
            3 => Mage::helper('catalog')->__('Quantity count down'),
        );
    }
}