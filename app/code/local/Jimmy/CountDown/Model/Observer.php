<?php
class Jimmy_CountDown_Model_Observer
{
    public function coreBlockAbstractPrepareLayoutAfter($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs) {
            $block->addTab('countdown', array(
                'label' => Mage::helper('catalog')->__('Count Down'),
                'content' => Mage::getBlockSingleton('jimmy_countdown/adminhtml_catalog_product_countDown')->toHtml(),
            ));
        }
        return $this;
    }

    public function catalogProductPrepareSave($observer)
    {
        $request =$observer->getEvent()->getRequest();
        $product = $observer->getEvent()->getProduct();
        $params = $request->getPost('countdown');
        if($params['countdown_enabled'] == 1){
            $product->setData('countdown_enabled',$params['countdown_enabled']);
            $product->setData('countdown_type',$params['countdown_type']);
            $product->setData('countdown_customer_groups', implode(',', $params['countdown_customer_groups']));
            $product->setData('countdown_from',$params['countdown_from']);
            $product->setData('countdown_to',$params['countdown_to']);
            if($product->getTypeId() != Mage_Catalog_Model_Product_Type::TYPE_GROUPED){
                if((float)$product->getPrice() > (float)$params['countdown_price']){
                    $product->setData('special_price', $params['countdown_price']);
                    $product->setData('special_from_date',$params['countdown_from']);
                    $product->setData('special_to_date',$params['countdown_to']);
                    $product->setData('countdown_price', $params['countdown_price']);
                }else{
                    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('catalog')->__('Discount price should be smaller than regular price'));
                }
            }else{
                if((float)$params['countdown_price'] < 100){
                    $children = $product->getTypeInstance(true)
                        ->getAssociatedProducts($product);
                    foreach($children as $item){
                        $price = $item->getPrice();
                        $special_price = ($price * (int)$params['countdown_price']) / 100;
                        $item->setData('special_price', $special_price);
                        $item->setData('special_from_date',$params['countdown_from']);
                        $item->setData('special_to_date',$params['countdown_to']);
                        $item->getResource()->save($item);
                    }
                    $product->setData('countdown_price', $params['countdown_price']);
                }
                else{
                    Mage::getSingleton('adminhtml/session')->addError(Mage::helper('catalog')->__('Discount percent should be smaller than 100'));
                }
            }
            $product->getResource()->save($product);
        }
    }
}