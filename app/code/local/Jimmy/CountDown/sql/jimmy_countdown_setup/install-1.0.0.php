<?php
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = Mage::getResourceModel('catalog/setup','catalog_setup');

$installer->startSetup();

$installer->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'countdown_enabled',
    array(
        'label'                      => 'Enable Count Down',
        'group'                      => 'General',
        'type'                       => 'int',
        'input'                      => 'boolean',
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'user_defined'               => false,
        'required'                   => false,
        'visible'                    => false,
        'source'                     => null,
        'backend'                    => null,
        'searchable'                 => false,
        'visible_in_advanced_search' => false,
        'visible_on_front'           => false,
        'is_configurable'            => false,
        'is_html_allowed_on_front'   => false,
    )
);

$installer->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'countdown_type',
    array(
        'label'                      => 'Count Down Type',
        'group'                      => 'Count Down',
        'type'                       => 'int',
        'input'                      => 'select',
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'user_defined'               => false,
        'required'                   => false,
        'visible'                    => false,
        'source'                     => 'jimmy_countdown/attribute_model_source',
        'backend'                    => null,
        'searchable'                 => false,
        'visible_in_advanced_search' => false,
        'visible_on_front'           => false,
        'is_configurable'            => false,
        'is_html_allowed_on_front'   => false,
    )
);

$installer->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'countdown_customer_groups',
    array(
        'label'                      => 'Customer Groups',
        'group'                      => 'Count Down',
        'type'                       => 'varchar',
        'input'                      => 'multiselect',
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'user_defined'               => false,
        'required'                   => false,
        'visible'                    => false,
        'source'                     => 'jimmy_countdown/system_config_backend_customer_group',
        'backend'                    => 'eav/entity_attribute_backend_array',
        'searchable'                 => false,
        'visible_in_advanced_search' => false,
        'visible_on_front'           => false,
        'is_configurable'            => false,
        'is_html_allowed_on_front'   => false,
    )
);

$installer->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'countdown_from',
    array(
        'label'                      => 'Count Down From',
        'group'                      => 'Count Down',
        'type'                       => 'datetime',
        'input'                      => 'date',
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'user_defined'               => false,
        'required'                   => false,
        'visible'                    => false,
        'source'                     => null,
        'backend'                    => 'eav/entity_attribute_backend_datetime',
        'searchable'                 => false,
        'visible_in_advanced_search' => false,
        'visible_on_front'           => false,
        'is_configurable'            => false,
        'is_html_allowed_on_front'   => false,
    )
);

$installer->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'countdown_to',
    array(
        'label'                      => 'Count Down To',
        'group'                      => 'Count Down',
        'type'                       => 'datetime',
        'input'                      => 'date',
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'user_defined'               => false,
        'required'                   => false,
        'visible'                    => false,
        'source'                     => null,
        'backend'                    => 'eav/entity_attribute_backend_datetime',
        'searchable'                 => false,
        'visible_in_advanced_search' => false,
        'visible_on_front'           => false,
        'is_configurable'            => false,
        'is_html_allowed_on_front'   => false,
    )
);

$installer->endSetup();