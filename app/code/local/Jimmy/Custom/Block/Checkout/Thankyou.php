<?php
class Jimmy_Custom_Block_Checkout_Thankyou extends Mage_Core_Block_Template
{
    public function getProductCollection()
    {
        if($categoryIds = $this->getProductsOnCheckoutCategoryId()){
            $collection = Mage::getModel('catalog/product')->getCollection()
                ->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('category_id', array('in' => $categoryIds));
                //->setPageSize(4)
                //->setCurPage(1);
            $collection->getSelect()->limit(4);
            return $collection;
        }
        return false;
    }
    public function getProductsOnCheckoutCategoryId()
    {
        return Mage::getStoreConfig('opc/default/products_on_checkout');
    }

    public function getOrder()
    {
        $oderId = Mage::getSingleton('checkout/session')->getLastOrderId();
        if(!$oderId){
            return null;
        }
        $order = Mage::getModel('sales/order')->load($oderId);
        return $order;
    }

    public function getOrderIncrement()
    {
        if($this->getOrder())
            return $this->getOrder()->getIncrementId();
    }

    public function getOrderTotalWithoutShippingFee()
    {
        if(!$this->getOrder()){
            return 0.00;
        }
        $order = $this->getOrder();

        $grandTotal = $order->getGrandTotal();
        $shippingFee = $order->getShippingAmount();

        return $grandTotal - $shippingFee;
    }

    public function getStoreName()
    {
        return Mage::app()->getStore()->getName();
    }

    public function getGrandTotal()
    {
        if(!$this->getOrder()){
            return 0.00;
        }
        $order = $this->getOrder();

        $grandTotal = $order->getGrandTotal();
        return $grandTotal;
    }

    public function getTaxAmount()
    {
        if(!$this->getOrder()){
            return 0.00;
        }
        $order = $this->getOrder();

        $tax = $order->getTaxAmount();
        return $tax;
    }

    public function getShippingAmount()
    {
        if(!$this->getOrder()){
            return 0.00;
        }
        $order = $this->getOrder();

        $shipping = $order->getShippingAmount();
        return $shipping;
    }

    public function getAllProducts()
    {
        if(!$this->getOrder()){
            return array();
        }
        $result = array();
        $order = $this->getOrder();
        $items = $order->getAllVisibileItems();
        foreach($items as $item){
            $tmp = array();
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            $tmp['sku'] = $product->getSku();
            $tmp['name'] = $product->getName();
            $tmp['price'] = $product->getPrice();
            $tmp['qty'] = $product->getStockItem()->getQty();
            $result[] = $tmp;
        }
        return $result;
    }
}