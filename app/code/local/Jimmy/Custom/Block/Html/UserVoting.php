<?php
class Jimmy_Custom_Block_Html_UserVoting extends Mage_Core_Block_Template
{
    public function getReviewCollection($storeId)
    {
        $ids = Mage::helper('jcustom')->getReviewIds();
        if($ids){
            $idsArr = explode(',',$ids);
            $collecton = Mage::getModel('review/review')->getCollection()
                ->addStoreFilter($storeId)
                ->addFieldToFilter('review_id',array('in' => $idsArr))
                ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED);
        }else{
            $collecton = Mage::getModel('review/review')->getCollection()
                ->addStoreFilter($storeId)
                ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED);
        }

        $collecton->getSelect()
            ->join(array('rov'=>'rating_option_vote'),'rov.review_id = main_table.review_id','rov.percent as rov_percent')
            ->group('main_table.entity_pk_value')
            ->order('rov_percent DESC')
            ->limit(30);

        return $collecton;
    }

    public function getReviewData($storeId)
    {
        $collection = $this->getReviewCollection($storeId);
        $review = array();
        foreach($collection as $item){
            $tmp = array();
            $product = Mage::getModel('catalog/product')->load($item->getData('entity_pk_value'));
            $tmp['title'] = '<a href="'.$product->getProductUrl().'">'.htmlspecialchars($product->getName()).'</a>';
            $tmp['slogan'] = $item->getData('title');
            $tmp['detail'] = (strlen($item->getData('detail')) > 450) ? substr($this->stripTags($item->getData('detail'), '<b><i><u><br>'),0,450) . '...' : $item->getData('detail');
            $tmp['customer'] = $item->getData('nickname');
            if($item->getData('customer_id')){
                $customer = Mage::getModel('customer/customer')->load($item->getData('customer_id'));
                $tmp['customer'] = $customer->getFirstname() . ' ' .$customer->getLastname();
            }
            $tmp['date'] = date('d. M Y',strtotime($item->getData('created_at')));
            $tmp['rate'] = $item->getData('rov_percent');//$this->calculateRate($item->getData('review_id'), $storeId);
            $review[] = $tmp;
        }
        return $review;
    }

    public function getVoteCollectionByReviewId($reviewId, $storeId)
    {
        $votesCollection = Mage::getModel('rating/rating_option_vote')
            ->getResourceCollection()
            ->setReviewFilter($reviewId)
            ->setStoreFilter($storeId)
            ->load();

        return $votesCollection;
    }

    public function calculateRate($reviewId, $storeId)
    {
        $votes = $this->getVoteCollectionByReviewId($reviewId, $storeId);
        $total = count($votes);
        $percent = 0;
        foreach($votes as $item){
            $percent += (int)$item->getData('percent');
        }
        return ($percent / $total);
    }

}