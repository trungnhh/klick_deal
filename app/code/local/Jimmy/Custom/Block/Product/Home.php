<?php
class Jimmy_Custom_Block_Product_Home extends Mage_Catalog_Block_Product_List
{
    public function getAllProducts()
    {
        $productIdsList = $this->getProductIds();
        $productIds = array();
        $tmp = explode(",", $productIdsList);
        foreach ($tmp as $item) {
            $productIds[] = (int) trim($item);
        }
        $products = array();
        foreach ($productIds as $id) {
            $products[] = Mage::getModel('catalog/product')->load($id);
        }

        return $products;
    }
}