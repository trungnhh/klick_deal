<?php
class Jimmy_Custom_Block_Product_View extends Mage_Catalog_Block_Product_View
{
    /**
     * Get product attribute value in frontend
     * @param string $attributeCode
     * @param string $attributeOptionId
     * @return string
     */
    function getOptionValueByAttributeCode($attributeCode,$attributeOptionId){
        return Mage::helper('jcustom')->getOptionValueByAttributeCode($attributeCode,$attributeOptionId);
    }

    public function getAssociatedProducts()
    {
        $ids = array();
        $associated = array();
        $associatedProducts =  $this->getProduct()->getTypeInstance(true)
            ->getAssociatedProducts($this->getProduct());
        foreach ($associatedProducts as $product) {
            if ($product->getTypeId() == 'grouped') {
                $children = $product->getTypeInstance(true)->getAssociatedProducts($product);
                foreach ($children as $item) {
                    if (!in_array($item->getId(), $ids)) {
                        $ids[] = $item->getId();
                        $associated[] = $item;
                    }
                }
            } else {
                $ids[] = $product->getId();
                $associated[] = $product;
            }
        }
        return $associated;
    }
}