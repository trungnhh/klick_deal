<?php
class Jimmy_Custom_Block_Product_List extends Mage_Catalog_Block_Product_List
{
    public function getAssociatedProducts($product)
    {
        return $product->getTypeInstance(true)
            ->getAssociatedProducts($product);
    }
}