<?php
class Jimmy_Custom_Model_Sales_Order_Total_Invoice_SaveAmount extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $order = $invoice->getOrder();

        $deliverAmountLeft = $order->getDeliveryAmount() - $order->getDeliveryAmountInvoiced();
        $baseDeliveryAmountLeft = $order->getBaseDeliveryAmount() - $order->getBaseDeliveryAmountInvoiced();
        if (abs($baseDeliveryAmountLeft) < $invoice->getBaseGrandTotal()) {
            $invoice->setGrandTotal($invoice->getGrandTotal() + $deliverAmountLeft);
            $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseDeliveryAmountLeft);
        } else {
            $deliverAmountLeft = $invoice->getGrandTotal() * -1;
            $baseDeliveryAmountLeft = $invoice->getBaseGrandTotal() * -1;

            $invoice->setGrandTotal(0);
            $invoice->setBaseGrandTotal(0);
        }

        $invoice->setDeliveryAmount($deliverAmountLeft);
        $invoice->setBaseDeliveryAmount($baseDeliveryAmountLeft);

        return $this;
    }
}