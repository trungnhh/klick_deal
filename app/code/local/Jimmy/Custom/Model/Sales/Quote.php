<?php
class Jimmy_Custom_Model_Sales_Quote extends Mage_Sales_Model_Quote
{
    public function getCheckoutMethod($originalMethod = false)
    {
        if ($this->getCustomerId() && !$originalMethod) {
            return self::CHECKOUT_METHOD_LOGIN_IN;
        }
        return self::CHECKOUT_METHOD_GUEST;
    }
}