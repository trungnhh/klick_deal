<?php
class Jimmy_Custom_Model_Sales_Quote_Address_Total_SaveAmount extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
    protected $_code = 'saveamount';

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $regularPrice = 0;
        $specialPrice = 0;
        $quote = $address->getQuote();
        foreach ($quote->getAllItems() as $item) {
            $productId = $item->getProductId();
            $product = Mage::getModel('catalog/product')->load($productId);
            if($product->getTypeId() == 'configurable' && $product->getTypeId() == 'grouped')
                continue;
            $qty = (float)$item->getQty();
            $regularPrice += (float)$product->getPrice() * $qty;
            $specialPrice += (float)$product->getFinalPrice() * $qty;
        }
        $amount = $specialPrice - $regularPrice;
        if($amount != 0)
            $address->addTotal(array(
                'code'=>$this->getCode(),
                'title'=>Mage::helper('checkout')->__('Sie sparen heute insgesamt'),
                'value'=> $amount/2
            ));
        return $this;
    }
}