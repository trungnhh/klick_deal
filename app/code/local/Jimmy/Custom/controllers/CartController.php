<?php

require_once 'Mage/Checkout/controllers/CartController.php';

class Jimmy_Custom_CartController extends Mage_Checkout_CartController
{
    /**
     * set redirect to onepage
     * Override indexAction Mage_Checkout_CartController
     */
    public function indexAction()
    {
        $this->_redirect('onepage');
    }
    
     /**
     * Delete shoping cart item action
     */
    public function deleteAction()
    {
        if ($this->_validateFormKey()) {
            $id = (int)$this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $this->_getCart()->removeItem($id)
                        ->save();
                } catch (Exception $e) {
                    $this->_getSession()->addError($this->__('Cannot remove the item.'));
                    Mage::logException($e);
                }
            }
        } else {
            $this->_getSession()->addError($this->__('Cannot remove the item.'));
        }

        $this->_redirect('checkout/cart');

    }

    public function couponPostAction()
    {
        $response = array();

        if (!$this->_getCart()->getQuote()->getItemsCount()) {
            $this->_goBack();
            return;
        }

        $couponCode = (string) $this->getRequest()->getParam('coupon_code');
        if ($this->getRequest()->getParam('remove') == 1) {
            $couponCode = '';
        }
        $oldCouponCode = $this->_getQuote()->getCouponCode();

        if (!strlen($couponCode) && !strlen($oldCouponCode)) {
            $this->_goBack();
            return;
        }

        try {
            $codeLength = strlen($couponCode);
            $isCodeLengthValid = $codeLength && $codeLength <= Mage_Checkout_Helper_Cart::COUPON_CODE_MAX_LENGTH;

            $this->_getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->_getQuote()->setCouponCode($isCodeLengthValid ? $couponCode : '')
                ->collectTotals()
                ->save();

            if ($codeLength) {
                if ($isCodeLengthValid && $couponCode == $this->_getQuote()->getCouponCode()) {
                	$response['success'] = true;
                    $response['msg'] = $this->__('Coupon code "%s" was applied.', Mage::helper('core')->escapeHtml($couponCode));
                } else {
                	$response['success'] = false;
                    $response['msg'] = $this->__('Coupon code "%s" is not valid.', Mage::helper('core')->escapeHtml($couponCode));
                }
            } else {
            	$response['success'] = true;
                $response['msg'] = $this->__('Coupon code was canceled.');
            }

        } catch (Mage_Core_Exception $e) {
        	$response['success'] = false;
            $response['msg'] = $e->getMessage();
        } catch (Exception $e) {
        	$response['success'] = false;
            $response['msg'] = $this->__('Cannot apply the coupon code.');
            Mage::logException($e);
        }

        $json = json_encode($response);
        $this->getResponse()->clearHeaders()->setHeader('Content-Type', 'application/json')->setBody($json);
    }
}