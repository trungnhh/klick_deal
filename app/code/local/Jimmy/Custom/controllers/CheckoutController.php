<?php
class Jimmy_Custom_CheckoutController extends Mage_Core_Controller_Front_Action
{
    public function estimateShippingAction()
    {
        if(!$this->checkIsContainPhysic()){
            echo Mage::helper('core')->formatPrice(0, true);
        }else{
            $tablerateColl = Mage::getResourceModel('shipping/carrier_tablerate_collection');
            /* @var $tablerateColl Mage_Shipping_Model_Resource_Carrier_Tablerate_Collection */
            $selectedCountry = $this->getRequest()->getPost('country');
            if($selectedCountry == '')
                return 0;
            foreach ($tablerateColl as $tablerate) {
                /* @var $tablerate Mage_Shipping_Model_Carrier_Tablerate */
                if(strtolower($tablerate->getData('dest_country_id')) == strtolower($selectedCountry)){
                    $shippingAmount = Mage::helper('core')->formatPrice($tablerate->getData('price'), true);
                    echo $shippingAmount;die;
                }
            }
            echo Mage::helper('core')->formatPrice(10, true);
        }

    }

    function checkIsContainPhysic()
    {
        $session = Mage::getSingleton('checkout/session');
        foreach ($session->getQuote()->getAllItems() as $item) {
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            if($product->getTypeId() != 'downloadable')
                return true;
        }
        return false;
    }

    /**
     * Add selected products from checkout thankyou page to cart
     */
    public function addInterestProductsAction()
    {
        $productIds = $this->getRequest()->getPost('product_ids');
        $productIdsArr = explode(',',$productIds);
        $cart = $this->_getCart();
        foreach($productIdsArr as $id){
            if($id!=''){
                try {
                    $qty = 1;
                    $product = Mage::getModel('catalog/product')
                        ->setStoreId(Mage::app()->getStore()->getId())
                        ->load($id);
                    $cart->addProduct($product, $qty);
                    $message = $this->__('%s was successfully added to your shopping cart.', $product->getName());
                    Mage::getSingleton('checkout/session')->addSuccess($message);
                }catch (Mage_Core_Exception $e) {
                    if (Mage::getSingleton('checkout/session')->getUseNotice(true)) {
                        Mage::getSingleton('checkout/session')->addNotice($product->getName() . ': ' . $e->getMessage());
                    }
                    else {
                        Mage::getSingleton('checkout/session')->addError($product->getName() . ': ' . $e->getMessage());
                    }
                }
                catch (Exception $e) {
                    Mage::getSingleton('checkout/session')->addException($e, $this->__('Can not add item to shopping cart'));
                }
            }
        }
        $cart->save();
        $this->_redirect('onepage');
    }

    public function setShippingMethodAction()
    {
        $method = $this->getRequest()->getPost('method');
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $shippingAddress = $quote->getShippingAddress();
        $shippingAddress->setShippingMethod($method)->setCollectShippingRates(true);
        $shippingAddress->save();
        $quote->save();
        return 'ok';
    }

    /**
     * Retrieve shopping cart model object
     *
     * @return Mage_Checkout_Model_Cart
     */
    protected function _getCart()
    {
        return Mage::getSingleton('checkout/cart');
    }
}