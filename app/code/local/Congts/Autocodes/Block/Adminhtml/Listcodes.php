<?php

class Congts_Autocodes_Block_Adminhtml_Listcodes extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'congts_autocodes';
        $this->_controller = 'adminhtml_listcodes';
        $this->_headerText      = $this->__('Auto code list manager');
        // $this->_addButtonLabel  = $this->__('Add Button Label');
        parent::__construct();
        $this->_removeButton('add');
    }

    public function getCreateUrl()
    {
        return $this->getUrl('*/*/new');
    }

}

