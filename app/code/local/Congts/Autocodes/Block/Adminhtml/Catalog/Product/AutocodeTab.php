<?php

class Congts_Autocodes_Block_Adminhtml_Catalog_Product_AutocodeTab extends Mage_Adminhtml_Block_Template
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    /**
     * Set the template for the block
     *
     */
    public function _construct()
    {
        parent::_construct();

        $this->setTemplate('autocodes/autocodetab.phtml');
    }

    /**
     * Retrieve the label used for the tab relating to this block
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->__('Auto Codes');
    }

    /**
     * Retrieve the title used by this tab
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->__('Auto Codes');
    }

    /**
     * Determines whether to display the tab
     * Add logic here to decide whether you want the tab to display
     *
     * @return bool
     */
    public function canShowTab()
    {
        $product = Mage::registry('current_product');

        //only show tab with Downloadable Products
        if ($product->getTypeId() == 'downloadable') {
            return true;
        }
        return false;
    }

    /**
     * Stops the tab being hidden
     *
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    public function getFieldSuffix()
    {
        return 'product';
    }

    /**
     * @return array|bool|string
     */
    public function getAutocodeEnabledValue()
    {
        $product = Mage::registry('current_product');
        $_item = $product->getId();
        $_resource = $product->getResource();
        return $_resource->getAttributeRawValue($_item, 'autocode_enabled', Mage::app()->getStore());
    }

    /**
     * @param $status
     * @return Congts_Autocodes_Model_Resource_Autocodes_Collection
     */
    protected function _getAutocodesByStatus($status)
    {
        $product = Mage::registry('current_product');
        $autocodesCollection = Mage::getModel('congts_autocodes/autocodes')->getCollection();
        $autocodesItems = $autocodesCollection->addFieldToFilter('sku', $product->getSku())
                                             ->addFieldToFilter('status', $status);
        return $autocodesItems;
    }

    /**
     * get available list codes
     *
     * @return string
     */
    public function getAvailabelListCodes()
    {
        $autocodesItems = $this->_getAutocodesByStatus(0);
        $listCodes = '';
        foreach ($autocodesItems as $autocodesItem) {
            $listCodes .= $autocodesItem->getCode()."\n";
        }
        return $listCodes;
    }

    /**
     * get used list codes
     *
     * @return string
     */
    public function getUsedListCodes()
    {
        $autocodesItems = $this->_getAutocodesByStatus(1);
        $listCodes = '';
        foreach ($autocodesItems as $autocodesItem) {
            $listCodes .= $autocodesItem->getCode()."\n";
        }
        return $listCodes;
    }

    public function getListUrlBySku()
    {
        $product = Mage::registry('current_product');
        $filter = base64_encode('sku='.$product->getSku());
        return Mage::helper("adminhtml")->getUrl("adminhtml/listcodes", array("filter"=> $filter));
    }

    public function getAutocodeInstruction()
    {
        return Mage::registry('current_product')->getData('autocode_instruction');
    }
}