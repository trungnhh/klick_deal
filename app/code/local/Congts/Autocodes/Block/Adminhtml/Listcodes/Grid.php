<?php

class Congts_Autocodes_Block_Adminhtml_Listcodes_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('grid_id');
        // $this->setDefaultSort('COLUMN_ID');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('congts_autocodes/autocodes')->getCollection();

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('congts_autocodes')->__('ID'),
            'index' => 'id',
            'width' => '20px',
            'type' => 'number'
        ));

        $this->addColumn('sku', array(
            'header' => Mage::helper('congts_autocodes')->__('SKU'),
            'align' => 'left',
            'width' => '120px',
            'index' => 'sku'
        ));

        $this->addColumn('code', array(
            'header' => Mage::helper('congts_autocodes')->__('Serial Code'),
            'align' => 'left',
            'width' => '500px',
            'index' => 'code'
        ));

        $this->addColumn('customer_id', array(
            'header' => Mage::helper('congts_autocodes')->__('Customer'),
            'align' => 'left',
            'width' => '120px',
            'index' => 'customer_id',
            'renderer'  => 'Congts_Autocodes_Block_Adminhtml_Listcodes_Renderer_Customer'
        ));

        $this->addColumn('order_increment_id', array(
            'header' => Mage::helper('congts_autocodes')->__('Order ID'),
            'align' => 'left',
            'width' => '120px',
            'index' => 'order_increment_id',
            'renderer'  => 'Congts_Autocodes_Block_Adminhtml_Listcodes_Renderer_Order'
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('congts_autocodes')->__('Created'),
            'align' => 'left',
            'width' => '120px',
            'type' => 'datetime',
            'default' => '--',
            'index' => 'created_at'
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('congts_autocodes')->__('Updated'),
            'align' => 'left',
            'width' => '120px',
            'type' => 'datetime',
            'default' => '--',
            'index' => 'updated_at'
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('congts_autocodes')->__('Status'),
            'align' => 'left',
            'width' => '85px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                0 => Mage::helper('congts_autocodes')->__('Available'),
                1 => Mage::helper('congts_autocodes')->__('Used'),
                2 => Mage::helper('congts_autocodes')->__('Pending')
            )
        ));

        $this->addColumn('action', array(
            'header'    => ' ',
            'filter'    => false,
            'sortable'  => false,
            'width'     => '100px',
            'renderer'  => 'Congts_Autocodes_Block_Adminhtml_Listcodes_Renderer_Action'
        ));

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return false;
        //return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('congts_autocodes/autocodes')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('resend', array(
            'label' => $this->__('Resend All'),
            'url' => $this->getUrl('*/*/massSendEmail'),
        ));
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
}
