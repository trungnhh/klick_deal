<?php

class Congts_Autocodes_Block_Adminhtml_Listcodes_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        if ($row->getOrderIncrementId() != "") {
            $orderUrl = Mage::helper('adminhtml')->getUrl("adminhtml/listcodes/sendEmail", array('order_increment_id'=> $row->getOrderIncrementId()));
            return '<a href="' . $orderUrl . '">' . Mage::helper('congts_autocodes')->__('Resend') . '</a>';
        }

    }
}