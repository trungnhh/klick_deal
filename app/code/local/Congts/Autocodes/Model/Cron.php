<?php

class Congts_Autocodes_Model_Cron
{
    /**
     * Check and send email daily all products have the autocodes less than 20
     * Time: 1h5p AM
     */
    public function sendEmailNotifyAutocodesDaily()
    {
        $number = Mage::helper('congts_autocodes')->getDailyNumberWarning();
        $listDaily = $this->_getListAutocodesLessThan($number);
        Mage::log($listDaily,null,'autocode_daily_warning.log');
        if ($listDaily) {
            $this->_sendNotifyEmail($listDaily, $number);
        }
    }

    /**
     * Check and send email hourly all products have the autocodes less than 10
     * and re-send Invoice Email to orders are out of autocode
     */
    public function sendEmailNotifyAutocodesHourly()
    {
        //send invoice Email to Orders are out of autocode
        $this->_sendInvoiceEmailToOrdersOutOfAutocode();
        $number = Mage::helper('congts_autocodes')->getHourlyNumberWarning();

        $listHourly = $this->_getListAutocodesLessThan($number);
        Mage::log($listHourly,null,'autocode_hourly_warning.log');
        if ($listHourly) {
            $this->_sendNotifyEmail($listHourly, $number);
        }
    }

    /**
     * list SKUs of downloadable products that enable "autocodes" have
     * amount of autocodes less than $amount
     * @param $amount
     * @return array
     */
    protected function _getListAutocodesLessThan($amount)
    {
        if ($amount == '' || $amount <= 0) return false;

        $listProductSku = array();
        $productCollection = Mage::getModel('catalog/product')->getCollection()
                            ->addAttributeToSelect('name')
                            ->addAttributeToSelect('sku')
                            ->addAttributeToSelect('type_id')
                            ->addAttributeToSelect('autocode_enabled');
        $productItems = $productCollection->addAttributeToFilter('type_id', array('eq' => 'downloadable'))
                          ->addAttributeToFilter('autocode_enabled', array('eq' => '1'));

        foreach ($productItems as $productItem) {
            $skuItem = $productItem->getSku();
            $autocodesModel = Mage::getModel('congts_autocodes/autocodes');
            $autocodesItems = $autocodesModel->getCollection()
                ->addFieldToFilter('status', 0)
                ->addFieldToFilter('sku', $skuItem);

            //check amount of autocodes
            if (count($autocodesItems) <= $amount) {
                $listProductSku[$skuItem] = array(
                    'qty' => count($autocodesItems),
                    'name' => $productItem->getName()
                );
            }
        }

        if (!empty($listProductSku)) {
            return $listProductSku;
        } else {
            return false;
        }
    }

    /**
     * @param $listProductLessThan
     * @param $amount
     * @return bool
     */
    protected function _sendNotifyEmail($listProductLessThan, $amount)
    {
        $listProductHtml = '';
        foreach ($listProductLessThan as $productSKU => $item) {
            $listProductHtml .= "<td valign='top' style='padding:10px'>" . $productSKU . "</td>";
            $listProductHtml .= "<td valign='top' style='padding:10px'>" . $item['name'] . "</td>";
            $listProductHtml .= "<td valign='top' style='padding:10px'>" . $item['qty'] . "</td>";
            $listProductHtml .= "</tr><tr>";
        }
        $emailVars = array(
            'products'	=> $listProductHtml,
            'amount'	=> $amount,
        );
        if (is_numeric($template = Mage::helper('congts_autocodes')->getLowStockNotifyTemplate())) {
            $emailTemplate = Mage::getSingleton('core/email_template')->load($template);
        } else {
            $emailTemplate = Mage::getSingleton('core/email_template')->loadDefault($template);
        }

        $listEmail = Mage::helper('congts_autocodes')->getLowStockNotifyEmail();

        if ($listEmail != "") {
            $emails = explode(',',$listEmail);
            $emails = array_map('trim', $emails);
            $emails = array_filter($emails);
        } else {
            return false;
        }

        if (empty($emails)) return false;
        $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_general/name'));
        $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
        $emailTemplate->send(
            $emails,
            'Administrator',
            $emailVars);

        return true;
    }

    /**
     * Send invoice Email to orders are out of autocode
     */
    protected function _sendInvoiceEmailToOrdersOutOfAutocode()
    {
        $invoicesOutofAutocodes = Mage::helper('congts_autocodes')->getInvoicesOutofAutocodes();

        if ($invoicesOutofAutocodes) {
            $invoiceCollection = Mage::getModel('sales/order_invoice')->getCollection();
            $invoiceCollection->addFieldToFilter('increment_id',array('in' => $invoicesOutofAutocodes))->load();

            foreach ($invoiceCollection as $invoice) {
                $isSendMail = false;
                foreach ($invoice->getAllItems() as $item) {
                    $skuItem = $item->getSku();
                    $autocodesModel = Mage::getModel('congts_autocodes/autocodes');
                    $autocodesItems = $autocodesModel->getCollection()
                        ->addFieldToFilter('status', 0)
                        ->addFieldToFilter('sku', $skuItem);
                    if (count($autocodesItems) >= $item->getQty()) {
                        Mage::getModel('congts_autocodes/autocodes')->setAutocodesToInvoiceItem($item, $invoice);
                        $isSendMail = true;
                    }
                }
                if ($isSendMail) {
                    if(($key = array_search($invoice->getIncrementId(), $invoicesOutofAutocodes)) !== false) {
                        unset($invoicesOutofAutocodes[$key]);
                        Mage::helper('congts_autocodes')->setInvoicesOutofAutocodes($invoicesOutofAutocodes);
                    }
                    $invoice->sendEmail();
                }
            }
        }
    }

}