<?php

class Congts_Autocodes_Model_Observer
{
    /**
     * hook to sales_order_invoice_save_before event
     * assign Autocode to Downloadable product
     * @param $observer
     */
    public function salesOrderInvoiceSaveBefore($observer)
    {
        $invoice = $observer->getEvent()->getInvoice();
        foreach ($invoice->getAllItems() as $item) {
            if ($item->getAutocode()=='' || $item->getAutocode()=='outofcode') {
                Mage::getModel('congts_autocodes/autocodes')->setAutocodesToInvoiceItem($item, $invoice);
            }
        }
    }

    /**
     * hook to catalog_product_save_after event
     *
     * @param $observer
     */
    public function catalogProductPrepareSave($observer)
    {
        $request = Mage::app()->getRequest();
        $productData = $request->getPost('product');

        if(is_object($observer->getEvent()->getData('product'))){
            $resource = $observer->getEvent()->getData('product');
            $productType = $observer->getEvent()->getData('product')->type_id;

            if($productType == 'downloadable'){

                $_product = Mage::registry('current_product');

                $dataFile = $resource->downloadable_data['link'][0]['file'];

                if($dataFile){
                    $dataSize = '';
                    $size = '';
                    $size = '';
                    $fileSize = 0;
                    if($dataFile != ''){
                        $dataFile = explode(',', $dataFile);
                        $dataSize = str_replace('"','',$dataFile[2]);

                        $size = explode(':', $dataSize);
                        $dataType = str_replace('"','',$dataFile[1]);
                        $type = explode(':', $dataType);

                        $dataStatus = str_replace('"','',$dataFile[3]);
                        $status = explode(':', $dataStatus);

                        if($size[1]!= ''){
                            $fileSize = intval($size[1]);
                            if ($fileSize >= 1073741824)
                            {
                                $fileSize = number_format($fileSize / 1073741824, 2) . ' GB';
                            }
                            elseif ($fileSize >= 1048576)
                            {
                                $fileSize = number_format($fileSize / 1048576, 2) . ' MB';
                            }
                            elseif ($fileSize >= 1024)
                            {
                                $fileSize = number_format($fileSize / 1024, 2) . ' KB';
                            }
                            elseif ($fileSize > 1)
                            {
                                $fileSize = $fileSize . ' b';
                            }
                            elseif ($fileSize == 1)
                            {
                                $fileSize = $fileSize . ' b';
                            }
                            else
                            {
                                $fileSize = '0 b';
                            }

                        }
                        if($type[1]!= ''){
                            $fileType = explode('.', $type[1]);
                        }
                    }

                    $attrCodeSize = 'filesize';
                    $attrCodeType = 'format';
                    $attrBookFormat = 'book_format';
                    $fileExt = $fileType[1];
                    if(strpos($status[1], 'new') !== false){
                        try {
                            //var_dump($_product->getData('book_format'));die('zzz');
                            if($_product->getData('book_format') === '' || count($_product->getData('book_format'))) {
                                //die('sss');
                                $attribute_details = Mage::getSingleton("eav/config")->getAttribute("catalog_product", $attrBookFormat);
                                $optionFormat = $attribute_details->getSource()->getAllOptions(false);
                                foreach ($optionFormat as $option) {
                                    if($fileExt == strtolower($option["label"])){

                                        $_product->setBookFormat($option["value"]);
                                    }
                                }
                                $_product->getResource()->saveAttribute($_product, $attrBookFormat);
                            }

//                                $fileSize = $fileSize . ' KB';
                            $_product->setFilesize($fileSize);
                            $_product->getResource()->saveAttribute($_product, $attrCodeSize);



                            //$attribute_code = "format";
                            if($_product->getData('format') === '' || count($_product->getData('format')) ){
                                $attribute_details = Mage::getSingleton("eav/config")->getAttribute("catalog_product", $attrCodeType);
                                $options = $attribute_details->getSource()->getAllOptions(false);
                                foreach ($options as $option) {
                                    //echo $option["value"];
                                    //echo $option["label"];
                                    if($fileExt == strtolower($option["label"])){
                                        $_product->setFormat($option["value"]);
                                    }
                                }
                                $_product->getResource()->saveAttribute($_product, $attrCodeType);
                            }

                        }catch(Exception $ex) {
                            Mage::log('Save downloadble product file error at Congts_Autocodes_Model_Observer');
                        }
                    }
                }


            }
        }

        if (isset($productData['autocode']) && $autocodes = $productData['autocode']) {
            if (isset($productData['autocode_enabled']) && $productData['autocode_enabled'] == 1
                && isset($autocodes['insert']) && $autocodes['insert'] != ''
            ) {
                $newAutocode = $autocodes['insert'];
                $newAutocodeArray = explode("\n",$newAutocode);
                foreach ($newAutocodeArray as $newAutocode) {
                    if (trim($newAutocode) == '') continue;
                    $autocodeModel = Mage::getModel('congts_autocodes/autocodes');
                    $autocodeModel->setSku($productData['sku']);
                    $autocodeModel->setCode(trim($newAutocode));
                    $autocodeModel->save();
                }
            }

            if ($productData['autocode_enabled'] && $autocodes['available']) {
                $updatedAutocode = trim($autocodes['available']);
                $updatedAutocodeArrayRaw = explode("\n", $updatedAutocode);
                $updatedAutocodeArray = array();
                foreach ($updatedAutocodeArrayRaw as $item) {
                    if (trim($item) == '') continue;
                    $updatedAutocodeArray[] = trim($item);
                }
                $currentAutocodeCollection = Mage::getModel('congts_autocodes/autocodes')->getCollection()
                    ->addFieldToFilter('sku', $productData['sku'])
                    ->addFieldToFilter('status', 0);
                $currentAutocodeArray = array();
                foreach ($currentAutocodeCollection as $item) {
                    $currentAutocodeArray[] = $item->getCode();
                }
                $insertCode = array_diff($updatedAutocodeArray, $currentAutocodeArray);
                $deleteCode = array_diff($currentAutocodeArray, $updatedAutocodeArray);
                foreach ($insertCode as $item) {
                    if (trim($item) == '') continue;
                    $autocodeModel = Mage::getModel('congts_autocodes/autocodes');
                    $autocodeModel->setSku($productData['sku']);
                    $autocodeModel->setCode(trim($item));
                    $autocodeModel->save();
                }

                if ($deleteCode) {
                    $deleteCodeWithinSlashArray = array();
                    foreach ($deleteCode as $item) {
                        $deleteCodeWithinSlashArray[] = "'" . $item . "'";
                    }
                    $joinedDeleteCode = join(",", $deleteCodeWithinSlashArray);
                    $writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
                    $query = "DELETE FROM autocode WHERE sku = '". $productData['sku'] ."'" . " AND code IN ({$joinedDeleteCode})";
                    $writeConnection->query($query);
                }
            }
        }
    }

}