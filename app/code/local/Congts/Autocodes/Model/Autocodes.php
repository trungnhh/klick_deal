<?php

class Congts_Autocodes_Model_Autocodes extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('congts_autocodes/autocodes');
    }

    /**
     * @param $item
     * @param $order
     * @param $invoice
     */
    public function setAutocodesToInvoiceItem($item, $invoice)
    {
        $order = $invoice->getOrder();
        $skuItem = $item->getSku();
        $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $skuItem);
        $_resource = $product->getResource();
        $autoCodeEnabled = $_resource->getAttributeRawValue($product->getId(), 'autocode_enabled', Mage::app()->getStore());
        if ($item->getQty() > 0 && $product->getTypeId() == 'downloadable' && $autoCodeEnabled) {
            $autocodesItems = $this->getCollection()
                ->addFieldToFilter('status', 1)
                ->addFieldToFilter('sku', $skuItem)
                ->addFieldToFilter('order_increment_id', $order->getIncrementId());
            if (count($autocodesItems)>0) {
                // Update autocode if sended
                $autoCodeItem = $autocodesItems->getFirstItem();
                $item->setAutocode($autoCodeItem->getCode());
            } else {
                // Send new autocode
                $autocodesItems = $this->getCollection()
                    ->addFieldToFilter('status', 0)
                    ->addFieldToFilter('sku', $skuItem);

                if (count($autocodesItems) < $item->getQty()) {
                    //Out of code need update product autocode on admin by edit auto code
                    $item->setAutocode('outofcode');
                    $invoicesOutofAutocodes = Mage::helper('congts_autocodes')->getInvoicesOutofAutocodes();
                    $invoicesOutofAutocodes[] = $invoice->getIncrementId();
                    Mage::helper('congts_autocodes')->setInvoicesOutofAutocodes($invoicesOutofAutocodes);
                } else {
                    // Update autocode
                    $code = '';
                    $count = 0;
                    foreach ($autocodesItems as $autocodesItem) {
                        $count++;
                        if ($count > $item->getQty()) break;
                        if ($count > 1) $code .= ',';

                        $code .= $autocodesItem->getCode();
                        $autocodesItem->setStatus(1);
                        $autocodesItem->setCustomerId($order->getCustomerId());
                        $autocodesItem->setOrderIncrementId($order->getIncrementId());
                        $autocodesItem->setUpdatedAt(Varien_Date::now());
                        $autocodesItem->save();
                    }

                    //Mage::log($code,null,'codes.log');
                    $item->setAutocode($code);
                }
            }
            //$item->save();
        }
    }
}
