<?php

class Congts_Autocodes_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isEnabled()
    {
        return Mage::getStoreConfig('autocodes/general/enabled');
    }

    public function getLowStockNotifyEmail()
    {
        return trim(Mage::getStoreConfig('autocodes/configuration/notify_email'));
    }

    public function getLowStockNotifyTemplate()
    {
        return Mage::getStoreConfig('autocodes/configuration/notify_email_template');
    }

    public function getResendAutocodesTemplate()
    {
        return Mage::getStoreConfig('autocodes/configuration/resend_autocode_template');
    }

    public function getInvoicesOutofAutocodes()
    {
        if (!Mage::getStoreConfig('autocodes/configuration/invoices_outofautocodes')) return false;

        $invoices = explode("\n",trim(Mage::getStoreConfig('autocodes/configuration/invoices_outofautocodes')));
        $invoices = array_map('trim', $invoices);

        if (!empty($invoices)) {
            return $invoices;
        } else {
            return false;
        }
    }

    public function setInvoicesOutofAutocodes($invoiceOutofAutocodes)
    {
        $invoices = implode("\n",$invoiceOutofAutocodes);
        Mage::getConfig()->saveConfig('autocodes/configuration/invoices_outofautocodes', $invoices);
        Mage::app()->getConfig()->reinit();
    }

    public function hasDownloadable($orderId)
    {
        return true;
        $order = Mage::getModel('sales/order')->load($orderId);
        if(!$order){
            return false;
        }
        $items = $order->getAllVisibleItems();
        foreach($items as $i){
            $p = Mage::getModel('catalog/product')->load($i->getProductId());
            if($p->getTypeId() == Mage_Downloadable_Model_Product_Type::TYPE_DOWNLOADABLE){
                return true;
            }
        }
    }

    public function getFrontendResendKey($orderId)
    {
        return Mage::getUrl('autocode/index/resendKey/id/'.$orderId);
    }

    public function getDailyNumberWarning()
    {
        return Mage::getStoreConfig('autocodes/configuration/number_daily');
    }

    public function getHourlyNumberWarning()
    {
        return Mage::getStoreConfig('autocodes/configuration/number_hourly');
    }
}