<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('congts_autocodes/autocodes')};
CREATE TABLE {$this->getTable('congts_autocodes/autocodes')} (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sku` varchar(255) NOT NULL default '',
  `code` varchar(255) NOT NULL default '',
  `order_increment_id` varchar(255) NOT NULL default '',
  `customer_id` int(11) NULL,
  `status` smallint(6) NOT NULL default '0',
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

  ALTER TABLE {$this->getTable('sales_flat_invoice_item')} ADD COLUMN `autocode` varchar(255);
");

$installer->endSetup();