<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 1/14/2016
 * Time: 9:11 PM
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');

$installer->startSetup();

$installer->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'autocode_instruction',
    array(
        'label'                      => Mage::helper('congts_autocodes')->__('Outofcode instruction'),
        'group'                      => 'General',
        'type'                       => 'text',
        'input'                      => 'textarea',
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'user_defined'               => false,
        'required'                   => false,
        'visible'                    => false,
        'source'                     => null,
        'backend'                    => null,
        'searchable'                 => false,
        'visible_in_advanced_search' => false,
        'visible_on_front'           => false,
        'is_configurable'            => false,
        'is_html_allowed_on_front'   => false,
    )
);

$installer->endSetup();