<?php

class Congts_Autocodes_Adminhtml_ListcodesController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('congts_autocodes/adminhtml_listcodes'));
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName = '_export.csv';
        $content = $this->getLayout()->createBlock('congts_autocodes/adminhtml_listcodes_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportExcelAction()
    {
        $fileName = '_export.xml';
        $content = $this->getLayout()->createBlock('congts_autocodes/adminhtml_listcodes_grid')->getExcel();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select (s).'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getSingleton('congts_autocodes/autocodes')->load($id);
                    $model->delete();
                }

                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('congts_autocodes')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function sendEmailAction()
    {
        $orderIncrementId = $this->getRequest()->getParam('order_increment_id');

        if ($orderIncrementId == "") {
            $this->_getSession()->addError($this->__('Invalid Order Id'));
        } else {
            try {
                $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);

                if ($order->hasInvoices()) {
                    foreach ($order->getInvoiceCollection() as $invoice) {
                        $invoice->save();
                        //$invoice->sendEmail();
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Send Invoice Emails of Order #%s successfully.', $orderIncrementId)
                    );
                } else {
                    $this->_getSession()->addError(
                        $this->__('Can not found any invoice in order #%s .', $orderIncrementId)
                    );
                }

            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('congts_autocodes')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }

        $this->_redirect('*/*/index');
    }

    public function massSendEmailAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select (s).'));
        } else {
            try {
                $autocodeCollection = Mage::getModel('congts_autocodes/autocodes')->getCollection();
                $autocodes = $autocodeCollection->addFieldToFilter('id', array('in' => $ids));

                $autocodeListOrderIds = array();
                $autocodelistNonOrderIds = array();
                $message = array();
                foreach ($autocodes as $autocode) {
                    if ($autocode->getOrderIncrementId() != '') {
                        $autocodeListOrderIds[] = $autocode->getOrderIncrementId();
                    } else {
                        $autocodelistNonOrderIds[] = $autocode->getId();
                    }
                }

                if (!empty($autocodelistNonOrderIds)) {
                    $this->_getSession()->addError(
                        $this->__('Can not found Order Id of IDs: %s.', implode(",",$autocodelistNonOrderIds))
                    );
                }

                if (!empty($autocodeListOrderIds)) {
                    foreach ($autocodeListOrderIds as $autocodeListOrderId) {
                        $order = Mage::getModel('sales/order')->loadByIncrementId($autocodeListOrderId);
                        if ($order->hasInvoices()) {
                            foreach ($order->getInvoiceCollection() as $invoice) {
                                $invoice->sendEmail();
                            }
                            $message['success'][] = $autocodeListOrderId;
                        } else {
                            $message['errors'][] = $autocodeListOrderId;
                        }
                    }
                }

                if (!empty($message['success'])) {
                    $this->_getSession()->addSuccess(
                        $this->__('Send Invoice Emails of Orders: %s successfully.', implode(",",$message['success']))
                    );
                }
                if (!empty($message['errors'])) {
                    $this->_getSession()->addError(
                        $this->__('Can not found any invoice in orders: %s .', implode(",",$message['errors']))
                    );
                }
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('congts_autocodes')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }
    
    /**
     * Check is allowed access to action
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}