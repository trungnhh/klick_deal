<?php
class Congts_Autocodes_IndexController extends Mage_Core_Controller_Front_Action
{
    public function resendKeyAction()
    {
        $orderId = $this->getRequest()->getParam('id');
        if ($orderId == "") {
            $this->_getSession()->addError($this->__('Invalid Order Id'));
            $this->_redirect('customer/account/');
        } else {
            try {
                $order = Mage::getModel('sales/order')->load($orderId);
                $orderIncrementId = $order->getIncrementId();
                if ($order->hasInvoices()) {
                    foreach ($order->getInvoiceCollection() as $invoice) {
                        $invoice->sendEmail();
                    }
                    $this->_getSession()->addSuccess(
                        $this->__('Send Invoice Emails of Order #%s successfully.', $orderIncrementId)
                    );
                } else {
                    $this->_getSession()->addError(
                        $this->__('Can not found any invoice in order #%s .', $orderIncrementId)
                    );
                }

            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('congts_autocodes')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
            }

            $this->_redirect('sales/order/view/order_id/'.$orderId);
        }
    }

    public function requestSendKeyAction()
    {
        $productId = $this->getRequest()->getParam('id');
        if ($productId == "" || !Mage::getSingleton('customer/session')->isLoggedIn()) {
            $this->_getSession()->addError($this->__('Can not resend Autocodes'));
        } else {
            $customerData = Mage::getSingleton('customer/session')->getCustomer();
            $customerId = $customerData->getId();
            $orders = Mage::getModel("sales/order")->getCollection()->addFieldToFilter('customer_id', $customerId);
            $orderIds = array();
            foreach ($orders as $order){
                $orderIds[$order->getIncrementId()] = $order->getId();
            }
            $orderIncrementIds = array_flip($orderIds);
            $invoiceCollection = Mage::getModel("sales/order_invoice_item")->getCollection()
                ->join(array('invoice' => 'sales/invoice'), 'main_table.parent_id = invoice.entity_id', [])
                ->addFieldToFilter('product_id', $productId)
                ->addFieldToFilter('invoice.order_id',array('in' => $orderIds));

            $listAutocodesHtml = '';
            $productNameHtml = '';
            $outOfAutocode = 0;
            foreach ($invoiceCollection as $invoice) {
                $autocode = ($invoice->getAutocode() == 'outofcode')? $this->__('This product is temporarily out of autocode, we will send to you later') : $invoice->getAutocode();
                $listAutocodesHtml .= "<tr>";
                $listAutocodesHtml .= "<td valign='top' style='padding:10px'>" . $autocode . "</td>";
                $listAutocodesHtml .= "<td valign='top' style='padding:10px'>";
                $listAutocodesHtml .= "<a href='" . Mage::getUrl('sales/order/view', array('order_id'=> $invoice->getOrderId())) . "' title='". $orderIncrementIds[$invoice->getOrderId()] ."'>".$orderIncrementIds[$invoice->getOrderId()]."</a>";
                $listAutocodesHtml .= "</td>";
                $listAutocodesHtml .= "</tr>";

                $productNameHtml = $invoice->getName();
                if ($invoice->getAutocode() == 'outofcode') {
                    $outOfAutocode++;
                }
            }

            $product = Mage::getModel('catalog/product')->load($productId);
            $autocodeInstruction = $product->getData('autocode_instruction');

            if ($this->_sendEmail($customerData, $productNameHtml, $listAutocodesHtml, $outOfAutocode, $autocodeInstruction)) {
                $this->_getSession()->addSuccess(
                    $this->__('Send Autocodes of Product #%s successfully.', $productNameHtml)
                );
            } else {
                $this->_getSession()->addError(
                    $this->__('Can not Send Autocodes of Product.', $productNameHtml)
                );
            }
        }

        $this->_redirect('customer/account/index');
    }
    protected function _getSession()
    {
        return Mage::getSingleton('core/session');
    }

    protected function _sendEmail($customerData, $productNameHtml, $listAutocodesHtml, $outOfAutocode, $autocodeInstruction)
    {
        $emailVars = array(
            'product_name' => $productNameHtml,
            'list_autocodes' => $listAutocodesHtml,
            'out_of_code' => (bool) $outOfAutocode,
            'autocode_instruction' => $autocodeInstruction,
            'customer_name' => $customerData->getName()
        );
        if (is_numeric($template = Mage::helper('congts_autocodes')->getResendAutocodesTemplate())) {
            $emailTemplate = Mage::getSingleton('core/email_template')->load($template);
        } else {
            $emailTemplate = Mage::getSingleton('core/email_template')->loadDefault($template);
        }
        $recepientEmail = $customerData->getEmail();

        if (!$recepientEmail) return false;
        $emailTemplate->setSenderName(Mage::getStoreConfig('trans_email/ident_support/name'));
        $emailTemplate->setSenderEmail(Mage::getStoreConfig('trans_email/ident_support/email'));
        $emailTemplate->send(
            $recepientEmail,
            'Administrator',
            $emailVars);

        return true;
    }
}
