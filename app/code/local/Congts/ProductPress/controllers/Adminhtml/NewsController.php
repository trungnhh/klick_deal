<?php

/**
 * Product Press Extension
 *
 * @category   Congts
 * @package    Congts_ProductPress
 * @author     CongTS
 */
class Congts_ProductPress_Adminhtml_NewsController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('congts_productpress/adminhtml_news'));
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName = 'Product Press_export.csv';
        $content = $this->getLayout()->createBlock('congts_productpress/adminhtml_news_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportExcelAction()
    {
        $fileName = 'Product Press_export.xml';
        $content = $this->getLayout()->createBlock('congts_productpress/adminhtml_news_grid')->getExcel();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Product Press(s).'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getSingleton('congts_productpress/news')->load($id);
                    $model->delete();
                }

                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('congts_productpress')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('congts_productpress/news');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError(
                    Mage::helper('congts_productpress')->__('This Product Press no longer exists.')
                );
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('congts_product_press_model', $model);

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('congts_productpress/adminhtml_news_edit'))
            ->_addLeft($this->getLayout()->createBlock('congts_productpress/adminhtml_news_edit_tabs'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        $redirectBack = $this->getRequest()->getParam('back', false);
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('congts_productpress/news');
            if ($id) {
                $model->load($id);
                if (!$model->getId()) {
                    $this->_getSession()->addError(
                        Mage::helper('congts_productpress')->__('This Product Press no longer exists.')
                    );
                    $this->_redirect('*/*/index');
                    return;
                }
            }

            // save model
            try {
                if (isset($data['image'])) unset($data['image']); //unset image data

//                var_dump($data);die;
                $model->addData($data);
                $valueImage = $this->getRequest()->getParam('image');
                if (is_array($valueImage) && !empty($valueImage['delete'])) {
                    $model->setImage('');
                } elseif ($fileName = Mage::helper('congts_productpress')->saveImage()) {
                    $model->setImage($fileName);
                }

                $model->setUpdatedAt(Varien_Date::now());
                $this->_getSession()->setFormData($data);
                $model->save();

                Mage::dispatchEvent('adminhtml_productpress_save_after', array(
                    'productpress_news'  => $data,
                ));

                $this->_getSession()->setFormData(false);
                $this->_getSession()->addSuccess(
                    Mage::helper('congts_productpress')->__('The Product Press has been saved.')
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('congts_productpress')->__('Unable to save the Product Press.'));
                $redirectBack = true;
                Mage::logException($e);
            }

            if ($redirectBack) {
                $this->_redirect('*/*/edit', array('id' => $model->getId()));
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                // init model and delete
                $model = Mage::getModel('congts_productpress/news');
                $model->load($id);
                if (!$model->getId()) {
                    Mage::throwException(Mage::helper('congts_productpress')->__('Unable to find a Product Press to delete.'));
                }
                $model->delete();
                // display success message
                $this->_getSession()->addSuccess(
                    Mage::helper('congts_productpress')->__('The Product Press has been deleted.')
                );
                // go to grid
                $this->_redirect('*/*/index');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('congts_productpress')->__('An error occurred while deleting Product Press data. Please review log and try again.')
                );
                Mage::logException($e);
            }
            // redirect to edit form
            $this->_redirect('*/*/edit', array('id' => $id));
            return;
        }
        // display error message
        $this->_getSession()->addError(
            Mage::helper('congts_productpress')->__('Unable to find a Product Press to delete.')
        );
        // go to grid
        $this->_redirect('*/*/index');
    }

    public function productsAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('productpress_news.grid.products');
        $this->renderLayout();
    }

    /**
     * Check is allowed access to action
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}