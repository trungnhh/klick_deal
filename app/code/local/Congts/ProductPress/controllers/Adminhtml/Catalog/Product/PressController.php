<?php

/**
 * Product Press Extension
 *
 * @category   Congts
 * @package    Congts_ProductPress
 * @author     CongTS
 */
require_once(Mage::getModuleDir('controllers','Mage_Adminhtml').DS.'Catalog'.DS.'ProductController.php');
class Congts_ProductPress_Adminhtml_Catalog_Product_PressController extends Mage_Adminhtml_Catalog_ProductController
{
    /**
     * construct
     * @access protected
     * @return void
     *
     */
    protected function _construct()
    {
        // Define module dependent translate
        $this->setUsedModuleName('Congts_ProductPress');
    }

    /**
     * articles in the catalog page
     * @access public
     * @return void
     *
     */
    public function pressAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('product.edit.tab.press')
            ->setProductPress($this->getRequest()->getPost('product_press', null));
        $this->renderLayout();
    }

    /**
     * articles grid in the catalog page
     * @access public
     * @return void
     *
     */
    public function pressGridAction()
    {
        $this->_initProduct();
        $this->loadLayout();
        $this->getLayout()->getBlock('product.edit.tab.press')
            ->setProductPress($this->getRequest()->getPost('product_press', null));
        $this->renderLayout();
    }

    /**
     * Check is allowed access to action
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}