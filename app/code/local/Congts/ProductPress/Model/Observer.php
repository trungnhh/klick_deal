<?php

/**
 * Product Press Extension
 *
 * @category   Congts
 * @package    Congts_ProductPress
 * @author     CongTS
 */
class Congts_ProductPress_Model_Observer
{
    /**
     * check if tab can be added
     * @access protected
     * @param Mage_Catalog_Model_Product $product
     * @return bool
     *
     */
    protected function _canAddTab($product)
    {
        if ($product->getId()) {
            return true;
        }
        if (!$product->getAttributeSetId()) {
            return false;
        }
        $request = Mage::app()->getRequest();
        if ($request->getParam('type') == 'configurable') {
            if ($request->getParam('attributes')) {
                return true;
            }
        }
        return false;
    }

    /**
     * hook to core_block_abstract_prepare_layout_after event
     * add the Press tab to products
     *
     * @param $observer
     * @return Congts_ProductPress_Model_Observer
     *
     */
    public function coreBlockAbstractPrepareLayoutAfter($observer)
    {
        $block = $observer->getEvent()->getBlock();
        $product = Mage::registry('product');
        if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs && $this->_canAddTab($product)) {
            $block->addTab('press', array(
                'label' => Mage::helper('congts_productpress')->__('Product Press'),
                'url' => Mage::helper('adminhtml')->getUrl('adminhtml/catalog_product_press/press', array('_current' => true)),
                'class' => 'ajax',
            ));
        }
        return $this;
    }

    /**
     * hook to catalog_product_save_before event
     * save product press data
     *
     * @param $observer
     * @return $this
     */
    public function catalogProductSaveBefore($observer)
    {
        $product = $observer->getEvent()->getProduct();
        $productNews = Mage::app()->getRequest()->getPost('product_press');

        if (isset($productNews)) {
            $newsArr = Mage::helper('adminhtml/js')->decodeGridSerializedInput($productNews);
            $productId = $product->getId();

            //remove news of product
            $newsByProductData = Mage::getModel('congts_productpress/news')->getCollection()
                ->addFieldToFilter('product_id', $productId);
            if (count($newsByProductData) > 0) {
                foreach ($newsByProductData as $news) {
                    if (!$newsArr || !in_array($news->getId(), $newsArr)) {
                        $news->setProductId(0);
                    }

                    try {
                        $news->save();
                    } catch (Mage_Core_Exception $e) {
                        Mage::log($e->getMessages());
                    } catch (Exception $e) {
                        Mage::log($e->getMessages());
                    }
                }
            }

            //update product_id to news
            if ($newsArr) {
                $newsData = Mage::getModel('congts_productpress/news')->getCollection()
                    ->addFieldToFilter('id', array('in' => $newsArr));
                if (count($newsData) > 0) {
                    foreach ($newsData as $news) {
                        $news->setProductId($productId);

                        try {
                            $news->save();
                        } catch (Mage_Core_Exception $e) {
                            Mage::log($e->getMessages());
                        } catch (Exception $e) {
                            Mage::log($e->getMessages());
                        }
                    }
                }
            }

            $product->setProductPress($productNews);
        }

        return $this;
    }

    /**
     * hook to adminhtml_productpress_save_after event
     * update news to product
     *
     * @param $observer
     * @return $this
     */
    public function adminhtmlProductpressSaveAfter($observer)
    {
        $newsData = $observer->getEvent()->getProductpressNews();
        if (isset($newsData['product_id']) && $newsData['product_id'] != 0) {
            $productId = $newsData['product_id'];
            if ($this->_getNewsIdByProductId($productId)) {
                $newsId = $this->_getNewsIdByProductId($productId);
                $product = Mage::getModel('catalog/product')->load($productId);

                if ($product->getProductPress() != '') {
                    $productNews = $product->getProductPress();
                    $newsArr = Mage::helper('adminhtml/js')->decodeGridSerializedInput($productNews);
                    if ($newsArr && !in_array($newsId, $newsArr)) {
                        $newsArr[] = $newsId;
                        $news = implode('&', $newsArr);
                        $product->setProductPress($news);
                    }
                } else {
                    $product->setProductPress($newsId);
                }

                try {
                    $product->save();
                } catch (Mage_Core_Exception $e) {
                    Mage::log($e->getMessages());
                } catch (Exception $e) {
                    Mage::log($e->getMessages());
                }
            }
        }

        return $this;
    }

    /**
     * get news id by product id
     *
     * @param $productId
     * @return false| int
     */
    protected function _getNewsIdByProductId($productId)
    {
        $newsCollection = Mage::getModel('congts_productpress/news')->getCollection()
                        ->addFieldToFilter('product_id', $productId);

        if (count($newsCollection) > 0) {
            $new = $newsCollection->getFirstItem();
            return $new->getId();
        }

        return false;
    }
}