<?php

/**
 * Product Press Extension
 *
 * @category   Congts
 * @package    Congts_ProductPress
 * @author     CongTS
 */
class Congts_ProductPress_Model_Resource_News_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct()
    {
        $this->_init('congts_productpress/news');
    }

}