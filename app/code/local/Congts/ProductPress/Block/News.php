<?php
/**
 * Product Press Extension
 *
 * @category   Congts
 * @package    Congts_ProductPress
 * @author     CongTS
 */
class Congts_ProductPress_Block_News extends Mage_Core_Block_Template
{
    /**
     * get News data
     *
     * @return bool|Congts_ProductPress_Block_News
     */
    public function getNewsData()
    {
        $productPress = $this->getProductPress();

        if ($productPress == '' || $productPress == 'on') return false;

        $productNews = Mage::helper('adminhtml/js')->decodeGridSerializedInput($productPress);
        $newCollection = Mage::getModel('congts_productpress/news')->getCollection()
            ->addFieldToFilter('id', array('in' => $productNews))
            ->addFieldToFilter('status', 1)
            ->setOrder('created_at', 'desc');

        if (count($newCollection) > 0) {
            return $newCollection;
        } else {
            return false;
        }
    }
}