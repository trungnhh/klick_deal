<?php
/**
 * Product Press Extension
 *
 * @category   Congts
 * @package    Congts_ProductPress
 * @author     CongTS
 */
class Congts_ProductPress_Block_Adminhtml_News_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('productpress_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->_getHelper()->__('Item Information'));
    }

    protected function _getHelper()
    {
        return Mage::helper('congts_productpress');
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => $this->_getHelper()->__('Press News'),
            'title'     => $this->_getHelper()->__('Press News'),
            'content'   => $this->getLayout()->createBlock('congts_productpress/adminhtml_news_edit_tab_form')->toHtml(),
        ));

        $this->addTab('grid_section', array(
            'label'     => $this->_getHelper()->__('Press Product'),
            'title'     => $this->_getHelper()->__('Press Product'),
            'url'       => $this->getUrl('*/*/products', array('_current' => true)),
            'class'     => 'ajax',
        ));

        return parent::_beforeToHtml();
    }
}