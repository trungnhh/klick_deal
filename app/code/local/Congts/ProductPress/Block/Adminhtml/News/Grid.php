<?php

/**
 * Product Press Extension
 *
 * @category   Congts
 * @package    Congts_ProductPress
 * @author     CongTS
 */
class Congts_ProductPress_Block_Adminhtml_News_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('grid_id');
         $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('congts_productpress/news')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('congts_productpress')->__('ID'),
            'index' => 'id',
            'width' => '20px',
            'type' => 'number'
        ));

        $this->addColumn('title', array(
            'header' => Mage::helper('congts_productpress')->__('Title'),
            'index' => 'title',
            'width' => '500px',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('congts_productpress')->__('Created'),
            'index' => 'created_at',
            'type' => 'datetime',
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('congts_productpress')->__('Last Modified'),
            'index' => 'updated_at',
            'type' => 'datetime',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('congts_productpress')->__('Status'),
            'align' => 'left',
            'width' => '85px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                0 => Mage::helper('congts_productpress')->__('Disabled'),
                1 => Mage::helper('congts_productpress')->__('Enabled'),
            )
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('congts_productpress')->__('Action'),
            'width' => '50px',
            'getter'    => 'getId',
            'type' => 'action',
            'actions' => array(
                array(
                    'caption' => Mage::helper('congts_productpress')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'is_system' => true,
        ));

        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('congts_productpress/news')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
}
