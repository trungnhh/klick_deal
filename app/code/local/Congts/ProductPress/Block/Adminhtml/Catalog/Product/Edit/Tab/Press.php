<?php

/**
 * Product Press Extension
 *
 * @category   Congts
 * @package    Congts_ProductPress
 * @author     CongTS
 */
class Congts_ProductPress_Block_Adminhtml_Catalog_Product_Edit_Tab_Press extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * Set grid params
     * @access protected
     * @return void
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('press_grid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);
        if ($this->getProduct()->getId()) {
            $this->setDefaultFilter(array('in_press' => 1));
        }
    }

    /**
     * prepare the article collection
     * @access protected
     * @return Congts_ProductPress_Block_Adminhtml_Catalog_Product_Edit_Tab_Press
     *
     */
    protected function _prepareCollection()
    {
        $productIds = array(0, $this->getProduct()->getId());
        $collection = Mage::getModel('congts_productpress/news')->getCollection()
                                ->addFieldToFilter('product_id',
                                    array(
                                        array('in' => $productIds),
                                        array('null' => true),
                                        ))
                                ->addFieldToFilter('status', 1);
        $this->setCollection($collection);

        parent::_prepareCollection();
        return $this;
    }

    /**
     * prepare mass action grid
     * @access protected
     * @return Congts_ProductPress_Block_Adminhtml_Catalog_Product_Edit_Tab_Press
     *
     */
    protected function _prepareMassaction()
    {
        return $this;
    }

    /**
     * prepare the grid columns
     * @access protected
     * @return Congts_ProductPress_Block_Adminhtml_Catalog_Product_Edit_Tab_Press
     *
     */
    protected function _prepareColumns()
    {
        $this->addColumn('in_press', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'in_press',
            'values' => $this->_getSelectedNews(),
            'align' => 'center',
            'index' => 'id'
        ));
        $this->addColumn('title', array(
            'header' => Mage::helper('congts_productpress')->__('Title'),
            'align' => 'left',
            'index' => 'title',
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('congts_productpress')->__('Last Modified'),
            'width' => '200px',
            'index' => 'updated_at',
            'type' => 'datetime',
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('congts_productpress')->__('Action'),
            'width' => '50px',
            'getter'    => 'getId',
            'type' => 'action',
            'actions' => array(
                array(
                    'caption' => Mage::helper('congts_productpress')->__('Edit'),
                    'url' => array('base' => 'adminhtml/news/edit'),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'is_system' => true,
        ));

        return parent::_prepareColumns();
    }

    /**
     * Retrieve selected press
     * @access protected
     * @return array
     *
     */
    protected function _getSelectedNews()
    {
        $press = $this->getProductPress();

        if (!is_array($press)) {
            $productNews = $this->getProduct()->getProductPress();
            $press = Mage::helper('adminhtml/js')->decodeGridSerializedInput($productNews);
        }
        return $press;
    }

    /**
     * Retrieve selected news
     * @access protected
     * @return array
     *
     */
    public function getSelectedNews()
    {
        return $this->_getSelectedNews();
    }

    /**
     * get row url
     * @access public
     * @return string
     *
     */
    public function getRowUrl($item)
    {
        return '#';
    }

    /**
     * get grid url
     * @access public
     * @return string
     *
     */
    public function getGridUrl()
    {
        return $this->getUrl('*/*/pressGrid', array(
            'id' => $this->getProduct()->getId()
        ));
    }

    /**
     * get the current product
     * @access public
     * @return Mage_Catalog_Model_Product
     *
     */
    public function getProduct()
    {
        return Mage::registry('current_product');
    }

    /**
     * Add filter
     * @access protected
     * @param object $column
     * @return Congts_ProductPress_Block_Adminhtml_Catalog_Product_Edit_Tab_Press
     *
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($column->getId() == 'in_press') {
            $newsIds = $this->_getSelectedNews();
            if (empty($newsIds)) {
                $newsIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('id', array('in' => $newsIds));
            } else {
                if ($newsIds) {
                    $this->getCollection()->addFieldToFilter('id', array('nin' => $newsIds));
                }
            }
        } else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }
}