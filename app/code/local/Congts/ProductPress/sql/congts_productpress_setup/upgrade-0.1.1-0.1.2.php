<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('congts_productpress/press_news')} ADD COLUMN `product_id` int(11) AFTER `link`
");

$installer->endSetup();