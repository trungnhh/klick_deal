<?php
require_once(Mage::getBaseDir('lib') . '/mail/SMTP_ValidateEmail.php');
class Congts_VerifyEmail_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * check email is exist or not
     * checkAction()
     * @return json
     */
    public function checkAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->_ajaxRedirectResponse();
            return;
        }

        $email = $this->getRequest()->getPost('email');
        $checkEmail = Mage::helper('congts_verifyemail')->check($email);

        $result = array();
        if ($checkEmail) {
            $result['status'] = true;
            $result['message'] = $this->__('The email is valid.');
        } else {
            $result['status'] = false;
            $result['message'] = $this->__('The email is not exist.');
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function emailIsAliveAction(){
        $email = $this->getRequest()->getParam('email');
        $checkEmail = Mage::helper('congts_verifyemail')->check($email);
        var_dump($checkEmail);die;
    }

    public function smtpValidAction()
    {
        $email = $this->getRequest()->getParam('email');
        $sender = 'trungnh28@gmail.com';
        $smtp_email = new SMTP_ValidateEmail();
        $smtp_email->debug = false;
        $results = $smtp_email->validate(array($email), $sender);
        var_dump($results);die;

    }
    /**
     * Send Ajax redirect response
     *
     * @return Congts_VerifyEmail_IndexController
     */
    protected function _ajaxRedirectResponse()
    {
        $this->getResponse()
            ->setHeader('HTTP/1.1', '403 Session Expired')
            ->setHeader('Login-Required', 'true')
            ->sendResponse();
        return $this;
    }
}