<?php

/**
 * Congts MyAccount module
 *
 * @category   Congts
 * @package    Congts_MyAccount
 */
class Congts_MyAccount_Block_MyProducts extends Mage_Core_Block_Template
{
    public function getMyProducts()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) return false;

        $customerData = Mage::getSingleton('customer/session')->getCustomer();
        $customerId = $customerData->getId();
        $items = Mage::getModel('sales/order_item')->getCollection()
            ->join(array('order' => 'sales/order'), 'main_table.order_id = order.entity_id')
            ->addFieldToFilter('customer_id', $customerId)
            ->setOrder('item_id','DESC');

        $listPaidItems = array();
        foreach ($items as $item) {
            if ($item->getBaseTotalDue() !== null || $item->getBaseTotalInvoiced() == $item->getBaseTotalPaid()) {
                $listPaidItems[] = $item->getProductId();
            }

            $downloadItem = Mage::getModel('downloadable/link_purchased_item')->getCollection()
                ->addFieldToFilter('order_item_id', $item->getItemId())
                ->addFieldToFilter('product_id', $item->getProductId())
                ->getFirstItem();
            $item->setLinkHash($downloadItem->getLinkHash());
        }

        return array('items' => $items,'list_paid_items' => $listPaidItems);
    }

    public function getRequestSendAutocodeLink($productId)
    {
        return Mage::getBaseUrl() . 'autocode/index/requestSendKey/id/' . $productId;
    }
}