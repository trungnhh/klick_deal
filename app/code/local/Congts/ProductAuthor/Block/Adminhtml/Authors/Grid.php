<?php

/**
 * Product Author Extension
 *
 * @category   Congts
 * @package    Congts_ProductAuthor
 * @author     CongTS
 */
class Congts_ProductAuthor_Block_Adminhtml_Authors_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('grid_id');
         $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('congts_productauthor/authors')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header' => Mage::helper('congts_productauthor')->__('ID'),
            'index' => 'id',
            'width' => '20px',
            'type' => 'number'
        ));

        $this->addColumn('name', array(
            'header' => Mage::helper('congts_productauthor')->__('Name'),
            'index' => 'name',
        ));

        $this->addColumn('created_at', array(
            'header' => Mage::helper('congts_productauthor')->__('Created'),
            'index' => 'created_at',
            'type' => 'datetime',
        ));

        $this->addColumn('updated_at', array(
            'header' => Mage::helper('congts_productauthor')->__('Last Modified'),
            'index' => 'updated_at',
            'type' => 'datetime',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('congts_productauthor')->__('Status'),
            'align' => 'left',
            'width' => '85px',
            'index' => 'status',
            'type' => 'options',
            'options' => array(
                0 => Mage::helper('congts_productauthor')->__('Disabled'),
                1 => Mage::helper('congts_productauthor')->__('Enabled'),
            )
        ));

        $this->addColumn('action', array(
            'header' => Mage::helper('congts_productauthor')->__('Action'),
            'width' => '100',
            'getter'    => 'getId',
            'type' => 'action',
            'actions' => array(
                array(
                    'caption' => Mage::helper('congts_productauthor')->__('Edit'),
                    'url' => array('base' => '*/*/edit'),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false,
            'is_system' => true,
        ));
        $this->addExportType('*/*/exportCsv', $this->__('CSV'));
        $this->addExportType('*/*/exportExcel', $this->__('Excel XML'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

    protected function _prepareMassaction()
    {
        $modelPk = Mage::getModel('congts_productauthor/authors')->getResource()->getIdFieldName();
        $this->setMassactionIdField($modelPk);
        $this->getMassactionBlock()->setFormFieldName('ids');
        // $this->getMassactionBlock()->setUseSelectAll(false);
        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }
}
