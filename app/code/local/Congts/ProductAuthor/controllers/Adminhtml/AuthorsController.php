<?php

/**
 * Product Author Extension
 *
 * @category   Congts
 * @package    Congts_ProductAuthor
 * @author     CongTS
 */
class Congts_ProductAuthor_Adminhtml_AuthorsController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('congts_productauthor/adminhtml_authors'));
        $this->renderLayout();
    }

    public function exportCsvAction()
    {
        $fileName = 'Product Author_export.csv';
        $content = $this->getLayout()->createBlock('congts_productauthor/adminhtml_authors_grid')->getCsv();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function exportExcelAction()
    {
        $fileName = 'Product Author_export.xml';
        $content = $this->getLayout()->createBlock('congts_productauthor/adminhtml_authors_grid')->getExcel();
        $this->_prepareDownloadResponse($fileName, $content);
    }

    public function massDeleteAction()
    {
        $ids = $this->getRequest()->getParam('ids');
        if (!is_array($ids)) {
            $this->_getSession()->addError($this->__('Please select Product Author(s).'));
        } else {
            try {
                foreach ($ids as $id) {
                    $model = Mage::getSingleton('congts_productauthor/authors')->load($id);
                    $model->delete();
                }

                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) have been deleted.', count($ids))
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('congts_productauthor')->__('An error occurred while mass deleting items. Please review log and try again.')
                );
                Mage::logException($e);
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel('congts_productauthor/authors');

        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->_getSession()->addError(
                    Mage::helper('congts_productauthor')->__('This Product Author no longer exists.')
                );
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register('congts_productauthor_model', $model);

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('congts_productauthor/adminhtml_authors_edit'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        $redirectBack = $this->getRequest()->getParam('back', false);
        if ($data = $this->getRequest()->getPost()) {

            $id = $this->getRequest()->getParam('id');
            $model = Mage::getModel('congts_productauthor/authors');
            if ($id) {
                $model->load($id);
                if (!$model->getId()) {
                    $this->_getSession()->addError(
                        Mage::helper('congts_productauthor')->__('This Product Author no longer exists.')
                    );
                    $this->_redirect('*/*/index');
                    return;
                }
            }

            // save model
            try {
                if (isset($data['image'])) unset($data['image']); //unset image data
//                var_dump($data);die;
                $model->addData($data);
                $valueImage = $this->getRequest()->getParam('image');
                if (is_array($valueImage) && !empty($valueImage['delete'])) {
                    $model->setImage('');
                } elseif ($fileName = Mage::helper('congts_productauthor')->saveImage()) {
                    $model->setImage($fileName);
                }

                $model->setUpdatedAt(Varien_Date::now());
                $this->_getSession()->setFormData($data);
                $model->save();
                $this->_getSession()->setFormData(false);
                $this->_getSession()->addSuccess(
                    Mage::helper('congts_productauthor')->__('The Product Author has been saved.')
                );
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
                $redirectBack = true;
            } catch (Exception $e) {
                $this->_getSession()->addError(Mage::helper('congts_productauthor')->__('Unable to save the Product Author.'));
                $redirectBack = true;
                Mage::logException($e);
            }

            if ($redirectBack) {
                $this->_redirect('*/*/edit', array('id' => $model->getId()));
                return;
            }
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                // init model and delete
                $model = Mage::getModel('congts_productauthor/authors');
                $model->load($id);
                if (!$model->getId()) {
                    Mage::throwException(Mage::helper('congts_productauthor')->__('Unable to find a Product Author to delete.'));
                }
                $model->delete();
                // display success message
                $this->_getSession()->addSuccess(
                    Mage::helper('congts_productauthor')->__('The Product Author has been deleted.')
                );
                // go to grid
                $this->_redirect('*/*/index');
                return;
            } catch (Mage_Core_Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            } catch (Exception $e) {
                $this->_getSession()->addError(
                    Mage::helper('congts_productauthor')->__('An error occurred while deleting Product Author data. Please review log and try again.')
                );
                Mage::logException($e);
            }
            // redirect to edit form
            $this->_redirect('*/*/edit', array('id' => $id));
            return;
        }
        // display error message
        $this->_getSession()->addError(
            Mage::helper('congts_productauthor')->__('Unable to find a Product Author to delete.')
        );
        // go to grid
        $this->_redirect('*/*/index');
    }
    
    /**
     * Check is allowed access to action
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return true;
    }
}