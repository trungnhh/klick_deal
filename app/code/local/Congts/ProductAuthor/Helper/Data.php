<?php

/**
 * Product Author Extension
 *
 * @category   Congts
 * @package    Congts_ProductAuthor
 * @author     CongTS
 */
class Congts_ProductAuthor_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * save author image
     * @return bool
     */
    public function saveImage()
    {
        if (isset($_FILES['image']['name']) and (file_exists($_FILES['image']['tmp_name']))) {
            try {
                $path = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'product' . DS . 'authors' . DS;
                $uploader = new Mage_Core_Model_File_Uploader('image');
                $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
                $uploader->setAllowRenameFiles(true);
                $result = $uploader->save($path);
                return $result['file'];
            } catch (Mage_Core_Exception $e) {
                Mage::log($e->getMessage());
                return false;
            } catch (Exception $e) {
                if ($e->getCode() != Mage_Core_Model_File_Uploader::TMP_NAME_EMPTY) {
                    Mage::logException($e);
                }
                return false;
            }
        }

        return false;
    }
}