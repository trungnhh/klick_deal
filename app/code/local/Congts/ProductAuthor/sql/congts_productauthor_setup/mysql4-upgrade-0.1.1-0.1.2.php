<?php
$installer = $this;

$installer->startSetup();

$installer->run("
ALTER TABLE `productauthor_authors` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '';
");

$installer->endSetup();