<?php
$installer = Mage::getResourceModel('catalog/setup','default_setup');
/***
 * When working with EAV entities it's important to use their module's setup class.
 */

$installer->startSetup();

$installer->updateAttribute(
    'catalog_product',
    'author',
    'source_model',
    'congts_productauthor/authors_attribute_model_source'
);
$installer->updateAttribute(
    'catalog_product',
    'author',
    'is_visible',
    1
);
$installer->endSetup();