<?php
/**
 * Product Author Extension
 *
 * @category   Congts
 * @package    Congts_ProductAuthor
 * @author     CongTS
 */
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS {$this->getTable('congts_productauthor/authors')};
CREATE TABLE {$this->getTable('congts_productauthor/authors')} (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL default '',
  `image` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->endSetup();