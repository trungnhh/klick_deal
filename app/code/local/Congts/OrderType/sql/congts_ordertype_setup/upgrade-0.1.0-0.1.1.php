<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$installer->run("
  ALTER TABLE {$this->getTable('sales_flat_order_item')} ADD COLUMN `product_isbn` varchar(255);
");

$installer->endSetup();