<?php

/**
 * Congts PdfInvoiceEmail module
 *
 * @category   Congts
 * @package    Congts_PdfInvoiceEmail
 */
class Congts_PdfInvoiceEmail_Model_Observer
{
    /**
     * hook to sales_order_invoice_save_after event
     * send invoice email automatically
     *
     * @param $observer
     */
    public function salesOrderInvoiceSaveAfter($observer)
    {
        $invoice = Mage::app()->getRequest()->getParam('invoice');
        if (isset($invoice['send_email']) && $invoice['send_email'] == 1) return;
        try {
            /* @var $order Mage_Sales_Model_Order_Invoice */
            $invoice = $observer->getEvent()->getInvoice();
            $sendEmail = $this->_allowSendEmail($invoice);

            if ($sendEmail) $invoice->sendEmail();
        } catch (Mage_Core_Exception $e) {
            Mage::log($e->getMessage());
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }
    }

    /**
     * hook to checkout_onepage_controller_success_action event
     * send invoice email automatically
     *
     * @param $observer
     */
    public function checkoutOnepageControllerSuccessAction($observer)
    {
        try {
            /* @var $order Mage_Sales_Model_Order_Invoice */
            $orderIds = $observer->getEvent()->getOrderIds();
            foreach ($orderIds as $orderId) {
                $order = Mage::getModel('sales/order')->load($orderId);
                if ($order->hasInvoices()) {
                    foreach ($order->getInvoiceCollection() as $invoice) {
                        $sendEmail = $this->_allowSendEmail($invoice);

                        if ($sendEmail) $invoice->sendEmail();
                    }
                }
            }
        } catch (Mage_Core_Exception $e) {
            Mage::log($e->getMessage());
        } catch (Exception $e) {
            Mage::log($e->getMessage());
        }
    }

    /**
     * check send invoice email if order has downloadable item
     * @param $invoice
     * @return bool
     */
    protected function _allowSendEmail($invoice)
    {
        $sendEmail = false;

        $itemProductIds = array();
        foreach ($invoice->getAllItems() as $invoiceItem) {
            $itemProductIds[] = $invoiceItem->getProductId();
        }

        $productCollection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect(array('entity_id','type_id'))
            ->addFieldToFilter('entity_id', array('in' => $itemProductIds));

        foreach ($productCollection as $product) {
            if ($product->getTypeId() == 'downloadable') {
                $sendEmail = true;
                break;
            }
        }

        return $sendEmail;
    }
}