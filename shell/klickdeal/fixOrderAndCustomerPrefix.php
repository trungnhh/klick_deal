<?php
require_once '../abstract.php';
require_once '../../app/Mage.php';

class Klickdeal_Shell_FixOrderAndCustomerPrefix extends Mage_Shell_Abstract
{
    /**
     * The main function.
     *
     * @return void
     */
    public function run()
    {
        echo "--- Start: Klickdeal_Shell_FixOrderAndCustomerPrefix ---\n";
        $this->process();
        echo "--- End: Klickdeal_Shell_FixOrderAndCustomerPrefix ---\n";
    }

    /**
     * Process the store codes.
     *
     * @return void
     */
    public function process()
    {
        $orders = Mage::getModel('sales/order')->getCollection()
            ->setOrder('created_at', 'desc');

        $orders->join('order_address',
            'main_table.entity_id=order_address.parent_id',
            ['address_type', 'firstname']);
        $orders->addFieldToFilter('address_type', 'shipping');
        $orders->addFieldToFilter(
            'firstname',
            [
                ['like' => 'Herr%'],
                ['like' => 'Frau%']
            ]
        );

        foreach ($orders as $order) {
            $shippingAddress = $order->getShippingAddress();
            if ($shippingAddress) {
                $firstName = $shippingAddress->getFirstname();
                $prefix = '';
                if (strpos($firstName, 'Frau') !== false) {
                    $prefix = 'Frau';
                    $firstName =  trim(str_replace($prefix, '', $firstName));
                } elseif (strpos($firstName, 'Herr') !== false) {
                    $prefix = 'Herr';
                    $firstName =  trim(str_replace($prefix, '', $firstName));
                }
                $shippingAddress->setFirstname($firstName);
                if ($prefix) {
                    $order->setCustomerPrefix($prefix);
                    if ($shippingAddress) {
                        $shippingAddress->setPrefix($prefix)
                            ->save();
                    }
                    $order->getBillingAddress()->setPrefix($prefix)
                        ->save();
                    $order->save();
                    echo 'Updated order id: '. $order->getId() . PHP_EOL;
                    $email = $order->getCustomerEmail();
                    $customer = Mage::getModel('customer/customer');
                    $customer->setWebsiteId(1);
                    $customer->loadByEmail($email);
                    try {
                        $customer->setPrefix($prefix);
                        $customer->save();
                        echo 'Updated customer : '. $email . PHP_EOL;
                    } catch (Exception $e) {
                        echo $e->getMessage().PHP_EOL;
                    }
                }
            }
        }
    }
}

$shell = new Klickdeal_Shell_FixOrderAndCustomerPrefix();
$shell->run();
