<?php
require_once '../abstract.php';
require_once '../../app/Mage.php';

class Klickdeal_Shell_UltimoGenerate extends Mage_Shell_Abstract
{
    /**
     * The main function.
     *
     * @return void
     */
    public function run()
    {
        echo "--- Start: Klickdeal_Shell_UltimoGenerate ---\n";
        $this->process();
        echo "--- End: Klickdeal_Shell_UltimoGenerate ---\n";
    }

    /**
     * Process the store codes.
     *
     * @return void
     */
    public function process()
    {
        $this->runUltimo(null, null);

        $websites= Mage::app()->getWebsites();
        foreach ($websites as $website) {
            $websiteCode = $website->getCode();
            $storeCode = null;
            $this->runUltimo($websiteCode, $storeCode);

            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $websiteCode = $website->getCode();
                    $storeCode = $store->getCode();
                    $this->runUltimo($websiteCode, $storeCode);
                }
            }
        }
    }

    /**
     * Generate
     * @param $websiteCode
     * @param $storeCode
     */
    public function runUltimo($websiteCode, $storeCode)
    {
        echo "Start: Generating CSS when website is: ". $websiteCode. " and Store is: ".$storeCode."\n";

        Mage::getSingleton('ultimo/cssgen_generator')->generateCss('grid',   $websiteCode, $storeCode);
        Mage::getSingleton('ultimo/cssgen_generator')->generateCss('layout', $websiteCode, $storeCode);
        Mage::getSingleton('ultimo/cssgen_generator')->generateCss('design', $websiteCode, $storeCode);

        echo "Complete: Generating CSS when website is: ". $websiteCode. " and Store is: ".$storeCode."\n";

    }
}

$shell = new Klickdeal_Shell_UltimoGenerate();
$shell->run();