<?php
$base = realpath(dirname(__FILE__));

require_once $base.'/abstract.php';
require_once $base.'/../app/Mage.php';
umask(0);
Mage::app();

if (isset($_SERVER['MAGE_IS_DEVELOPER_MODE'])) {
    Mage::setIsDeveloperMode(true);
}
ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);

class Balance_Shell_Crawler extends Mage_Shell_Abstract
{
    public function run()
    {
        $crawler = Mage::getModel('eb_varnish/crawler');
        $crawler->run();
    }
}

$shell = new Balance_Shell_Crawler();
$shell->run();
