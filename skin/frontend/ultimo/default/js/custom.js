jQuery(document).ready(function(){
    jQuery('.regular-price .price').each(function () {
        var price = jQuery(this),
        txt = price.text(),
        splt = txt.split(','), 
        spltFirst = splt.pop(),
        spn3 = jQuery('<span/>', {
                text: spltFirst.substring(2, spltFirst.lastIndexOf('$')),
                        'class': 'font-small'
        }),
        spltSecond = splt.pop(),
        spn1 = jQuery('<span/>', {
                text: ' €',
                        'class': 'font-medium'
        }),
        spn2 = jQuery('<span/>', {
                text: spltSecond.substring(spltSecond.lastIndexOf('$') + 1) + ',',
                        'class': 'font-big'
        });
        price.text('');
        price.append(spn2).append(spn3).append(spn1);
    });
    
    jQuery('.special-price .price').each(function () {
        var price = jQuery(this),
        txt = price.text(),
        splt = txt.split(','), 
        spltFirst = splt.pop(),
        spn3 = jQuery('<span/>', {
                text: spltFirst.substring(2, spltFirst.lastIndexOf('$')),
                        'class': 'font-small'
        }),
        spltSecond = splt.pop(),
        spn1 = jQuery('<span/>', {
                text: ' €',
                        'class': 'font-medium'
        }),
        spn2 = jQuery('<span/>', {
                text: spltSecond.substring(spltSecond.lastIndexOf('$') + 1) + ',',
                        'class': 'font-big'
        });
        price.text('');
        price.append(spn2).append(spn3).append(spn1);
    });

    jQuery('.old-price-parent .price').each(function () {
        var price = jQuery(this),
        txt = price.text(),
        splt = txt.split(','), 
        spltFirst = splt.pop(),
        spn3 = jQuery('<span/>', {
                text: spltFirst.substring(2, spltFirst.lastIndexOf('$')),
                        'class': 'font-small'
        }),
        spltSecond = splt.pop(),
        spn1 = jQuery('<span/>', {
                text: ' €',
                        'class': 'font-medium'
        }),
        spn2 = jQuery('<span/>', {
                text: spltSecond.substring(spltSecond.lastIndexOf('$') + 1) + ',',
                        'class': 'font-big'
        });
        price.text('');
        price.append(spn2).append(spn3).append(spn1);
    });
        
    jQuery('.catalog-product-view .regular-price .price').each(function () {
        var price = jQuery(this),
        txt = price.text(),
        splt = txt.split(','), 
        spltFirst = splt.pop(),
        spn3 = jQuery('<span/>', {
                text: spltFirst.substring(2, spltFirst.lastIndexOf('$')),
                        'class': 'font-small-details'
        }),
        spltSecond = splt.pop(),
        spn1 = jQuery('<span/>', {
                text: ' €',
                        'class': 'font-medium-details'
        }),
        spn2 = jQuery('<span/>', {
                text: spltSecond.substring(spltSecond.lastIndexOf('$') + 1) + ',',
                        'class': 'font-big-details'
        });
        price.text('');
        price.append(spn2).append(spn3).append(spn1);
    });
    
	jQuery("#customer_signin").fancybox();
    jQuery("#is_customer_signin").change(function() {
        if (this.checked) {
            jQuery("#customer_signin").click();
        }
    });
    jQuery("#customer_signin").click(function() {
        jQuery("#is_customer_signin").prop("checked", true);
    });
    jQuery("#is_customer_signin").click(function() {
        jQuery.fancybox.open({
            href: "#myModal"
        });
    });
    jQuery('label[for="is_customer_signin"]').click(function() {
        jQuery.fancybox.open({
            href: "#myModal"
        });
    });

    jQuery("#billing-address-select").attr("onchange", ""); //fix the onchange bug in console
    /*if(jQuery('select#billing\\:prefix').val() == 'Firma'){
        jQuery(this).find('[value="Firma"]').remove();
        jQuery('.company-vat').show();
    }
	jQuery(document).on('change', 'select#billing\\:prefix', function() {
        if(jQuery(this).val() == 'Firma'){
            jQuery(this).find('[value="Firma"]').remove();
            jQuery('.company-vat').show();
        }
    });*/

    //Hide SOFOFT banking for not allowed country
    var SOFOFTList = ['DE', 'AT', 'CH', 'BE', 'UK', 'NL', 'IT', 'PL', 'HU', 'FR', 'ES', 'CZ', 'SK'],
        SOFOFTInput = jQuery("#p_method_paymentnetwork_pnsofortueberweisung"),
        countrySelect = jQuery("#billing\\:country_id");
    if (countrySelect.val() !== "" && SOFOFTList.indexOf(countrySelect.val()) == -1) {
        if (SOFOFTInput.is(":checked")) {
            jQuery("[name='payment[method]']").first().click();
        }
        SOFOFTInput.parent().hide();
    }
    jQuery("#billing-address-select").change(function() {
        if (SOFOFTList.indexOf(countrySelect.val()) == -1) {
            if (SOFOFTInput.is(":checked")) {
                jQuery("[name='payment[method]']").first().click();
            }
            SOFOFTInput.parent().hide();
        } else {
            SOFOFTInput.parent().show();
        }
    });
    countrySelect.change(function() {
        if (SOFOFTList.indexOf(this.value) == -1) {
            if (SOFOFTInput.is(":checked")) {
                jQuery("[name='payment[method]']").first().click();
            }
            SOFOFTInput.parent().hide();
        } else {
            SOFOFTInput.parent().show();
        }
    });
    //end

    //PIX-70: Packstation is only available in Germany
    jQuery(".delivery-options input").change(function() {
        if (jQuery("#billing\\:packing_station").is(":checked")) {
            jQuery("#shipping\\:prefix").find('[value="Firma"]').hide();
            jQuery("#shipping\\:country_id option").not('[value="DE"]').hide();
            jQuery("#shipping\\:country_id").val("DE");
            jQuery('input[name="shipping[company]"]').parentsUntil('li').hide();
        } else {
            jQuery("#shipping\\:prefix").find('[value="Firma"]').show();
            jQuery("#shipping\\:country_id option").not('[value="DE"]').show();
            jQuery('input[name="shipping[company]"]').parentsUntil('li').show();
        }
    });
    //end

    jQuery(".impressumPopup").on("click", function() { 
        jQuery("#overlay").fadeTo(100, .3); 
        jQuery("#popup1").show();
        return false; 
    })
    jQuery(".agbPopup").on("click", function() { 
        jQuery("#overlay").fadeTo(100, .3); 
        jQuery("#popup2").show();
        return false; 
    })
    jQuery(".datenschutzPopup").on("click", function() { 
        jQuery("#overlay").fadeTo(100, .3); 
        jQuery("#popup3").show();
        return false; 
    })
    jQuery(".widerrufsrechtPopup").on("click", function() { 
        jQuery("#overlay").fadeTo(100, .3); 
        jQuery("#popup4").show();
        return false; 
    })
    jQuery(".versandPopup").on("click", function() { 
        jQuery("#overlay").fadeTo(100, .3); 
        jQuery("#popup5").show();
        return false; 
    })
 
    jQuery(".popup .close").on("click", function() { 
        jQuery("#overlay").hide(); 
        jQuery(".popup").hide(); 
        return false;
    })
});