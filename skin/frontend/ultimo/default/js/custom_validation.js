//customize validate-email
/*
Validation.add('validate-email', 'Please enter a valid email address. For example johndoe@domain.com.', function (v) {

    if (!(Validation.get('IsEmpty').test(v) || /^([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9,!\#\$%&'\*\+\/=\?\^_`\{\|\}~-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*@([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z0-9-]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*\.(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]){2,})$/i.test(v))) {
        return false;
    }
    //var verify_email_url = '/mut_new/verifyemail/index/check';
    var verify_email_url = VALIDATE_EMAIL;
    var emailChecked = jQuery('#email-checked').val();
    if(emailChecked == '1'){
        return true;
    }
    var validate = false;
    new Ajax.Request(
        verify_email_url,
        {
            method: 'post',
            asynchronous: false,
            onSuccess: function(value){
                var response = JSON.parse(value.responseText);
                if (response.status == false) {
                    Validation.get('validate-email').error = response.message;
                    validate = false;
                    jQuery('#email-checked').val('0');
                } else {
                    jQuery('#email-checked').val('1');
                    validate = true;
                }
            },
            onComplete: function() {
                if ($('advice-validate-email-email')) {
                    $('advice-validate-email-email').remove();
                }

                if ($('advice-validate-email-email_address')) {
                    $('advice-validate-email-email_address').remove();
                }

                if ($('advice-validate-email-billing:email')) {
                    $('advice-validate-email-billing:email').remove();
                }

                if ($('advice-validate-email-shipping:email')) {
                    $('advice-validate-email-shipping:email').remove();
                }
            },
            parameters: {email: v}
        }
    );
    return validate;
});*/
Validation.add('validate-german-phone', 'Please enter a valid phone number. For example 08942040031 or 089 / 420 400-31 or 089/420 400-31.', function (v) {
    return Validation.get('IsEmpty').test(v) || /^[(]?\b\d{3}[) ]?[-.\/]?[ ]?\d{3}[ ]?\d{3}[-. ]?\d{2}\b$/i.test(v);
});


