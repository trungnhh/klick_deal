$jq=jQuery.noConflict();
var Ebvarnish	=	function(){
    return {
        onUpdate:function(){
            var url = '/varnish/update/index';
            if (typeof(CURRENT_PRODUCT_ID)!= 'undefined'){
                url += '/id/'+CURRENT_PRODUCT_ID;
            }
            new Ajax.Request(url, {
                    method:'POST',
                    contentType: "application/json",
                    dataType: "text",
                    onLoading:function(data){
                    },
                    onComplete:function(data){
                        if(200!=data.status){
                            $jq("#top .user-menu").show();
                            return false;
                        }else{
                            var htmls = data.responseText.evalJSON();
                            for(var property in htmls){
                                if ('formkey' === property) {
                                    var real_key = htmls[property];
                                    $$('input[name="form_key"]').each(function(e){e.value=real_key});
                                    $$('form[action*="form_key"]').each(function(e){
                                        actionurl=e.action;
                                        actionurl=actionurl.replace(/\/form_key\/[^\/]+/, '/form_key/'+real_key);
                                        actionurl=actionurl.replace(/([\&\?])form_key=([^\&])/, '$1form_key='+real_key + '$2');
                                        //console.log(actionurl);
                                        e.action=actionurl;
                                    });
                                    $$('a[href*="form_key"]').each(function(e){
                                        link=e.href;
                                        link=link.replace(/\/form_key\/[^\/]+/, '/form_key/'+real_key);
                                        link=link.replace(/([\&\?])form_key=([^\&])/, '$1form_key='+real_key + '$2');
                                        //console.log(link);
                                        e.href=link;
                                    });
                                    $$('form').each(function(e){e.enable();});
                                }
                                if('usermenu' === property){
                                    $jq("#top .user-menu").html(htmls[property]);
                                    $jq("#top .user-menu").css('opacity', 1);

                                }
                                if('header_top_links' === property){
                                    $jq(".header-text-top").html(htmls[property]);
                                    $jq(".header-text-top").css('opacity', 1);

                                }

                                if('mobile.customer.link' === property){
                                    $jq(".nav .vertnav").append(htmls[property]);
                                    $jq(".mobile-customer-link").css('opacity', 1);

                                }

                                if('reviews_form' === property){
                                    /* TODO update with product id */
                                    $jq("#customer-reviews").replaceWith(htmls[property]);
                                }
                                if('reviews_summary' === property){
                                    /* TODO update with product id */
                                    $jq("#reviews-summary").replaceWith(htmls[property]);
                                }

                                if('related_download' === property){
                                    /* TODO update with product id */
                                    $jq(".related-download-std").html(htmls[property]);
                                }
                                if('count_down' === property){
                                    $jq("#product-count-down").html(htmls[property]);
                                }
                                if('globalmsg' === property || 'msg' === property){
                                    if (htmls['msg'] != '' && 'globalmsg' === property) {
                                        continue;
                                    }
                                    if ($jq('.col-main ul.messages').length<=0) {
                                        $jq('.main.container').prepend(htmls[property]);
                                    } else {
                                        if (htmls[property] != '') {
                                            $jq('.col-main ul.messages').replaceWith(htmls[property]);
                                        } else {
                                            /* @todo improve this point if duplicate message some pages*/
                                            if ($jq('.contacts-index-index').length<=0 && $jq('.customer-account-login').length<=0 && $jq('.customer-account-forgotpassword').length<=0) {
                                                $jq('.col-main ul.messages').remove();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                });
        }
    }
}();
Prototype.Browser.IE?Event.observe(window,"load",function(){Ebvarnish.onUpdate()}):document.observe("dom:loaded",function(){Ebvarnish.onUpdate()});
