;
var isloading = false;
var dataHtml = '';
var isCreateAccount = false;
var billlingAddressForm;
function getHtml(urlData){
	if(	'https:' == document.location.protocol){
		urlData = urlData.replace("http:", "https:");
	}
	if(isloading==false){
		$j.ajax({
			url: urlData,
			type: 'GET',
			dataType: 'html',
			data: { ajaxonepage: 1 },
			beforeSend: function(){
				isloading = true;
				jQuery('.opc-ajax-loader').show();
			},
			success: function(data) {
				jQuery('.opc-ajax-loader').hide();
				$j.fancybox({
					'type': 'inline',
					maxWidth    : '800px',
					width        : '70%',
					'content': '<div id="cms-ajax-content">' + data + '</div>'
				});
				isloading = false;
				return false;

			}
		});
	}
}

function sleep(milliseconds) {
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
		if ((new Date().getTime() - start) > milliseconds){
			break;
		}
	}
}

function checkoutLink(){

	$j('.checkout-term').on('click', function(){
		getHtml($j(this).attr('href'));
		return false;
	});

	$j('.agree .conditions').on('click', function(){
		getHtml($j(this).attr('href'));
		return false;
	});

}
//dummy
Billing =  Class.create();
Shipping =  Class.create();
if(validateFlag == 'undefined'){
	var validateFlag = false;
}
if(validateShippingFlag == 'undefined'){
	var validateShippingFlag = false;
}
$j = jQuery;
if (!window.hasOwnProperty('IWD')) {
	IWD = {};
};

IWD.OPC = {

	agreements : null,
	saveOrderStatus:false,

	initMessages: function(){
		$j('.close-message-wrapper, .opc-messages-action .button').click(function(){
			$j('.opc-message-wrapper').hide();
			$j('.opc-message-container').empty();
		});
	},

	/** CREATE EVENT FOR SAVE ORDER **/
	initSaveOrder: function(){

		$j(document).on('click', '.opc-btn-checkout', function(){
			//validate agreements checkbox
			var tickCheckbox = true;
			$j('.checkout-agreements input[type="checkbox"]').each(function(){
				if (!$j(this).prop('checked')) {
					tickCheckbox = false;
					return;
				}
			});
			if (!tickCheckbox) {
				var mess = $j('#message-select-agreements').html();
				alert(mess);
				return;
			}

			var addressForm = new VarienForm('billing-new-address-form');
			if (!addressForm.validator.validate()){
				return;
			}

			if (!$j('input[name="billing[use_for_shipping]"]').prop('checked')){
				var addressForm = new VarienForm('opc-address-form-shipping');
				if (!addressForm.validator.validate()){
					return;
				}
			}

			IWD.OPC.saveCustomerComment();
			IWD.OPC.saveOrderStatus = true;

			if (IWD.OPC.Checkout.isVirtual===false){
				IWD.OPC.Shipping.saveShippingMethod();
			}else{
				IWD.OPC.validatePayment();
			}
		});

	},



	/** INIT CHAGE PAYMENT METHOD **/
	initPayment: function(){
		IWD.OPC.bindChangePaymentFields();
		$j(document).on('click', '#co-payment-form input[type="radio"]', function(event){
			IWD.OPC.validatePayment();
		});
	},

	/** CHECK PAYMENT IF PAYMENT IF CHECKED AND ALL REQUIRED FIELD ARE FILLED PUSH TO SAVE **/
	validatePayment: function(){

		payment.validate();

		var paymentMethodForm = new Validation('co-payment-form', { onSubmit : false, stopOnFirst : false, focusOnError : false});

		if (paymentMethodForm.validate()){
			IWD.OPC.savePayment();
		}else{
			IWD.OPC.saveOrderStatus = false;
			IWD.OPC.bindChangePaymentFields();
		}


	},

	/** BIND CHANGE PAYMENT FIELDS **/
	bindChangePaymentFields: function(){
		IWD.OPC.unbindChangePaymentFields();

		$j('#co-payment-form input').change(function(event){

			if (IWD.OPC.Checkout.ajaxProgress!=false){
				clearTimeout(IWD.OPC.Checkout.ajaxProgress);
			}

			IWD.OPC.Checkout.ajaxProgress = setTimeout(function(){
				IWD.OPC.validatePayment();
			}, 1000);
		});

		$j('#co-payment-form select').change(function(event){
			if (IWD.OPC.Checkout.ajaxProgress!=false){
				clearTimeout(IWD.OPC.Checkout.ajaxProgress);
			}

			IWD.OPC.Checkout.ajaxProgress = setTimeout(function(){
				IWD.OPC.validatePayment();
			}, 1000);
		});
	},

	/** UNBIND CHANGE PAYMENT FIELDS **/
	unbindChangePaymentFields: function(){
		$j('#co-payment-form input').unbind('change');
		$j('#co-payment-form select').unbind('change');
	},


	/** SAVE PAYMENT **/
	savePayment: function(){

		if (IWD.OPC.Checkout.xhr!=null){
			IWD.OPC.Checkout.xhr.abort();
		}
		methodUsing =   '';
		var selectedMethod = jQuery("input[type='radio'][name='safepayment[method]']:checked");
		if (selectedMethod.length > 0) {
			methodUsing = selectedMethod.val();
		}
		$j('#p_method_saferpaycw_creditcard').val(methodUsing);
		var form = $j('#co-payment-form').serializeArray();
		IWD.OPC.Checkout.showLoader();
		IWD.OPC.Checkout.xhr = $j.post(IWD.OPC.Checkout.config.baseUrl + 'onepage/json/savePayment',form, IWD.OPC.preparePaymentResponse,'json');
	},

	/** CHECK RESPONSE FROM AJAX AFTER SAVE PAYMENT METHOD **/
	preparePaymentResponse: function(response){
		IWD.OPC.Checkout.hideLoader();
		IWD.OPC.Checkout.xhr = null;

		IWD.OPC.agreements = $j('#checkout-agreements').serializeArray();


		if (typeof(response.review)!= "undefined" && IWD.OPC.saveOrderStatus===false){
			$j('#opc-review-block').html(response.review);
			IWD.OPC.Checkout.removePrice();
		}

		if (typeof(response.error) != "undefined"){

			IWD.OPC.Plugin.dispatch('error');

			$j('.opc-message-container').html(response.error);
			$j('.opc-message-wrapper').show();
			IWD.OPC.Checkout.hideLoader();
			IWD.OPC.saveOrderStatus = false;

			return;
		}
		if (typeof(response.payment_method)!="undefined" && response.payment_method!=""){
			$j('.payment-info').html(response.payment_method);
		}
		//SOME PAYMENT METHOD REDIRECT CUSTOMER TO PAYMENT GATEWAY
		if (typeof(response.redirect) != "undefined" && IWD.OPC.saveOrderStatus===true){
			setLocation(response.redirect);
			return;
		}

		if (IWD.OPC.saveOrderStatus===true){
			IWD.OPC.saveOrder();
		}

	},

	/** SAVE ORDER **/
	saveOrder: function(){
		if (jQuery('#is_subscribed').is(":checked")) {
			ga('send', 'event', {
				eventCategory: 'Email Subscription',
				eventAction: 'check',
				eventLabel: 'Checkout Subsribe checkbox checked'
			});
		}
		if (jQuery("#billing\\:create_account").is(':checked')) {
			ga('send', 'event', {
				eventCategory: 'Customer create account',
				eventAction: 'check',
				eventLabel: 'Checkout register checkbox checked'
			});
		}
		var form = $j('#co-payment-form').serializeArray();
		form  = IWD.OPC.checkAgreement(form);
		IWD.OPC.Checkout.showLoader();
		if (IWD.OPC.Checkout.config.comment!=="0"){
			IWD.OPC.saveCustomerComment();
		}

		IWD.OPC.Plugin.dispatch('saveOrder');
		IWD.OPC.Checkout.xhr = $j.post(IWD.OPC.Checkout.saveOrderUrl ,form, IWD.OPC.prepareOrderResponse,'json');
	},

	/** SAVE CUSTOMER COMMNET **/
	saveCustomerComment: function(){
		$j.post(IWD.OPC.Checkout.config.baseUrl + 'onepage/json/comment',{"comment": $j('#customer_comment').val()});
	},

	/** ADD AGGREMENTS TO ORDER FORM **/
	checkAgreement: function(form){
		$j.each(IWD.OPC.agreements, function(index, data){

			form.push(data);
		});
		return form;
	},


	/** CHECK RESPONSE FROM AJAX AFTER SAVE ORDER **/
	prepareOrderResponse: function(response){

		if (typeof(response.error) != "undefined"){
			IWD.OPC.Checkout.hideLoader();
			IWD.OPC.saveOrderStatus = false;
			$j('.opc-message-container').html(response.error);
			$j('.opc-message-wrapper').show();
			return;
		}

		if (typeof(response.redirect) !="undefined"){
			//$j('.thankyou-steps').html(response.success);
			//jQuery('.main-layout').hide();
			//jQuery('div.step-3').show();
			//jQuery('li.step-3').addClass('active');
			//IWD.OPC.Checkout.hideLoader();
			//jQuery('li.step-3').removeClass('active');
			setLocation(response.redirect);
			return;
		}

		IWD.OPC.Plugin.dispatch('responseSaveOrder', response);
	}


};



IWD.OPC.Checkout = {
	config:null,
	ajaxProgress:false,
	xhr: null,
	isVirtual: false,

	saveOrderUrl: null,

	init:function(){

		if (this.config==null){
			return;
		}
		//base config
		this.config = $j.parseJSON(this.config);

		this.saveOrderUrl = IWD.OPC.Checkout.config.baseUrl + 'onepage/json/saveOrder',

			//DECORATE
			this.clearOnChange();
		this.removePrice();



		//MAIN FUNCTION
		IWD.OPC.Billing.init();
		IWD.OPC.Shipping.init();
		IWD.OPC.initMessages();
		IWD.OPC.initSaveOrder();


		if (this.config.isLoggedIn===1){
			var addressId = $j('#billing-address-select').val();
			if (addressId!='' && addressId!=undefined ){
				IWD.OPC.Billing.save();
			}
			else{
				//FIX FOR MAGENTO 1.8 - NEED LOAD PAYTMENT METHOD BY AJAX
				IWD.OPC.Checkout.pullPayments();
			}
		}
		else{
			//FIX FOR MAGENTO 1.8 - NEED LOAD PAYTMENT METHOD BY AJAX
			IWD.OPC.Checkout.pullPayments();
		}



		IWD.OPC.initPayment();
	},



	/** PARSE RESPONSE FROM AJAX SAVE BILLING AND SHIPPING METHOD **/
	prepareAddressResponse: function(response){
		IWD.OPC.Checkout.xhr = null;


		if (typeof(response.error) != "undefined"){
			$j('.opc-message-container').html(response.message);
			$j('.opc-message-wrapper').show();
			IWD.OPC.Checkout.hideLoader();
			return;
		}

		if (typeof(response.shipping) != "undefined"){
			$j('#shipping-block-methods').empty().html(response.shipping);
		}

		if (typeof(response.payments) != "undefined"){
			$j('#checkout-payment-method-load').empty().html(response.payments);
			payment.initWhatIsCvvListeners();//default logic for view "what is this?"

		}

		if (typeof(response.review_data)!="undefined" && response.review_data!=""){
			$j('.review-info').html(response.review_data);
			checkoutLink();
		}
		if (typeof(response.cart_total_data)!="undefined" && response.cart_total_data!=""){
			$j('.totals-wrap').html(response.cart_total_data);
		}
		if (typeof(response.cart_table_data)!="undefined" && response.cart_table_data!=""){
			$j('div.step-1').html(response.cart_table_data);
		}
		if (typeof(response.payment_block)!="undefined" && response.payment_block!="" && $j('#checkout-payment-method-load').length && $j('.step-3.thankyou-steps').is(":visible") === false){
			$j('#checkout-payment-method-load').html(response.payment_block);
		}

		if (typeof(response.isVirtual) != "undefined"){

			IWD.OPC.Checkout.isVirtual = true;
		}

		if (IWD.OPC.Checkout.isVirtual===false){
			IWD.OPC.Shipping.saveShippingMethod();

		}else{
			$j('.shipping-block').hide();
			$j('.payment-block').addClass('clear-margin');
			IWD.OPC.Checkout.hideLoader();
		}
	},

	/** PARSE RESPONSE FROM AJAX SAVE SHIPPING METHOD **/
	prepareShippingMethodResponse: function(response){
		IWD.OPC.Checkout.xhr = null;
		IWD.OPC.Checkout.hideLoader();
		if (typeof(response.error)!="undefined"){

			IWD.OPC.Plugin.dispatch('error');

			$j('.opc-message-container').html(response.message);
			$j('.opc-message-wrapper').show();
			IWD.OPC.saveOrderStatus = false;
			return;
		}

		if (typeof(response.review_data)!="undefined"){
			$j('.review-info').html(response.review_data);
			checkoutLink();
		}
		if (typeof(response.cart_total_data)!="undefined"){
			$j('.totals-wrap').html(response.cart_total_data);
		}
		if (typeof(response.cart_table_data)!="undefined"){
			$j('div.step-1').html(response.cart_table_data);
		}

		if (typeof(response.review)!="undefined" && IWD.OPC.saveOrderStatus===false){
			$j('#opc-review-block').html(response.review);
			IWD.OPC.Checkout.removePrice();
		}

		//IF STATUS TRUE - START SAVE PAYMENT FOR CREATE ORDER
		if (IWD.OPC.saveOrderStatus==true){
			IWD.OPC.validatePayment();
		}
	},


	clearOnChange: function(){
		$j('.opc-col-left input, .opc-col-left select').removeAttr('onclick').removeAttr('onchange');
	},

	removePrice: function(){

		$j('.opc-data-table tr th:nth-child(2)').remove();
		$j('.opc-data-table tbody tr td:nth-child(2)').remove();
		$j('.opc-data-table tfoot td').each(function(){
			var colspan = $j(this).attr('colspan');

			if (colspan!="" && colspan !=undefined){
				colspan = parseInt(colspan) - 1;
				$j(this).attr('colspan', colspan);
			}
		});

		$j('.opc-data-table tfoot th').each(function(){
			var colspan = $j(this).attr('colspan');

			if (colspan!="" && colspan !=undefined){
				colspan = parseInt(colspan) - 1;
				$j(this).attr('colspan', colspan);
			}
		});

	},

	showLoader: function(){
		$j('.opc-ajax-loader').show();
	},

	hideLoader: function(){
		$j('.opc-ajax-loader').hide();
	},

	/** APPLY SHIPPING METHOD FORM TO BILLING FORM **/
	applyShippingMethod: function(form){
		formShippimgMethods = $j('#opc-co-shipping-method-form').serializeArray();
		$j.each(formShippimgMethods, function(index, data){
			form.push(data);
		});

		return form;
	},

	/** APPLY NEWSLETTER TO BILLING FORM **/
	applySubscribed: function(form){
		if ($j('#is_subscribed').length){
			if ($j('#is_subscribed').is(':checked')){
				form.push({"name":"is_subscribed", "value":"1"});
			}
		}

		return form;
	},

	/** PULL REVIEW **/
	pullReview: function(){
		IWD.OPC.Checkout.showLoader();
		IWD.OPC.Checkout.xhr = $j.post(IWD.OPC.Checkout.config.baseUrl + 'onepage/json/review',function(response){
			IWD.OPC.Checkout.hideLoader();
			if (typeof(response.review)!="undefined"){
				$j('#opc-review-block').html(response.review);
				IWD.OPC.Checkout.removePrice();
			}
		});
	},

	/** PULL PAYMENTS METHOD AFTER LOAD PAGE **/
	pullPayments: function(){
		IWD.OPC.Checkout.showLoader();
		IWD.OPC.Checkout.xhr = $j.post(IWD.OPC.Checkout.config.baseUrl + 'onepage/json/payments',function(response){
			IWD.OPC.Checkout.hideLoader();

			if (typeof(response.error)!="undefined"){
				$j('.opc-message-container').html(response.error);
				$j('.opc-message-wrapper').show();
				IWD.OPC.saveOrderStatus = false;
				return;
			}

			if (typeof(response.payments)!="undefined"){
				$j('#checkout-payment-method-load').html(response.payments);
				payment.initWhatIsCvvListeners();
				IWD.OPC.bindChangePaymentFields();
			};

			IWD.OPC.Checkout.pullReview();

		},'json');
	}

};


IWD.OPC.Billing = {

	init: function(){
		//set flag use billing for shipping and init change flag
		$j('input[name="billing[difference_shipping_address]"]').prop('checked', false);
		this.setBillingForShipping(true);
		var shippingTitle = $j('.shipping-title').html();
		$j('input[name="billing[use_for_shipping]"]').change(function(){
			$j('.shipping-title').html(shippingTitle);
			$j('.packstation-fields').insertBefore($j(".shipping-street-address"));
			if ($j(this).is(':checked')){
				IWD.OPC.Billing.setBillingForShipping(true);
				$j('#opc-address-form-billing select[name="billing[country_id]"]').change();
			}else{
				IWD.OPC.Billing.setBillingForShipping(false);
			}
		});

		jQuery('.packstation_shipping_address').change(function(){
			$j('.shipping-title').html("Packstation");
			if($j(this).is(':checked')){
				if($j('#shipping-address-select').length>0){
					if($j('#shipping-address-select').val()!=''){
						$j('.packstation-fields').insertAfter($j("#select-shipping-address"));
					}
				}
				IWD.OPC.Billing.setBillingForPackstation(true);
			}else{
				if($j('#shipping-address-select').length>0){
					$j('.packstation-fields').insertBefore($j(".shipping-street-address"));
				}
				IWD.OPC.Billing.setBillingForPackstation(false);
			}

		});

		//update password field
		$j('input[name="billing[create_account]"]').click(function(){
			if ($j(this).is(':checked')){
				$j('#register-customer-password').show();
				IWD.OPC.Billing.validateAddressForm();
				isCreateAccount = true;
				if(IWD.OPC.Checkout.xhr!=null){
					IWD.OPC.Checkout.xhr.abort();
				}
				$j('#register-customer-password input').change(function(){
					if(!validateFlag)
						IWD.OPC.Billing.validateForm();
				});
			}else{
				$j('#register-customer-password').hide();
				$j('#register-customer-password input').val('').trigger("change");
				var valid = IWD.OPC.Billing.validateAddressForm();
				if(valid){
					IWD.OPC.Billing.save();
				}
			}

		});

		this.initChangeAddress();
		this.initChangeSelectAddress();

	},

	/** CREATE EVENT FOR UPDATE SHIPPING BLOCK **/
	initChangeAddress: function(){

		$j('#billing\\:postcode').change(function(){
			if(IWD.OPC.Billing.checkFields()){
				if(IWD.OPC.Billing.validateForm())
					validateFlag = true;
			}
		});

		$j('#billing\\:country_id').change(function(){
			if(IWD.OPC.Billing.validateForm())
				validateFlag = true;
		});
		$j('#billing\\:region_id').change(function(){
			if(IWD.OPC.Billing.validateForm())
				validateFlag = true;
		});
		//$j('#billing\\:telephone').change(function(){
		//	if(!validateFlag)
		//		IWD.OPC.Billing.validateForm();
		//});
		//$j('#billing\\:street1').change(function(){
		//	if(!validateFlag && IWD.OPC.Billing.checkFields())
		//		IWD.OPC.Billing.validateForm();
		//});
		//$j('#billing\\:email').change(function(){
		//	if(!validateFlag && IWD.OPC.Billing.checkFields())
		//		IWD.OPC.Billing.validateForm();
		//});
		$j('#billing\\:city').change(function(){
			if(!validateFlag)
				IWD.OPC.Billing.validateForm();
		});
		$j('#billing\\:confirm_password').change(function(){
			if(!validateFlag)
				IWD.OPC.Billing.validateForm();
		});
		// Fix problem validate when change data and click to create account
		$j('#billing\\:create_account').on('click', function(){
			IWD.OPC.Billing.validateAddressForm();
		});
//			$j('#opc-address-form-billing select').not('#billing-address-select').change(function(){
//				IWD.OPC.Billing.validateForm();
//			});
	},

	checkFields: function(){
		if($j('#billing\\:postcode').val() == '' || $j('#billing\\:city').val() == '' || $j('#billing\\:email').val() == ''){
			return false;
		}
		return true;
	},

	validateForm: function(){
		if(isCreateAccount==true){
			isCreateAccount = false;
			return false;
		}
		var valid = IWD.OPC.Billing.validateAddressForm();
		if (valid){
			IWD.OPC.Billing.save();
		}
	},


	/** CREATE EVENT FOR CHANGE ADDRESS TO NEW OR FROM ADDRESS BOOK **/
	initChangeSelectAddress: function(){
		$j('#billing-address-select').change(function(){
			if ($j(this).val()==''){
				$j('#billing-new-address-form').show();
			}else{
				$j('#billing-new-address-form').hide();
				IWD.OPC.Billing.validateForm();
			}
		});


	},

	/** VALIDATE ADDRESS BEFORE SEND TO SAVE QUOTE**/
	validateAddressForm: function(form){
		billlingAddressForm = new Validation('opc-address-form-billing', { onSubmit : false, stopOnFirst : false, focusOnError : false});
		if (billlingAddressForm.validate()){
			return true;
		}else{
			return false;
		}
	},

	/** SET SHIPPING AS BILLING TO TRUE OR FALSE **/
	setBillingForShipping:function(useBilling){
		$j('input[name="billing[packing_station]"]').prop('checked', false);
		$j('.packstation-fields').hide();
		$j('.shipping-street-address').show();
		if (useBilling==true){
			$j('input[name="billing[use_for_shipping]"]').prop('checked', true);
			$j('input[name="shipping[same_as_billing]"]').prop('checked', true);
			$j('#opc-address-form-shipping').hide();
			$j("#is_packstation").val(0);
		}else{
			if($j("#shipping\\:postcode").val()==''){
				this.pushBilingToShipping();
			}
			$j('input[name="billing[use_for_shipping]"]').prop('checked', false);
			$j('input[name="shipping[same_as_billing]"]').prop('checked', false);
			$j('#opc-address-form-shipping').show();
			$j("#is_packstation").val(0);
		}

	},
	setBillingForPackstation:function(useBilling){
		if (useBilling==false){
			$j('input[name="billing[use_for_shipping]"]').prop('checked', true);
			$j('input[name="shipping[same_as_billing]"]').prop('checked', true);
			$j('input[name="billing[difference_shipping_address]"]').prop('checked', false);
			$j('.packstation-fields').hide();
			$j('.shipping-street-address').show();
			$j('#opc-address-form-shipping').hide();
			$j("#is_packstation").val(0);
		}else{
			$j('input[name="billing[use_for_shipping]"]').prop('checked', false);
			$j('input[name="shipping[same_as_billing]"]').prop('checked', false);
			$j('input[name="billing[difference_shipping_address]"]').prop('checked', false);
			$j('.packstation-fields').show();
			$j('.shipping-street-address').hide();
			$j('#opc-address-form-shipping').show();
			$j("#is_packstation").val(1);
		}

	},

	/** COPY FIELD FROM BILLING FORM TO SHIPPING **/
	pushBilingToShipping:function(clearShippingForm){
		//pull country
		var valueCountry = $j('#billing-new-address-form select[name="billing[country_id]"]').val();
		$j('#opc-address-form-shipping  select[name="shipping[country_id]"] [value="' + valueCountry + '"]').prop("selected", true);
		shippingRegionUpdater.update();


		//pull region id
		var valueRegionId = $j('#billing-new-address-form select[name="billing[region_id]"]').val();
		$j('#opc-address-form-shipping  select[name="shipping[region_id]"] [value="' + valueRegionId + '"]').prop("selected", true);

		//pull other fields
		$j('#billing-new-address-form input').not(':hidden, :input[type="checkbox"]').each(function(){
			var name = $j(this).attr('name');
			var value = $j(this).val();
			var shippingName =  name.replace( /billing/ , 'shipping');

			$j('#opc-address-form-shipping input[name="'+shippingName+'"]').val(value);

		});

		//pull address field
		$j('#billing-new-address-form input[name="billing[street][]"]').each(function(indexBilling){
			var valueAddress = $j(this).val();
			$j('#opc-address-form-shipping input[name="shipping[street][]"]').each(function(indexShipping){
				if (indexBilling==indexShipping){
					$j(this).val(valueAddress);
				}
			});
		});

		//init trigger change shipping form
		//$j('#opc-address-form-shipping select[name="shipping[country_id]"]').change();
	},

	/** METHOD CREATE AJAX REQUEST FOR UPDATE SHIPPING METHOD **/
	save: function(){
		if (IWD.OPC.Checkout.ajaxProgress!=false){
			clearTimeout(IWD.OPC.Checkout.ajaxProgress);
		}

		IWD.OPC.Checkout.ajaxProgress = setTimeout(function(){
			// Fix problem validate when change data and click to create account
			if(isCreateAccount==true){
				isCreateAccount = false;
				IWD.OPC.Billing.validateForm();
				return false;
			}
			var form = $j('#opc-address-form-billing').serializeArray();
			form = IWD.OPC.Checkout.applyShippingMethod(form);
			form = IWD.OPC.Checkout.applySubscribed(form);

			if (IWD.OPC.Checkout.xhr!=null){
				IWD.OPC.Checkout.xhr.abort();
			}
			IWD.OPC.Checkout.showLoader();
			IWD.OPC.Checkout.xhr = $j.post(IWD.OPC.Checkout.config.baseUrl + 'onepage/json/saveBilling',form, IWD.OPC.Checkout.prepareAddressResponse,'json');
		}, 300);
	}




};

IWD.OPC.Shipping = {


	init: function(){

		this.initChangeAddress();
		this.initChangeSelectAddress();
		this.initChangeShippingMethod();
	},

	/** CREATE EVENT FOR UPDATE SHIPPING BLOCK **/
	initChangeAddress: function(){

		$j('#shipping\\:country_id').change(function(){
			if(IWD.OPC.Shipping.validateForm())
				validateShippingFlag = true;
		});
		$j('#shipping\\:region_id').change(function(){
			if(IWD.OPC.Shipping.validateForm())
				validateShippingFlag = true;
		});
		$j('#shipping\\:postcode').change(function(){
			if(IWD.OPC.Shipping.validateForm())
				validateShippingFlag = true;
		});
		$j('#shipping\\:telephone').change(function(){
			if(!validateShippingFlag)
				IWD.OPC.Shipping.validateForm();
		});
		$j('#shipping\\:street1').change(function(){
			if(!validateShippingFlag)
				IWD.OPC.Shipping.validateForm();
		});
		//$j('#shipping\\:lastname').change(function(){
		//	if(!validateShippingFlag)
		//		IWD.OPC.Shipping.validateForm();
		//});
		//$j('#shipping\\:firstname').change(function(){
		//	if(!validateShippingFlag)
		//		IWD.OPC.Shipping.validateForm();
		//});
		//$j('#opc-address-form-shipping select').not('#shipping-address-select').change(function(){
		//	IWD.OPC.Shipping.validateForm();
		//});
	},

	/** CREATE VENT FOR CHANGE ADDRESS TO NEW OR FROM ADDRESS BOOK **/
	initChangeSelectAddress: function(){
		$j('#shipping-address-select').change(function(){
			if ($j(this).val()==''){
				$j('.packstation-fields').insertBefore($j(".shipping-street-address"));
				$j('#shipping-new-address-form').show();
			}else{
				$j('.packstation-fields').insertAfter($j("#select-shipping-address"));
				$j('#shipping-new-address-form').hide();
				IWD.OPC.Shipping.validateForm();
			}
		});


	},

	//create observer for change shipping method.
	initChangeShippingMethod: function(){

		$j('.opc-wrapper-opc #shipping-block-methods').on('change', 'input[type="radio"]', function(){

			IWD.OPC.Shipping.saveShippingMethod();

		});

	},

	validateForm: function(){
		var valid = IWD.OPC.Shipping.validateAddressForm();
		if (valid){
			IWD.OPC.Shipping.save();
		}
	},

	/** VALIDATE ADDRESS BEFORE SEND TO SAVE QUOTE**/
	validateAddressForm: function(form){

		var addressForm = new Validation('opc-address-form-shipping', { onSubmit : false, stopOnFirst : false, focusOnError : false});
		if (addressForm.validate()){
			return true;
		}else{
			return false;
		}
	},

	/** METHOD CREATE AJAX REQUEST FOR UPDATE SHIPPIN METHOD **/
	save: function(){
		if (IWD.OPC.Checkout.ajaxProgress!=false){
			clearTimeout(IWD.OPC.Checkout.ajaxProgress);
		}

		IWD.OPC.Checkout.ajaxProgress = setTimeout(function(){
			var form = $j('#opc-address-form-shipping').serializeArray();
			form = IWD.OPC.Checkout.applyShippingMethod(form);
			if (IWD.OPC.Checkout.xhr!=null){
				IWD.OPC.Checkout.xhr.abort();
			}
			IWD.OPC.Checkout.showLoader();
			IWD.OPC.Checkout.xhr = $j.post(IWD.OPC.Checkout.config.baseUrl + 'onepage/json/saveShipping',form, IWD.OPC.Checkout.prepareAddressResponse);
		}, 300);
	},

	saveShippingMethod: function(){

		if (IWD.OPC.Shipping.validateShippingMethod()===false){


			$j('.opc-message-container').html('Please specify shipping method');
			$j('.opc-message-wrapper').show();

			IWD.OPC.Checkout.hideLoader();

			return;
		}


		if (IWD.OPC.Checkout.ajaxProgress!=false){
			clearTimeout(IWD.OPC.Checkout.ajaxProgress);
		}

		IWD.OPC.Checkout.ajaxProgress = setTimeout(function(){
			var form = $j('#opc-co-shipping-method-form').serializeArray();
			form = IWD.OPC.Checkout.applySubscribed(form);
			if (IWD.OPC.Checkout.xhr!=null){
				IWD.OPC.Checkout.xhr.abort();
			}
			IWD.OPC.Checkout.showLoader();
			IWD.OPC.Checkout.xhr = $j.post(IWD.OPC.Checkout.config.baseUrl + 'onepage/json/saveShippingMethod',form, IWD.OPC.Checkout.prepareShippingMethodResponse);
		}, 300);
	},

	validateShippingMethod: function(){
		var shippingChecked = false;
		$j('#opc-co-shipping-method-form input').each(function(){
			if ($j(this).prop('checked')){
				shippingChecked =  true;
			}
		});

		return shippingChecked;
	}
};


IWD.OPC.Coupon = {
	init: function(){


		$(document).on('click', '.apply-coupon', function(){
			IWD.OPC.Coupon.applyCoupon(false);
		});


		$(document).on('click', '.remove-coupon', function(){
			IWD.OPC.Coupon.applyCoupon(true);
		});

	},

	applyCoupon: function(remove){

		var form = $j('#opc-discount-coupon-form :input').serializeArray();
		if (remove===false){
			form.push({"name":"remove", "value":"0"});
		}else{
			form.push({"name":"remove", "value":"1"});
		}

		IWD.OPC.Checkout.showLoader();
		IWD.OPC.Checkout.xhr = $j.post(IWD.OPC.Checkout.config.baseUrl + 'onepage/coupon/couponPost',form, IWD.OPC.Coupon.prepareResponse,'json');
	},

	prepareResponse: function(response){
		IWD.OPC.Checkout.hideLoader();
		if (typeof(response.message) != "undefined"){
			$j('.opc-message-container').html(response.message);
			$j('.opc-message-wrapper').show();

			IWD.OPC.Checkout.pullReview();
		}
		if (typeof(response.coupon) != "undefined"){
			$j('.discount-block').html(response.coupon);
		}

	}
};

IWD.OPC.Agreement ={

	init: function(){
		$j(document).on('click', '.view-agreement', function(e){
			e.preventDefault();
			//$j('#agreement-dialog').modal('show');

			var id = $j(this).data('id');
			var title = $j(this).html();
			var content = $j('#agreement-block-'+id).html();

			$j('#agreement-title').html(title);
			$j('#agreement-modal-body').html(content);
		});

	}
};

IWD.OPC.Login ={

	init: function(){
		$j(document).on('click', '.restore-account', function(e){
			e.preventDefault();
			$j('#login-form').hide();$j('#login-button-set').hide();
			$j('#form-validate-email').fadeIn();$j('#forgotpassword-button-set').show();
		});


		$j('#login-button-set .btn').click(function(){
			$j('#login-form').submit();
		});

		$j('#forgotpassword-button-set .btn').click(function(){
			var form = $j('#form-validate-email').serializeArray();
			IWD.OPC.Checkout.showLoader();
			IWD.OPC.Checkout.xhr = $j.post(IWD.OPC.Checkout.config.baseUrl + 'onepage/json/forgotpassword',form, IWD.OPC.Login.prepareResponse,'json');
		});


		$j('#forgotpassword-button-set .back-link').click(function(e){
			e.preventDefault();
			$j('#form-validate-email').hide();$j('#forgotpassword-button-set').hide();
			$j('#login-form').fadeIn();$j('#login-button-set').show();

		});

	},

	prepareResponse: function(response){
		IWD.OPC.Checkout.hideLoader();
		if (typeof(response.error)!="undefined"){
			alert(response.message);
		}else{
			alert(response.message);
			$j('#forgotpassword-button-set .back-link').click();
		}
	}
};

IWD.OPC.Geo = {
	init: function(){

		if (IWD.OPC.Checkout.config.geoCountry===false){
			return;
		}else{
			//setup country for billing and than for shipping
			if ($j('#opc-address-form-billing select[name="billing[country_id]"]').is(":visible") && $j('#country_from_quote').val() == ""){
				$j('#opc-address-form-billing select[name="billing[country_id]"]').val(IWD.OPC.Checkout.config.geoCountry);
				billingRegionUpdater .update();
			}
		}


		if (IWD.OPC.Checkout.config.geoCity===false){
			return;
		}else{
			$j('#opc-address-form-billing [name="billing[city]"]').val(IWD.OPC.Checkout.config.geoCity);
		}


	}
};

$j(document).ready(function(){
	IWD.OPC.Checkout.init();
	IWD.OPC.Coupon.init();
	IWD.OPC.Agreement.init();
	IWD.OPC.Login.init();
	IWD.OPC.Geo.init();
	$j('.opc-link').on('click', function(){
		getHtml($j(this).attr('href'));
		return false;
	});
});
