IWD.OPC.Plugin = {
		
		observer: {},
		
		
		dispatch: function(event, data){
			//console.debug('DISPATCH::EVENT::' + event);
			if (typeof(IWD.OPC.Plugin.observer[event]) !="undefined"){
				
				var callback = IWD.OPC.Plugin.observer[event];
				callback(data);
				
			}
		},
		
		event: function(eventName, callback){
			IWD.OPC.Plugin.observer[eventName] = callback;
		}
};